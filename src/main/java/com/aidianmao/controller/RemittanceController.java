package com.aidianmao.controller;

import com.aidianmao.entity.Remittance;
import com.aidianmao.service.RemittanceService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class RemittanceController {
    @Autowired
    private RemittanceService remittanceService;

    /**
     * 线下充值提交
     * @param remittance    线下充值对象
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PostMapping(value = "v1/auth/remittances")
    public Result add(Remittance remittance, @CookieValue String aidianmaoMemberToken) {
        return remittanceService.add(remittance,aidianmaoMemberToken);
    }

    /**
     * 将图片保存至服务器，返回保存图片路径
     * @param file
     * @return
     */
    @PostMapping(value = "v1/auth/remittances-image")
    public Result getImageSource(MultipartFile file){
        return remittanceService.addSelfProductDetailPhoto(file);
    }


    /**
     * 多条件筛选线下汇款
     * @param remittance
     * @param aidianmaoMemberToken
     * @return
     */
    @GetMapping(value = "v1/auth/remittances")
    public Result getList(Remittance remittance, @CookieValue String aidianmaoMemberToken,Integer pageNum, Integer pageSize){
        return remittanceService.getList(remittance,aidianmaoMemberToken,pageNum,pageSize);
    }

}
