package com.aidianmao.controller;


import com.aidianmao.service.CaptchaService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @GetMapping(value = "v1/auth/captchas")
    public Result captcha(String resp){
        return captchaService.checkAuth(resp);
    }





}
