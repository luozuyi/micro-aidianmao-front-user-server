package com.aidianmao.controller;

import com.aidianmao.entity.TmallFastCommit;
import com.aidianmao.service.TmallFastCommitService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TmallFastCommitController {

    @Autowired
    private TmallFastCommitService service;

    /**
     * 首页快速挂店
     * @param record    快速挂点对象
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PostMapping("v1/tmall-fast-commits")
    public Result fastSale(TmallFastCommit record, @CookieValue String aidianmaoMemberToken){
        return service.fastSale(record,aidianmaoMemberToken);
    }
}
