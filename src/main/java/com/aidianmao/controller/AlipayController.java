package com.aidianmao.controller;


import com.aidianmao.entity.Alipay;
import com.aidianmao.service.AlipayService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
public class AlipayController {
    @Autowired
    private AlipayService alipayService;

    /**
     *  添加充值
     *  @param alipay 充值对象
     *  @param aidianmaoMemberToken 充值会员
     *  @return
     */
    @PostMapping(value = "v1/auth/alipays")
    public Result addAlipay(Alipay alipay, @CookieValue String aidianmaoMemberToken){
        return alipayService.addAlipay(alipay,aidianmaoMemberToken);
    }


    /**
     * 查询充值
     * @return
     */
    @GetMapping(value = "v1/auth/alipays")
    public Result getList(){
        return alipayService.getList();
    }


    /**
     * 分页查询充值列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/alipays/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, Alipay alipay, @CookieValue String aidianmaoMemberToken){
        return alipayService.pageList(pageNum,pageSize,alipay,aidianmaoMemberToken);
    }

    /**
     * 根据memberId查询其充值记录
     * @param alipay   支付宝对象
     * @param aidianmaoMemberToken   用户缓存
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/user-alipays")
    public Result getByMemberId(Alipay alipay,@CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize){
        return alipayService.getByMemberId(alipay,aidianmaoMemberToken,pageNum,pageSize);
    }

}
