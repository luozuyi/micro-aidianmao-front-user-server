package com.aidianmao.controller;

import com.aidianmao.entity.MemberBankInfo;
import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.service.MemberBankInfoService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberBankInfoController {
    @Autowired
    private MemberBankInfoService memberBankInfoService;

    /**
     * 添加个人银行
     * @param memberBankInfo 个人银行对象
     * @param provincesCitysCountrys 详细地址对象
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/member-bank-infos")
    public Result add(MemberBankInfo memberBankInfo, ProvincesCitysCountrys provincesCitysCountrys, @CookieValue String aidianmaoMemberToken) {
        return memberBankInfoService.add(memberBankInfo, provincesCitysCountrys, aidianmaoMemberToken);
    }
}
