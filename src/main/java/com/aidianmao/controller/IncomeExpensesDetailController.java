package com.aidianmao.controller;

import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.service.IncomeExpensesDetailService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IncomeExpensesDetailController {

    @Autowired
    private IncomeExpensesDetailService service;

    /**
     * 多条件筛选查询收支明细
     * @param record       收支明细对象
     * @param pageNum       当前页
     * @param pageSize      一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/income-details-list")
    public Result getList(IncomeExpensesDetail record, @CookieValue String aidianmaoMemberToken, Integer pageNum, Integer pageSize){
        return service.getAllBySelective(record,aidianmaoMemberToken,pageNum,pageSize);
    }
}
