package com.aidianmao.controller;

import com.aidianmao.service.SendPhoneCodeService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendPhoneCodeController {
    @Autowired
    private SendPhoneCodeService sendPhoneCodeService;

    /**
     * 会员注册时发送短信验证码
     * @param telephone 手机号
     * @return
     */
    @GetMapping(value = "v1/regist-telephone-code")
    public Result getRegistTelephoneCode(String telephone,String captcha) {
        return sendPhoneCodeService.getRegistTelephoneCode(telephone,captcha);
    }

    /**
     * 会员修改支付密码时发送短信验证码
     * @param phone  手机号
     * @return
     */
    @GetMapping(value = "v1/auth/regist-phone-code")
    public Result getModifyPhoneCode(String phone){
        return sendPhoneCodeService.getModifyPhoneCode(phone);
    }

    /**
     * 找回密码时发送短信验证码
     * @param phone    手机号
     * @return
     */
    @GetMapping(value = "v1/retrieve-phones")
    public Result getRetrievePhoneCode(String phone,String captcha){
        return sendPhoneCodeService.getRetrievePhoneCode(phone,captcha);
    }

    /**
     * 修改支付密码时人机验证发送短信验证码(正在使用)
     * @param phone
     * @param captcha
     * @return
     */
    @GetMapping(value = "v1/phone-messages")
    public Result sendPhoneMessage(String phone,String captcha){
        return sendPhoneCodeService.sendPhoneCode(phone,captcha);
    }
}
