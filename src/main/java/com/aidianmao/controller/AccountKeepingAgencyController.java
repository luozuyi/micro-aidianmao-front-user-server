package com.aidianmao.controller;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.service.AccountKeepingAgencyService;
import com.aidianmao.service.ProvincesService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AccountKeepingAgencyController {
    @Autowired
    private AccountKeepingAgencyService accountKeepingAgencyService;
    @Autowired
    private ProvincesService provincesService;

    /**
     *  添加记账城市
     *  @return
     */
    @GetMapping(value = "v1/account-keeping-agencys-city")
    public Result addAccountKeepingAgencyCity(){
        return accountKeepingAgencyService.addAccountKeepingAgencyCity();
    }

    /**
     *  添加记账
     *  @param accountKeepingAgency 记账对象
     *  @return
     */
    @PostMapping(value = "v1/account-keeping-agencys")
    public Result addAccountKeepingAgency(AccountKeepingAgency accountKeepingAgency){
        return accountKeepingAgencyService.addAccountKeepingAgency(accountKeepingAgency);
    }

    /**
     * 分页查询所有记账
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/account-keeping-agencys/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params){
        return accountKeepingAgencyService.pageList(pageNum,pageSize,params);
    }
    /**
     * 服务动态(最近的代理记账服务一条)
     * @return
     */
    @GetMapping(value = "v1/account-keeping-agencys/dynamic")
    public Result getDynamic(){
        return  accountKeepingAgencyService.getDynamic();
    }

    /**
     * 获取所有省份
     * @return
     */
    @GetMapping(value = "v1/Provinces")
    public Result getProvinces(){
        return provincesService.getProvinces();
    }
}
