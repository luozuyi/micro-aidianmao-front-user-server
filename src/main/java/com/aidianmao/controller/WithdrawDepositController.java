package com.aidianmao.controller;

import com.aidianmao.entity.MemberBankInfo;
import com.aidianmao.entity.WithdrawDeposit;
import com.aidianmao.service.WithdrawDepositService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class WithdrawDepositController {
    @Autowired
    private WithdrawDepositService withdrawDepositService;

    /**
     * 会员提交提现信息
     * @param withdrawDeposit 提现对象
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/withdraw-deposits")
    public Result add(WithdrawDeposit withdrawDeposit, @CookieValue String aidianmaoMemberToken) {
        return withdrawDepositService.add(withdrawDeposit, aidianmaoMemberToken);
    }

    /**
     * 申请提现
     * @param money 提现金额
     * @param payPassword   支付密码
     * @param aidianmaoMemberToken
     * @return
     */
    @PostMapping(value = "v1/auth/front-withdraw-deposits")
    public Result add(String money,String payPassword,@CookieValue String aidianmaoMemberToken,String remark){
        return withdrawDepositService.frontWithdraw(money,payPassword,aidianmaoMemberToken,remark);
    }

    /**
     * 多条件查询用户提现记录
     * @param withdrawDeposit   提现对象
     * @param aidianmaoMemberToken  用户
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/withdraw-deposits")
    public Result get(WithdrawDeposit withdrawDeposit, @CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize){
        return withdrawDepositService.getByMember(withdrawDeposit,aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 获取用户提现银行信息
     * @param aidianmaoMemberToken  用户
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/withdraw-account-banks")
    public Result getAccount(@CookieValue String aidianmaoMemberToken, Integer pageNum, Integer pageSize){
        return withdrawDepositService.getWithdrawAccount(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 新增提现银行信息
     * @param memberBankInfo
     * @param aidianmaoMemberToken
     * @return
     */
    @PostMapping(value = "v1/auth/withdraw-account-banks")
    public Result addAccount(MemberBankInfo memberBankInfo, @CookieValue String aidianmaoMemberToken){
        return withdrawDepositService.addWithdrawAccountInfo(memberBankInfo,aidianmaoMemberToken);
    }

    /**
     * 修改提现账户信息
     * @param memberBankInfo
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-account-banks")
    public Result updateDefault(MemberBankInfo memberBankInfo, @CookieValue String aidianmaoMemberToken){
        return withdrawDepositService.updateWithdrawAccountInfo(memberBankInfo,aidianmaoMemberToken);
    }

    /**
     * 设置默认
     * @param aidianmaoMemberToken
     * @param id
     * @return
     */
    @PatchMapping(value = "v1/auth/withdraw-account-banks-default")
    public Result setDefault(@CookieValue String aidianmaoMemberToken,String id){
        return withdrawDepositService.setDefault(aidianmaoMemberToken,id);
    }
    /**
     * 获取所有银行信息并分页
     * @return
     */
    @GetMapping(value = "v1/bank-infos")
    public Result getBankInfo(){
        return withdrawDepositService.getBankInfo();
    }
}
