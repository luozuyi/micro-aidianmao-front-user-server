package com.aidianmao.controller;

import com.aidianmao.service.ShopCartService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class ShopCartController {

    @Autowired
    private ShopCartService shopCartService;

    /**
     * 根据用户id获取购物车
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @GetMapping(value = "v1/auth/shop-carts")
    public Result getShopCart(@CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize){
        return shopCartService.getShopCart(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 添加购物车
     * @param tmallId 天猫网店id
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PostMapping(value = "v1/auth/shop-carts")
    public Result addShopCart(String tmallId,@CookieValue String aidianmaoMemberToken){
        return shopCartService.addShopCart(tmallId,aidianmaoMemberToken);
    }

    /**
     * 直接从购物车删除
     * @param tmallId 天猫网店id
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PatchMapping(value = "v1/auth/shop-carts")
    public Result deleteShopCart(String tmallId,@CookieValue String aidianmaoMemberToken){
         return shopCartService.deleteShopCart(tmallId,aidianmaoMemberToken);
    }

    /**
     * 分页查询购店车
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/shop-carts/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params){
        return shopCartService.pageList(pageNum,pageSize,params);
    }
}
