package com.aidianmao.controller;

import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.service.BusinessCooperationService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class BusinessCooperationController {
    @Autowired
    private BusinessCooperationService businessCooperationService;

    /**
     * 提交合作
     * @param citysName         城市名
     * @param industry          行业
     * @param companyName       公司名
     * @param phone             手机号
     * @param ipAddress         ip地址
     * @return
     */
    @PostMapping(value = "v1/business-cooperations")
    public Result addBusinessCooperation(String citysName,String industry,String companyName,String phone,String ipAddress){
        return businessCooperationService.addBusinessCooperation(citysName,industry,companyName,phone,ipAddress);
    }
}
