package com.aidianmao.controller;

import com.aidianmao.service.PlatformMessageService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlatformMessageController {

    @Autowired
    private PlatformMessageService service;

    /**
     * 获取站内信并分页
     * @param aidianmaoMemberToken   用户
     * @param pageNum       当前页
     * @param pageSize      一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/paltformMessages")
    public Result getList(@CookieValue String aidianmaoMemberToken,String status,Integer pageNum, Integer pageSize){
        return service.getList(aidianmaoMemberToken,status,pageNum,pageSize);
    }


    /**
     * 根据Id获取站内信
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/platformMessages-info")
    public Result getPlatformMessage(String id){
        return service.getPlatformMessage(id);
    }

    /**
     * 批量修改系统通知的状态为已读
     * @param ids
     * @return
     */
    @PatchMapping(value = "v1/auth/platformMessages")
    public Result updateAll(String[] ids,@CookieValue String aidianmaoMemberToken){
        return service.updateAll(ids,aidianmaoMemberToken);
    }


    /**
     * 批量删除系统通知
     * @param ids
     * @return
     */
    @PatchMapping(value = "v1/auth/platformMessages-more")
    public Result delete(String[] ids,@CookieValue String aidianmaoMemberToken){
        return service.delete(ids,aidianmaoMemberToken);
    }
}
