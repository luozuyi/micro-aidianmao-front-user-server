package com.aidianmao.controller;

import com.aidianmao.service.ArticleService;
import com.aidianmao.utils.Result;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ArticleController {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(ArticleController.class);
    @Autowired
    private ArticleService articleService;

    /**
     * 动态分页条件查询文章列表
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    @GetMapping(value = "v1/articles/pagination")
    public Result listPageBySelection(Integer pageNum, Integer pageSize, @RequestParam Map<String,Object> params){
        return  articleService.listPageBySelection(pageNum,pageSize,params);
    }

    /**
     * 获取文章详情
     * @param id   文章id
     * @return
     */
    @GetMapping(value = "v1/articles/detail")
    public Result toDetailArticle(String id){
        return articleService.toDetailArticle(id);
    }

    /**
     * 文章详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/article-details")
    public Result detailArticle(String id){
        return articleService.ArticleDetail(id);
    }

    /**
     * 获取栏目下面的文章
     * @param channelId   栏目id
     * @return
     */
    @GetMapping(value = "v1/articles/channel-id")
    public Result list(String channelId) {
        return articleService.list(channelId);
    }

    /**
     * 分页查询栏目下面的文章
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param channelId 栏目id
     * @return
     */
    @GetMapping(value = "v1/articles/pagination/channel-id")
    public Result listPagination(Integer pageNum, Integer pageSize, String channelId) {
        return articleService.list(pageNum,pageSize,channelId);
    }

    /**
     * 查询父栏目下面的子栏目文章
     * @param parentId
     * @return
     */

    @GetMapping(value = "v1/articles/parent-id")
    public Result recentlyList(String parentId) {
        return articleService.recentlyList(parentId);
    }


    @GetMapping(value = "v1/articles/test")
    public Result test(String content) {
        System.out.println(content);
        Result result = new Result();
        result.setData(content);
        content.equals("a");
        logger.error("v1/articles/test");
        return result;
    }

}
