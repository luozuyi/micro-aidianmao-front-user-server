package com.aidianmao.controller;


import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.service.OnlineQuestionAnswerService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class OnlineQuestionAnswerController {

    @Autowired
    private OnlineQuestionAnswerService onlineQuestionAnswerService;

    /**
     * 添加问答
     * @param onlineQuestionAnswer 提问对象
     * @param aidianmaoMemberToken 提问的人
     * */
    @PostMapping(value = "v1/auth/online-question-answers")
    public Result addOnlineQuestionAnswer(OnlineQuestionAnswer onlineQuestionAnswer,@CookieValue String aidianmaoMemberToken){
        return onlineQuestionAnswerService.addOnlineQuestionAnswer(onlineQuestionAnswer,aidianmaoMemberToken);
    }


    /**
     * 分页查询反馈列表
     * @param pageNum
     * @param pageSize
     * @param flag 查询标志 1：查询所有 2:查询当前用户
     * @param aidianmaoMemberToken
     * @return
     */
    @GetMapping(value = "v1/auth/online-question-answers/pagination")
    public Result pageList(Integer pageNum,Integer pageSize,String flag,@CookieValue String aidianmaoMemberToken,String tradeType){
        return onlineQuestionAnswerService.pageList(pageNum,pageSize,flag,aidianmaoMemberToken,tradeType);
    }

    /**
     * 分页查询反馈列表
     * @param pageNum
     * @param pageSize
     * @param flag 查询标志 1：查询所有 2:查询当前用户
     * @param aidianmaoMemberToken
     * @return
     */
    @GetMapping(value = "v1/online-question-answers/pagination")
    public Result getList(Integer pageNum,Integer pageSize,String flag,@CookieValue String aidianmaoMemberToken,String tradeType){
        return onlineQuestionAnswerService.pageList(pageNum,pageSize,flag,aidianmaoMemberToken,tradeType);
    }

    /**
     * 您可能感兴趣的问题(解答时间由近及远8条)
     * @return
     */
    @GetMapping(value="v1/online-question-answers-interested")
    public Result pageInterested(){
        return onlineQuestionAnswerService.pageInterested();
    }


    /**
     * 根据flag标志查询问题数量
     * @param flag
     * @param aidianmaoMemberToken
     * @return
     */
    @GetMapping(value = "v1/auth/online-question-answers")
    public Result getCount(String flag,@CookieValue String aidianmaoMemberToken){
        return onlineQuestionAnswerService.getCount(flag,aidianmaoMemberToken);
    }

    /**
     * 根据id获取问答详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/online-question-answer-infos")
    public Result getDetail(String id){
        return onlineQuestionAnswerService.getOnline(id);
    }
}
