package com.aidianmao.controller;

import com.aidianmao.entity.TmallOrder;
import com.aidianmao.service.TmallOrderService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TmallOrderController {

    @Autowired
    private TmallOrderService tmallOrderService;

    /**
     * 分页条件查询订单
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params 参数map
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return tmallOrderService.pageList(pageNum,pageSize,params);
    }

    /**
     * 买家下订单
     * @param tmallId 店铺id
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-orders")
    public Result placingOrder(String tmallId, @CookieValue String aidianmaoMemberToken) {
        return tmallOrderService.placingOrder(tmallId,aidianmaoMemberToken);
    }

    /**
     * 买家支付订单
     * @param tmallOrderId     天猫订单id
     * @param aidianmaoMemberToken  用户
     * @param payPassword   支付密码
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-orders/pay")
    public Result payOrder(String tmallOrderId, @CookieValue String aidianmaoMemberToken, String payPassword) {
        return tmallOrderService.payOrder(tmallOrderId,aidianmaoMemberToken,payPassword);
    }

    /**
     * 买家确认收货
     * @param tmallOrderId 订单id
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-orders/take-order")
    public Result takeOrder(String tmallOrderId, @CookieValue String aidianmaoMemberToken) {
        return tmallOrderService.takeOrder(tmallOrderId,aidianmaoMemberToken);
    }

    /**
     * 买家支付定金
     * @param tmallOrderId 天猫id
     * @param aidianmaoMemberToken  用户
     * @param payPassword 支付密码
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-orders/one")
    public Result payOrderOneStept(String tmallOrderId, @CookieValue String aidianmaoMemberToken, String payPassword) {
        return tmallOrderService.payOrderOneStept(tmallOrderId,aidianmaoMemberToken,payPassword);
    }

    /**
     * 卖家支付余款
     * @param tmallOrderId 天猫id
     * @param aidianmaoMemberToken  用户
     * @param payPassword 支付密码
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-orders/two")
    public Result payOrderTwoStept(String tmallOrderId, @CookieValue String aidianmaoMemberToken, String payPassword) {
        return tmallOrderService.payOrderTwoStept(tmallOrderId,aidianmaoMemberToken,payPassword);
    }

    /**
     * 分页查询我买到的网店
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param order    条件参数
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders/buy/pagination")
    public Result getList(Integer pageNum, Integer pageSize,  TmallOrder order,@CookieValue String aidianmaoMemberToken){
        return tmallOrderService.getList(pageNum,pageSize,order,aidianmaoMemberToken);
    }

    /**
     * 服务动态(最近的天猫店铺一条)
     * @return
     */
    @GetMapping(value = "v1/tmall-order/dynamic")
    public Result getDynamic(){
        return  tmallOrderService.getDynamic();
    }


    /**
     * 获取卖家订单/传参查询可操作订单，不传查询所有
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @param isSaleSure 卖家是否确认
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders-member")
    public Result getNoPayOrder( @CookieValue String aidianmaoMemberToken,Integer pageNum, Integer pageSize,String isSaleSure){
        return tmallOrderService.getSaleOrder(aidianmaoMemberToken,pageNum,pageSize,isSaleSure);
    }

    /**
     * 获取卖家所有订单
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders-member-all")
    public Result getAllSaleOrder(@CookieValue String aidianmaoMemberToken,Integer pageNum, Integer pageSize){
        return tmallOrderService.getAllSaleOrder(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 获取买家可操作订单
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-orders-buyer")
    public Result getBuyOrder(@CookieValue String aidianmaoMemberToken,Integer pageNum, Integer pageSize){
        return tmallOrderService.getBuyOrder(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 根据主键id查询订单详情
     * @param id
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-order-details")
    public Result getDetail(String id){
        return tmallOrderService.getOrderDetail(id);
    }

    /**
     * 个人中心首页可操作订单分页查询
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-order-lists")
    public Result getList(@CookieValue String aidianmaoMemberToken,Integer pageNum, Integer pageSize){
        return tmallOrderService.getDealOrder(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 取消订单
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders")
    public Result cancleOrder(String id,@CookieValue String aidianmaoMemberToken){
        return tmallOrderService.cancelOrder(id,aidianmaoMemberToken);
    }

    /**
     * 卖家确认
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders-sale")
    public Result sureOrder(String id,@CookieValue String aidianmaoMemberToken){
        return tmallOrderService.sureOrder(id,aidianmaoMemberToken);
    }

    /**
     * 卖家再次确认，订单状态至交接中
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-orders-sale-two")
    public Result sureOrderTwo(String id,@CookieValue String aidianmaoMemberToken){
        return tmallOrderService.sureToConnect(id,aidianmaoMemberToken);
    }

}
