package com.aidianmao.controller;

import com.aidianmao.service.WithdrawFreezeDetailService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WithdrawFreezeDetailController {

    @Autowired
    private WithdrawFreezeDetailService service;

    /**
     * 冻结明细详情
     * @param aidianmaoMemberToken  用户
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/freeze-details")
    public Result getFreezeDetail(@CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize){
        return service.getFreezeDetail(aidianmaoMemberToken,pageNum,pageSize);
    }
}
