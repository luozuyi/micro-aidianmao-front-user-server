package com.aidianmao.controller;

import com.aidianmao.service.MemberBrowseHistoryService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberBrowseHistoryController {

    @Autowired
    private MemberBrowseHistoryService service;

    /**
     * 获取浏览历史记录
     * @return
     */
    @GetMapping(value = "v1/historys")
    public Result getHistory(){
        return service.getMemberBrowseHistory();
    }


    /**
     * 添加浏览记录
     * @param tmallId           天猫id
     * @return
     */
    @PostMapping(value="v1/historys")
    public Result addHistory(String tmallId){
        return service.addMemberBrowseHistory(tmallId);
    }
}
