package com.aidianmao.controller;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Tmall;
import com.aidianmao.entity.TmallCollect;
import com.aidianmao.entity.TmallLess;
import com.aidianmao.service.TmallService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TmallController {
    @Autowired
    private TmallService tmallService;

    /**
     *新增店铺接口
     * @param tmall 店铺对象
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @PostMapping(value = "v1/auth/tmalls")
    public Result add(Tmall tmall, @CookieValue String aidianmaoMemberToken, ProvincesCitysCountrys provincesCitysCountrys) {
        return tmallService.add(tmall,aidianmaoMemberToken,provincesCitysCountrys);
    }

    /**
     * 首页多条件查询天猫店铺列表
     * @param tmallLess     天猫店铺部分信息对象
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/tmalls-list")
    public Result pageList(TmallLess tmallLess,String pageNum,String pageSize){
        return tmallService.selectTmallSelective(tmallLess,pageNum,pageSize);
    }



    /**
     * 查询审核成功上架最近的4个天猫店铺
     * @return
     */
    @GetMapping(value = "v1/tmalls-list-four")
    public Result pageList(){
        return tmallService.selectFourTmall();
    }

    /**
     * 根据主键id获取天猫店铺详情
     * @param id   天猫id
     * @return
     */
    @GetMapping(value = "v1/tmalls-detail")
    public Result tmallDetail(String id){
        return tmallService.TmallDetail(id);
    }

    /**
     * 修改店铺信息
     * @param tmall
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmalls-detail")
    public Result updateTmall(Tmall tmall,@CookieValue String aidianmaoMemberToken){
        return tmallService.updateTmall(tmall,aidianmaoMemberToken);
    }
    /**
     * 根据用户id查询用户出售店铺
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls-info")
    public Result userTmall(@CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize,Tmall tmall){
        return tmallService.getUserTmall(aidianmaoMemberToken,pageNum,pageSize,tmall);
    }

    /**
     * 获取用户收藏的天猫店铺
     * @param aidianmaoMemberToken  用户
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-collects")
    public Result getTmallCollect(@CookieValue String aidianmaoMemberToken,Integer pageNum,Integer pageSize){
        return tmallService.getMemberCollect(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 收藏店铺
     * @param tmallCollect      天猫收藏对象
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-collects")
    public Result addTmallCollect(TmallCollect tmallCollect, @CookieValue String aidianmaoMemberToken){
        return tmallService.addMemberCollect(tmallCollect,aidianmaoMemberToken);
    }


    /**
     * 删除收藏的网店
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-collects")
    public Result deleteTmallCollect(String id,@CookieValue String aidianmaoMemberToken){
        return tmallService.deleteMemberCollect(id,aidianmaoMemberToken);
    }

    /**
     * 判断用户是否收藏了该店铺
     * @param tmallId
     * @param aidianmaoMemberToken
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls-collect")
    public Result isCollect(String tmallId,@CookieValue String aidianmaoMemberToken){
        return tmallService.isCollect(tmallId,aidianmaoMemberToken);
    }

    /**
     * 下架店铺
     * @param tmallId       天猫id
     * @return
     */
    @PatchMapping(value = "v1/auth/tmalls")
    public Result delete(String tmallId) {
        return tmallService.deleteTmall(tmallId);
    }


    /**
     * 智能提示店铺名称
     * @param shopName
     * @return
     */
    @GetMapping(value = "v1/auth/tmalls-name")
    public Result getShopName(String shopName){
        return tmallService.selectShopName(shopName);
    }
}
