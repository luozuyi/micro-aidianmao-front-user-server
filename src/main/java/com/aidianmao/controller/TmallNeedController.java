package com.aidianmao.controller;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.TmallNeed;
import com.aidianmao.service.TmallNeedService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class TmallNeedController {
    @Autowired
    private TmallNeedService tmallNeedService;

    /**
     *  发布求购
     * @param tmallNeed  求购对象
     * @param aidianmaoMemberToken   用户
     * @return
     */
    @PostMapping(value = "v1/auth/tmall-needs")
    public Result addTmallNeed(TmallNeed tmallNeed, @CookieValue String aidianmaoMemberToken, ProvincesCitysCountrys provincesCitysCountrys){
        return tmallNeedService.addTmallNeed(tmallNeed,aidianmaoMemberToken,provincesCitysCountrys);
    }


    /**
     *  分页条件查询求购信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params
     * @return
     */
    @GetMapping(value = "v1/tmall-needs/pagination")
    public Result pageTmallNeedList(String pageNum, String pageSize, @RequestParam Map<String, Object> params){
        return tmallNeedService.pageTmallNeedList(pageNum,pageSize,params);
    }

    /**
     * 分页查询用户的求购信息
     * @param pageNum   当前页
     * @param pageSize 一页显示多少条
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @GetMapping(value = "v1/auth/tmall-needs")
    public Result getUserTmallNeed(Integer pageNum, Integer pageSize,@CookieValue String aidianmaoMemberToken){
        return tmallNeedService.getUserTmallNeed(pageNum,pageSize,aidianmaoMemberToken);
    }

    /**
     * 根据主键id获取天猫求购详情
     * @param id  主键id
     * @return
     */
    @GetMapping(value = "v1/tmall-needs")
    public Result getTmallNeed(String id){
        return tmallNeedService.getTmallNeed(id);
    }

    /**
     * 修改求购信息
     * @param tmallNeed
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-needs")
    public Result updateTmallNeed(TmallNeed tmallNeed){
        return tmallNeedService.updateTmallNeed(tmallNeed);
    }


    /**
     * 下架求购
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/tmall-needs-no")
    public Result deleteTmallNeed(String id,@CookieValue String aidianmaoMemberToken){
        return tmallNeedService.deleteTmallNeed(id,aidianmaoMemberToken);
    }
}
