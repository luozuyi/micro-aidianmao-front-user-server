package com.aidianmao.controller;

import com.aidianmao.service.AdvertiseService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdvertiseController {
    @Autowired
    private AdvertiseService advertiseService;
    /**
     * 根据广告位类型去获取广告位的图片路径
     * @param type   广告类型
     * @return
     */
    @GetMapping(value = "v1/advertise-sources")
    public Result selectSourceByType(String type){
        return advertiseService.selectSourceByType(type);
    }
}
