package com.aidianmao.controller;


import com.aidianmao.entity.BarginPrice;
import com.aidianmao.service.BarginPriceService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Map;

@RestController
public class BarginPriceController {
    @Autowired
    private BarginPriceService barginPriceService;

    /**
     *  登陆后添加砍价
     *  @param price 期望价格
     *  @param phone 电话号码
     *  @param tmallId 店铺id
     *  @param aidianmaoMemberToken 砍价会员
     *  @return
     */
    @PostMapping(value = "v1/auth/bargin-prices" )
    public Result addBarginPrice(BigDecimal price,String phone, String tmallId, @CookieValue String aidianmaoMemberToken){
        return barginPriceService.addBarginPrice(price,phone,tmallId,aidianmaoMemberToken);
    }

    /**
     *  未登陆后添加砍价
     *  @param price 期望价格
     *  @param phone 电话号码
     *  @param tmallId 店铺id
     *  @return
     * */
    @PostMapping(value = "v1/bargin-prices" )
    public Result addBarginprice(BigDecimal price,String phone, String tmallId){
        return barginPriceService.addBarginprice(price,phone,tmallId);
    }

    /**
     *  查询砍价
     *  @return
     * */
    @GetMapping(value = "v1/bargin-prices")
    public Result getList(){
        return barginPriceService.getList();
    }

    /**
     * 分页查询砍价列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/bargin-prices/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params){
        return barginPriceService.pageList(pageNum,pageSize,params);
    }
}
