package com.aidianmao.controller;

import com.aidianmao.utils.VerifyCodeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
public class VerifyCodeController {
	private String codeLength="4";  
    
    private String width="82";  
      
    private String height="38";

    /**
     * 图片验证码
     * @param request
     * @param response
     * @throws Exception
     */
    @GetMapping(value = "v1/verify-code/generate")
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {  
          
        // 设置  
        response.setDateHeader("Expires", 0);  
        // Set standard HTTP/1.1 no-cache headers.  
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");  
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).  
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");  
        // Set standard HTTP/1.0 no-cache header.  
        response.setHeader("Pragma", "no-cache");  
        // return a jpeg  
        response.setContentType("image/jpeg");  
          
        String verifyCode = VerifyCodeUtils.generateVerifyCode(Integer.parseInt(codeLength));
          
        request.getSession().setAttribute("validateCode", verifyCode);  
          
        VerifyCodeUtils.outputImage(Integer.parseInt(width), Integer.parseInt(height), response.getOutputStream(), verifyCode);
    }  
}
