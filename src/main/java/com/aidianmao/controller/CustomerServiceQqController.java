package com.aidianmao.controller;

import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerServiceQqController {
    @Autowired
    private CustomerServiceQqService customerServiceQqService;

    /**
     * 联系客服(登陆)
     * @param type 联系的组
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    @GetMapping(value = "v1/auth/customer-service-qqs")
    public Result contactQqLand(String type, @CookieValue String aidianmaoMemberToken){
        return customerServiceQqService.contactQqLand(type,aidianmaoMemberToken);
    }

    /**
     * 联系客服(未登录)
     * @param type 联系的组
     * @return
     */
    @GetMapping(value = "v1/customer-service-qqs")
    public Result contactQq(String type){
        return customerServiceQqService.contactQq(type);
    }

    /**
     * 获取天猫特服qq(登录)
     * @return
     * @param aidianmaoMemberToken 凭据
     */
    @GetMapping(value = "v1/auth/tmall-customer-service-qqs")
    public Result getTmallCustomerServerQQ( @CookieValue String aidianmaoMemberToken){
        return customerServiceQqService.contactQqLand("1",aidianmaoMemberToken);
    }

    /**
     * 获取天猫特服qq(未登录)
     * @return
     */
    @GetMapping(value = "v1/tmall-customer-service-qqs")
    public Result getTmallCustomerServerQQLand(){
        return customerServiceQqService.contactQq("1");
    }
}
