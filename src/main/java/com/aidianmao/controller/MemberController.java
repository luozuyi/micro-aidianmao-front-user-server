package com.aidianmao.controller;

import com.aidianmao.service.MemberService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MemberController {
    @Autowired
    private MemberService memberService;

    /**
     * 会员注册
     * @param telephone 手机号
     * @param password 密码
     * @param surePassword 确认密码
     * @param telePhoneCode 手机验证码
     * @param userName 用户名
     * @return
     */
    @PostMapping(value = "v1/members/regist")
    public Result regist(String telephone, String password, String surePassword, String telePhoneCode, String userName) {
        return memberService.regist(telephone, password, surePassword, telePhoneCode, userName);
    }

    /**
     * 手机号校验会员是否已经被注册
     * @param telephone   手机号
     * @return
     */
    @GetMapping(value = "v1/members/telephone-check")
    public Result telephoneCheck(String telephone) {
        return memberService.telephoneCheck(telephone);
    }

    /**
     * 用户名校验是否已经被注册
     * @param userName      用户名
     * @return
     */
    @GetMapping(value = "v1/members/user-name-check")
    public Result userNameCheck(String userName) {
        return memberService.userNameCheck(userName);
    }

    /**
     * 根据id查询用户资金详情
     * @param aidianmaoMemberToken   用户
     * @return
     */
    @GetMapping(value = "v1/auth/members/money-detail")
    public Result getMoneyDetail(@CookieValue String aidianmaoMemberToken){
        return memberService.moneyDetail(aidianmaoMemberToken);
    }

    /**
     * 修改密码
     * @param oldPassword  旧密码
     * @param newPassword   新密码
     * @param newPasswordtwo    第二次输入新密码
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/members")
    public Result modifyPassword(String oldPassword,String newPassword,String newPasswordtwo,@CookieValue String aidianmaoMemberToken){
        return memberService.modifyPassword(oldPassword,newPassword,newPasswordtwo,aidianmaoMemberToken);
    }


    /**
     * 基本信息修改
     * @param newIdCard   新身份证号
     * @param newrealName    新名字
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/member-identitys")
    public Result updateMember(String newIdCard,String newrealName,@CookieValue String aidianmaoMemberToken){
        return memberService.updateMember(newIdCard,newrealName,aidianmaoMemberToken);
    }


    /**
     * 修改支付密码
     * @param verification  验证码
     * @param newPayPassword    新支付密码
     * @param newPayPasswordTwo     再次输入新支付密码
     * @param aidianmaoMemberToken
     * @return
     */
    @PatchMapping(value = "v1/auth/members-paypassword")
    public Result updatePayPassword(String verification,String newPayPassword,String newPayPasswordTwo,@CookieValue String aidianmaoMemberToken){
        return memberService.updatePayPassword(verification,newPayPassword,newPayPasswordTwo,aidianmaoMemberToken);
    }


    /**
     * 找回密码
     * @param phone  手机号
     * @param verification  验证码
     * @param newPassword    新密码
     * @param newPasswordTwo     再次输入密码
     * @return
     */
    @PostMapping(value = "v1/members-retrieve-phone")
    public Result retrievePhone(String phone,String verification,String newPassword,String newPasswordTwo){
        return memberService.retrievePhone(phone,verification,newPassword,newPasswordTwo);
    }
}
