package com.aidianmao.controller;

import com.aidianmao.service.BankService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankController {
    @Autowired
    private BankService bankService;

    /**
     * 查询银行列表
     * @return
     */
    @GetMapping(value = "v1/banks/list")
    public Result list() {
        return bankService.list();
    }
}
