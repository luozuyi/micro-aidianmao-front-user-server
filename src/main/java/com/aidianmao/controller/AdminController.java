package com.aidianmao.controller;

import com.aidianmao.service.AdminService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

    @Autowired
    private AdminService service;

    /**
     * 校验真假客服
     * @param qq  校验qq
     * @return
     */
    @GetMapping(value = "v1/customer-confirmations")
    public Result checkCustomer(String qq){
        return service.checkCustomer(qq);
    }
}
