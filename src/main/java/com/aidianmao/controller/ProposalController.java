package com.aidianmao.controller;

import com.aidianmao.entity.Proposal;
import com.aidianmao.service.ProposalService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProposalController {

    @Autowired
    private ProposalService service;

    /**
     * 获取我的建议并分页
     * @param aidianmaoMemberToken      用户
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/proposals-list")
    public Result getList(@CookieValue String aidianmaoMemberToken, Integer pageNum, Integer pageSize){
        return service.getProposals(aidianmaoMemberToken,pageNum,pageSize);
    }

    /**
     * 我要提议
     * @param proposal  提议对象
     * @param aidianmaoMemberToken  用户
     * @return
     */
    @PostMapping(value = "v1/auth/proposals")
    public Result addProposal(Proposal proposal,@CookieValue String aidianmaoMemberToken){
        return service.addProposal(proposal,aidianmaoMemberToken);
    }
}
