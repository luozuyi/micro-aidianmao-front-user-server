package com.aidianmao.controller;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.service.AssessTmallService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class AssessTmallController {
    @Autowired
    private AssessTmallService assessTmallService;

    /**
     * 添加估价
     *
     * @param assessTmall          估价对象
     * @return
     */
    @PostMapping(value = "v1/auth/asses-tmalls")
    public Result addAssessTmall(AssessTmall assessTmall,@CookieValue String aidianmaoMemberToken) {
        return assessTmallService.addAssessTmall(assessTmall,aidianmaoMemberToken);
    }


    /**
     * 分页查询充值列表
     *
     * @param pageNum  当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    @GetMapping(value = "v1/auth/assess-tmalls/pagination")
    public Result pageList(Integer pageNum, Integer pageSize, @RequestParam Map<String, Object> params) {
        return assessTmallService.pageList(pageNum, pageSize, params);
    }

    /**
     * 获取总估计数量
     * @return
     */
    @GetMapping(value = "v1/assess-tmalls")
    public Result getCount(){
        return assessTmallService.getCount();
    }
}
