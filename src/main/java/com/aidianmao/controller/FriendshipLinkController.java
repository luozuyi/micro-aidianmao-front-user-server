package com.aidianmao.controller;

import com.aidianmao.service.FriendshipLinkService;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author W1665
 * @date 2018/12/14
 */
@RestController
public class FriendshipLinkController {

    @Autowired
    private FriendshipLinkService friendshipLinkService;


    /**
     * 友情链接获取列表
     * @return
     */
    @GetMapping(value = "v1/friendship-links")
    public Result getList(){
        return friendshipLinkService.getFriendshipLinkList();
    }
}
