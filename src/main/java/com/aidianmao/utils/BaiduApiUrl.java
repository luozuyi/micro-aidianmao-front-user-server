package com.aidianmao.utils;

public class BaiduApiUrl {
    /**
     * 通用票据接口
     */
    public static String RECEIPT = "https://aip.baidubce.com/rest/2.0/ocr/v1/receipt";
    /**
     * 通用文字识别
     */
    public static String WORDS = "https://aip.baidubce.com/rest/2.0/ocr/v1/general_basic";
}
