package com.aidianmao.utils;

import com.squareup.okhttp.*;
import java.io.IOException;

public class HttpClientUtil {

    public static final MediaType JSON= MediaType.parse("application/json; charset=utf-8");

    /**
     *
     * @param url
     * @param body
     * @return
     */
    public static String dataExchange(String url,RequestBody body) {
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().url(url).post(body).build();
        try {
            Response response = httpClient.newCall(request).execute();
            String strBody = response.body().string();
            return  strBody;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param url
     * @param json
     * @return
     */
    public static String dataExchange(String url,String json) {
        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        try {
            Response response = httpClient.newCall(request).execute();
            String strBody = response.body().string();
            return  strBody;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}
