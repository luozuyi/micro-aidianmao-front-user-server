package com.aidianmao.utils;




import com.aidianmao.sms.AppConfig;
import com.aidianmao.sms.ConfigLoader;
import com.aidianmao.sms.MESSAGEXsend;
import org.apache.commons.lang.StringUtils;

public class SMSSendUtil {
    public static void send(String phone, String code, String time) {
        if (StringUtils.isBlank(phone) || StringUtils.isBlank(code) || StringUtils.isBlank(time)) {
            return;
        }

        AppConfig config = ConfigLoader.load(ConfigLoader.ConfigType.Message);
        MESSAGEXsend submail = new MESSAGEXsend(config);
        submail.addTo(phone);
        submail.setProject("ydrny1");
        submail.addVar("code", code);
        submail.addVar("time", time);
        submail.xsend();
    }
}
