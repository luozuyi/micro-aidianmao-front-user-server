package com.aidianmao.utils;

public abstract class Constants {
	
	/**
	 * 错误
	 */
	public static String ERROR = "-2";
	/**
	 * 失败
	 */
	public static String FAIL = "-1";
	/**
	 * 成功
	 */
	public static String SUCCESS = "0";
	/**
	 * UTF-8编码
	 */
	public static final String UTF8 = "UTF-8";
	
	/**
	 * fastdfs server ip
	 */
	public static final String FASTDFS_PATH = "fastdfsIp";
	/**
	 * fastdfs server ip address
	 */
	public static final String FASTDFS_SERVER_IP = "http://118.31.46.156";
	/**
	 * elasticsearch server ip address
	 */
	public static final String ELASTICSEARCH_SERVER_IP = "http://47.96.1.66";
	
	public static final String ACTIVITY_IP = "http://47.96.1.66:8080";

	public static final String PICTUREEXTENTION = "JPG,jpg,PNG,png";

	/**
	 * 章节索引
	 */
	public static final String BOOKCHAPTERDIRECTORYINDEX = "chapterDirectory";
	/**
	 * 所有内容
	 */
	public static final String BOOKCONTENT = "content";
	/**
	 * 书籍来源
	 */
	public enum BookSouce{
		UPLOAD("1","上传"), SPIDER("2","爬虫"),
		PUBLISH("3","出版"), NONE("4","无");
		private String source ;
		private String info;

		BookSouce(String source, String info) {
			this.source = source;
			this.info = info;
		}

		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}
	public enum BookStatus{
		WAITAUDIT("0","待审核"),
		NOPASS("1","不通过"),
		HASUP("2","已上架"),
		HASDOWN("3","已下架");


		private String status ;
		private String info;

		BookStatus(String status, String info) {
			this.status = status;
			this.info = info;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	public enum AdminIsDisable{
		DISABLE("1","是"),
		UNDISABLE("0","否");
		private String isDisable;
		private String info;

		AdminIsDisable(String isDisable, String info) {
			this.isDisable = isDisable;
			this.info = info;
		}

		public String getIsDisable() {
			return isDisable;
		}

		public void setIsDisable(String isDisable) {
			this.isDisable = isDisable;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}


	/**
	 * 广告是否禁用
	 */
	public enum AdvertiseIsForbidden{
		UNFORBIDDEN("0","不禁用"),
		FORBIDDEN("1","禁用");

		private String isForbidden;
		private String info;

		AdvertiseIsForbidden(String isForbidden, String info) {
			this.isForbidden = isForbidden;
			this.info = info;
		}

		public String getIsForbidden() {
			return isForbidden;
		}

		public void setIsForbidden(String isForbidden) {
			this.isForbidden = isForbidden;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否退还消费者保障金
	 */
	public enum IsReturnDeposit{
		RETURN("0","需要退还"),
		UNRETURN("1","不需要退还")
		;
		private String isReturn;
		private String info;

		IsReturnDeposit(String isReturn, String info) {
			this.isReturn = isReturn;
			this.info = info;
		}

		public String getIsReturn() {
			return isReturn;
		}

		public void setIsReturn(String isReturn) {
			this.isReturn = isReturn;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否退还技术年费
	 */
	public enum IsReturnTechServiceFee{
		RETURN("0","需要退还"),
		UNRETURN("1","不需要退还")
		;
		private String isReturn;
		private String info;
		IsReturnTechServiceFee(String isReturn, String info) {
			this.isReturn = isReturn;
			this.info = info;
		}

		public String getIsReturn() {
			return isReturn;
		}

		public void setIsReturn(String isReturn) {
			this.isReturn = isReturn;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 商城类型
	 */
	public enum ShopType{
		FLAGSHIPSTORE("0","旗舰店"),
		MONOPOLYSTORE("1","专营店"),
		MONOSALESTORE("2","专卖店")
		;
		private String type;
		private String info;

		ShopType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 商标类型
	 */
	public enum TrademarkType{
		R("0","R标"),
		TM("1","TM标")
		;
		private String type;
		private String info;

		TrademarkType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否带货
	 */
	public enum IsCarryGoods{
		CARRY("0","带货"),
		UNCARRY("1","不带货")
		;
		private String type;
		private String info;

		IsCarryGoods(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 纳税人性质
	 */
	public enum TaxPayerType{
		GENERAL("0","一般纳税人"),
		SMALLSCALE("1","小规模纳税人");
		private String type;
		private String info;

		TaxPayerType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否贷款
	 */
	public enum IsLoan{
		YES("0","有贷款"),
		NO("1","没贷款");
		private String type;
		private String info;

		IsLoan(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否完成营业额指标
	 */
	public enum IsCompletedTurnoverIndex{
		YES("0","完成"),
		NO("1","没有完成");
		private String type;
		private String info;

		IsCompletedTurnoverIndex(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 行业类型
	 */
	public enum IndustryType{
		TYPE0("0","服饰鞋包"),
		TYPE1("1","美容护理"),
		TYPE2("2","母婴用品"),
		TYPE3("3","3C数码类"),
		TYPE4("4","运动/户外"),
		TYPE5("5","家装家饰"),
		TYPE6("6","家居用品"),
		TYPE7("7","食品/保健"),
		TYPE8("8","珠宝/首饰"),
		TYPE9("9","游戏/话费"),
		TYPE10("10","生活服务"),
		TYPE11("11","汽车配件"),
		TYPE12("12","书籍音像"),
		TYPE13("13","玩乐/收藏"),
		TYPE14("14","万用百搭"),
		TYPE15("15","其他行业"),
		TYPE16("16","医药健康"),
		TYPE17("17","大家电")
		;
		private String type;
		private String info;

		IndustryType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 当前主营行业
	 */
	public enum CurrentMainCamp{
		TYPE0("0","服饰鞋包"),
		TYPE1("1","美容护理"),
		TYPE2("2","母婴用品"),
		TYPE3("3","3C数码类"),
		TYPE4("4","运动/户外"),
		TYPE5("5","家装家饰"),
		TYPE6("6","家居用品"),
		TYPE7("7","食品/保健"),
		TYPE8("8","珠宝/首饰"),
		TYPE9("9","游戏/话费"),
		TYPE10("10","生活服务"),
		TYPE11("11","汽车配件"),
		TYPE12("12","书籍音像"),
		TYPE13("13","玩乐/收藏"),
		TYPE14("14","万用百搭"),
		TYPE15("15","其他行业"),
		TYPE16("16","医药健康"),
		TYPE17("17","大家电")
		;
		private String type;
		private String info;

		CurrentMainCamp(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否可以过户
	 */
	public enum IsTransfer{
		YES("0","可以过户"),
		NO("1","不可以过户")
		;
		private String type;
		private String info;

		IsTransfer(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 是否有债务纠纷
	 */
	public enum IsDisputeOverObligation{
		YES("1","有债务纠纷"),
		NO("0","没有债务纠纷")
		;
		private String type;
		private String info;

		IsDisputeOverObligation(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 店铺资质
	 */
	public enum ShopQualification{
		TYPE1("1","正品保障"),
		TYPE2("2","消费者保障"),
		TYPE3("3","七天退换")
		;
		private String type;
		private String info;

		ShopQualification(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 可以提供的证件
	 */
	public enum Credentials{
		TYPE1("1","身份证"),
		TYPE2("2","手持身份证"),
		TYPE3("3","营业执照"),
		TYPE4("4","手持营业执照")
		;
		private String type;
		private String info;

		Credentials(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * TM标是否入品牌库
	 */
	public enum IsTmTrademarkAddBrand{
		YES("0","是"),
		NO("1","否"),
		;
		private String type;
		private String info;

		IsTmTrademarkAddBrand(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 店铺在平台的状态
	 */
	public enum Status{
		ADUITING("0","审核中"),
		ADUITPASS("1","审核通过"),
		ADUITNOPASS("2","审核不通过"),
		DOWN("3","下架"),
		PREPAY("4","已付定金"),
		HASPAY("5","已付全款"),
		TRANSFERING("6","交接中"),
		HASSALE("7","已售出")
		;
		private String type;
		private String info;

		Status(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 商标所属人
	 */
	public enum TrademarkOwner{
		TYPE0("0","个人"),
		TYPE1("1","公司"),
		;
		private String type;
		private String info;

		TrademarkOwner(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 订单状态
	 */
	public enum OrderStatus{
		TYPE0("0","待付款"),
		TYPE1("1","已付定金"),
		TYPE2("2","已付全款(补齐余款)"),
		TYPE3("3","交易成功"),
		TYPE4("4","已取消"),
		TYPE5("5","已终止"),
		TYPE6("6","异常订单(无资金调整)"),
		TYPE7("7","异常订单(有资金调整)"),
		TYPE8("8","交接中")
		;
		private String type;
		private String info;

		OrderStatus(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 订单冻结状态
	 */
	public enum TmallOrderFreezeStatus{
		TYPE0("0","冻结中"),
		TYPE1("1","已解冻")
		;
		private String type;
		private String info;

		TmallOrderFreezeStatus(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 收支明细类型
	 */
	public enum IncomeExpensesType{
		TYPE0("0","收入"),
		TYPE1("1","支出")
		;
		private String type;
		private String info;

		IncomeExpensesType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 明细场景
	 */
	public enum IncomeExpensesMoneyScene{
		TYPE0("0","充值"),
		TYPE1("1","佣金返利"),
		TYPE2("2","差价返利"),
		TYPE3("3","违约定金返还"),
		TYPE4("4","违约金调整返还"),
		TYPE5("5","支付余额返还"),
		TYPE6("6","全款购买"),
		TYPE7("7","店铺余额"),
		TYPE8("8","定金"),
		TYPE9("9","违约金"),
		TYPE10("10","提现"),
		TYPE11("11","交易成功"),
		TYPE12("12","违约全款返还"),
		TYPE13("13","差价返利扣除"),
		TYPE14("14","充值调整"),
		TYPE16("16","提现手续费"),
		;
		private String type;
		private String info;

		IncomeExpensesMoneyScene(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 问题及对应编号
	 */
	public enum QuestionType{
		All("4","全部问题"),
		PRETRANSACTION("0","交易前"),
		INTRANSACTION("1","交易中"),
		AFTERTRANSACTION("2","交易后"),
		OTHERQUESTION("3","其他问题");


		private String type ;
		private String info;

		QuestionType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 *银行卡默认状态
	 */
	public enum MemberBankInfoIsDefault{
		TYPE0("0","默认"),
		TYPE1("1","不是默认")
		;
		private String type;
		private String info;

		MemberBankInfoIsDefault(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 提现状态
	 */
	public enum WithdrawDepositStatus{
		TYPE0("0","审核中"),
		TYPE1("1","打款中"),
		TYPE2("2","成功"),
		TYPE3("3","失败")
		;
		private String type;
		private String info;

		WithdrawDepositStatus(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 平台收支场景
	 */
	public enum PlatformFundDetailMoneyScene{
		TYPE0("0","tmall订单交易"),
		TYPE1("1","提现")
		;
		private String type;
		private String info;

		PlatformFundDetailMoneyScene(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}
	/**
	 * 平台收支类型
	 */
	public enum PlatformFundDetailType{
		TYPE0("0","收入"),
		TYPE1("1","支出")
		;
		private String type;
		private String info;

		PlatformFundDetailType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}

	/**
	 * 平台收支关联类型
	 */
	public enum PlatformFundDetailRelationType{
		TYPE0("0","订单id"),
		TYPE1("1","提现id")
		;
		private String type;
		private String info;

		PlatformFundDetailRelationType(String type, String info) {
			this.type = type;
			this.info = info;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getInfo() {
			return info;
		}

		public void setInfo(String info) {
			this.info = info;
		}
	}
	public static void main(String[] args) {
		for (IndustryType i:IndustryType.values()) {
			System.out.println(i.getType());
		}
	}
}
