package com.aidianmao.service;

import com.aidianmao.utils.Result;

/**
 *
 * @author Catch22
 * @date 2018年6月21日
 */
public interface CitysService extends BaseService {

    /**
     * 根据省id获取所有城市
     * @param provincesId
     * @return
     */
    Result getAllCity(String provincesId);
}
