package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface AdminService {

    /**
     * 校验客服qq真假
     * @param qq
     * @return
     */
    Result checkCustomer(String qq);
}
