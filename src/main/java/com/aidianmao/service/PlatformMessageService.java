package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface PlatformMessageService {

    /**
     * 查询站内信
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getList(String aidianmaoMemberToken,String status,Integer pageNum,Integer pageSize);

    /**
     * 根据主键id获取站内信内容
     * @param id
     * @return
     */
    Result getPlatformMessage(String id);

    /**
     * 批量修改站内信的状态
     * @param ids
     * @return
     */
    Result updateAll(String[] ids,String aidianmaoMemberToken);

    /**
     * 删除站内信
     * @param ids
     * @return
     */
    Result delete(String[] ids,String aidianmaoMemberToken);
}
