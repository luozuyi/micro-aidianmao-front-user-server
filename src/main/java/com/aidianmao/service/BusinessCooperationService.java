package com.aidianmao.service;

import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.utils.Result;

import java.util.Date;

public interface BusinessCooperationService {

    /**
     * 提交合作
     * @param citysName         城市名
     * @param industry          行业
     * @param companyName       公司名
     * @param phone             手机号
     * @param ipAddress         ip地址
     * @return
     */
    Result addBusinessCooperation(String citysName, String industry, String companyName, String phone, String ipAddress);
}
