package com.aidianmao.service;

import com.aidianmao.entity.Alipay;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AlipayService {
    /**
     *  添加充值
     *  @param alipay 充值对象
     *  @param aidianmaoMemberToken
     *  @return
     */
    Result addAlipay(Alipay alipay, String aidianmaoMemberToken);


    /**
     *  查询充值
     *  @return
     */
    Result getList();

    /**
     * 分页查询充值列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Alipay alipay,String aidianmaoMemberToken);

    /**
     * 根据memberId及其他条件动态筛选查询充值记录
     * @param alipay
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getByMemberId(Alipay alipay,String aidianmaoMemberToken,Integer pageNum, Integer pageSize);
}
