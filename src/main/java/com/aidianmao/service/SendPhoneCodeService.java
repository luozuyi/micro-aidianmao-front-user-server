package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface SendPhoneCodeService {
    /**
     * 获取注册时的手机验证码
     * 注册第一步
     * @param telephone
     * @return
     */
    Result getRegistTelephoneCode(String telephone,String captcha);

    /**
     * 会员修改支付密码时发送短信验证码
     * @param phone
     * @return
     */
    Result getModifyPhoneCode(String phone);

    /**
     * 找回密码
     * @param phone
     * @return
     */
    Result getRetrievePhoneCode(String phone,String verify);

    /**
     * 人机验证发送短信
     * @param phone
     * @param captcha
     * @return
     */
    Result sendPhoneCode(String phone,String captcha);
}
