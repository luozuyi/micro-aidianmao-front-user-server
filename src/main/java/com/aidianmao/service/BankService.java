package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface BankService {
    /**
     * 查询所有银行
     * @return
     */
    Result list();
}
