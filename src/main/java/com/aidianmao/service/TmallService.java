package com.aidianmao.service;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Tmall;
import com.aidianmao.entity.TmallCollect;
import com.aidianmao.entity.TmallLess;
import com.aidianmao.utils.Result;

public interface TmallService {
    /**
     * 出售店铺提交
     * @param tmall 天猫店铺
     * @param aidianmaoMemberToken 凭据
     * @param provincesCitysCountrys 省市区对象
     * @return
     */
    Result add(Tmall tmall,String aidianmaoMemberToken,ProvincesCitysCountrys provincesCitysCountrys);


    /**
     * 多条件筛选查询天猫店铺
     * @param tmallLess
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result selectTmallSelective(TmallLess tmallLess,String pageNum,String pageSize);



    /**
     * 根据id获取天猫店铺详情
     * @param id
     * @return
     */
    Result TmallDetail(String id);

    /**
     * 修改天猫店铺详情
     * @param tmall
     * @return
     */
    Result updateTmall(Tmall tmall,String memberId);

    /**
     * 根据用户id查询所出售店铺
     * @param memberId
     * @return
     */
    Result getUserTmall(String memberId,Integer pageNum,Integer pageSize,Tmall tmall);

    /**
     * 查询用户的收藏店铺
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getMemberCollect(String aidianmaoMemberToken,Integer pageNum, Integer pageSize);

    /**
     * 收藏店铺
     * @param tmallCollect
     * @param aidianmaoMemberToken
     * @return
     */
    Result addMemberCollect(TmallCollect tmallCollect,String aidianmaoMemberToken);

    /**
     * 删除收藏网店
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    Result deleteMemberCollect(String id,String aidianmaoMemberToken);

    /**
     * 查看店铺是否被当前用户收藏
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    Result isCollect(String id,String aidianmaoMemberToken);

    /**
     * 审核结果出现之前下架店铺
     * @param tmallId
     * @return
     */
    Result deleteTmall(String tmallId);

    /**
     * 查询审核成功上架最近的4个天猫店铺
     * @return
     */
    Result selectFourTmall();

    /**
     * 智能提示匹配输入框精确度最高的5条记录
     * @param shopName
     * @return
     */
    Result selectShopName(String shopName);

}
