package com.aidianmao.service;

import com.aidianmao.utils.Result;

/**
 * 省份
 * @author Catch22
 * @date 2018年6月21日
 */
public interface ProvincesService extends BaseService {

    /**
     * 获取所有省份
     * @return
     */
    Result getProvinces();
}
