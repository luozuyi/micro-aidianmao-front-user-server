package com.aidianmao.service;

import com.aidianmao.utils.Result;

import java.awt.image.RescaleOp;

public interface MemberBrowseHistoryService {

    /**
     * 获取看了又看(浏览记录)
     * @return
     */
    Result getMemberBrowseHistory();

    /**
     * 添加浏览记录
     * @param tmallId
     * @return
     */
    Result addMemberBrowseHistory(String tmallId);
}
