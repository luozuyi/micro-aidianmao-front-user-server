package com.aidianmao.service;

import com.aidianmao.entity.Proposal;
import com.aidianmao.utils.Result;

public interface ProposalService {

    /**
     * 我要建议
     * @param proposal
     * @param aidianmaoMemberToken
     * @return
     */
    Result addProposal(Proposal proposal,String aidianmaoMemberToken);

    /**
     * 获取我的建议并分页
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getProposals(String aidianmaoMemberToken,Integer pageNum,Integer pageSize);
}
