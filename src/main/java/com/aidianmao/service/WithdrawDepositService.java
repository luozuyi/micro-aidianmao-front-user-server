package com.aidianmao.service;

import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.entity.MemberBankInfo;
import com.aidianmao.entity.WithdrawDeposit;
import com.aidianmao.utils.Result;

public interface WithdrawDepositService {
    /**
     * 添加提现
     * @param withdrawDeposit 提现对象
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    Result add(WithdrawDeposit withdrawDeposit,String aidianmaoMemberToken);


    /**
     * 前台申请提现
     * @param money
     * @param payPassword
     * @param aidianmaoMemberToken
     * @return
     */
    Result frontWithdraw(String money,String payPassword,String aidianmaoMemberToken,String remark);

    /**
     * 多条件查询用户提现记录
     * @param withdrawDeposit
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getByMember(WithdrawDeposit withdrawDeposit, String aidianmaoMemberToken, Integer pageNum,Integer pageSize);

    /**
     * 查询用户对应的提现账户
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getWithdrawAccount(String aidianmaoMemberToken,Integer pageNum,Integer pageSize);

    /**
     * 新增提现账户信息
     * @param memberBankInfo
     * @param aidianmaoMemberToken
     * @return
     */
    Result addWithdrawAccountInfo(MemberBankInfo memberBankInfo,String aidianmaoMemberToken);

    /**
     * 修改为提现账户信息
     * @param memberBankInfo
     * @return
     */
    Result updateWithdrawAccountInfo(MemberBankInfo memberBankInfo,String aidianmaoMemberToken);

    /**
     * 设为默认提现账户
     * @param memberId
     * @return
     */
    Result setDefault(String memberId,String id);

    /**
     * 获取银行信息并分页
     * @return
     */
    Result getBankInfo();
}
