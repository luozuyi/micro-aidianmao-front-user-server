package com.aidianmao.service;

import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface OnlineQuestionAnswerService {

    /**
     * 添加问答
     * @param onlineQuestionAnswer 提问对象
     * @param aidianmaoMemberToken 提问的人
     * */
    Result addOnlineQuestionAnswer(OnlineQuestionAnswer onlineQuestionAnswer, String aidianmaoMemberToken);

    /**
     * 分页查询充值列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param flag 1：查询所有 2：查询当前用户
     * @return
     */
    Result pageList(Integer pageNum,Integer pageSize,String flag ,String aidianmaoMemberToken,String tradeType);

    /**
     * 您可能感兴趣的问题(解答时间由近及远8条)
     * @return
     */
    Result pageInterested();

    /**
     * 根据flag标志来查询提问数量
     * @param flag     1：查询所有 2：查询当前用户
     * @param memberId
     * @return
     */
    Result getCount(String flag,String memberId);

    /**
     * 根据id获取在线问答
     * @param id
     * @return
     */
    Result getOnline(String id);
}
