package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface CustomerServiceQqService {
    /**
     * 联系客服(登陆)
     * @param type 联系的组
     * @return
     */
    Result contactQqLand(String type,String aidianmaoMemberToken);

    /**
     * 联系客服(未登陆)
     * @param type 联系的组
     * @return
     */
    Result contactQq(String type);



}
