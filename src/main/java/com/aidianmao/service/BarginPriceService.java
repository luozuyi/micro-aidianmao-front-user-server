package com.aidianmao.service;

import com.aidianmao.entity.BarginPrice;
import com.aidianmao.utils.Result;

import java.math.BigDecimal;
import java.util.Map;

public interface BarginPriceService {

    /**
     *  添加砍价
     *  @param price 期望价格
     *  @param phone 电话号码
     *  @param tmallId 店铺id
     *  @param aidianmaoMemberToken 砍价会员
     *  @return
     */
    Result addBarginPrice(BigDecimal price, String phone, String tmallId, String aidianmaoMemberToken);

    /**
     *  未登陆后添加砍价
     *  @param price 期望价格
     *  @param phone 电话号码
     *  @param tmallId 店铺id
     *  @return
     * */
    Result addBarginprice(BigDecimal price,String phone, String tmallId);


    /**
     *  查询砍价
     *  @return
     * */
    Result getList();


    /**
     * 分页查询充值列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);
}
