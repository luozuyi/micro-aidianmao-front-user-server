package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface AdvertiseService {
    /**
     * 通过广告位类型去获取广告位的图片路径
     * @param type
     * @return
     */
    Result selectSourceByType (String type);
}
