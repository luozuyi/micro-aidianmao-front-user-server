package com.aidianmao.service;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.utils.Result;

import java.util.List;
import java.util.Map;

public interface AccountKeepingAgencyService {

    /**
     *  添加记账城市
     *  @return
     */
    Result addAccountKeepingAgencyCity();

    /**
     *  添加记账
     *  @param accountKeepingAgency 记账对象
     *  @return
     */
    Result addAccountKeepingAgency(AccountKeepingAgency accountKeepingAgency);

    /**
     * 分页查询所有记账
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 服务动态(最近的代理记账服务一条)
     * @return
     */

    Result getDynamic();


    List<String[]> getList();

}
