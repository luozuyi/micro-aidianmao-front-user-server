package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface WithdrawFreezeDetailService {

    /**
     * 根据memberId获取对应的冻结明细
     * @param aidianmaoMemberToken
     * @return
     */
    Result getFreezeDetail(String aidianmaoMemberToken,Integer pageNum,Integer pageSize);
}
