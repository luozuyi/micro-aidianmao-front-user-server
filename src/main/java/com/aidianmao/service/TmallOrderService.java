package com.aidianmao.service;

import com.aidianmao.entity.TmallOrder;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallOrderService {
    /**
     * 分页条件查询订单
     *
     * @param pageNum  当前页
     * @param pageSize 一页显示多少台哦
     * @param params   参数map
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 下订单
     *
     * @param tmallId              天猫店铺id
     * @param aidianmaoMemberToken 用户凭据
     * @return
     */
    Result placingOrder(String tmallId, String aidianmaoMemberToken);

    /**
     * 支付订单
     *
     * @param tmallOrderId         订单id
     * @param aidianmaoMemberToken 凭据
     * @param payPassword          支付密码
     * @return
     */
    Result payOrder(String tmallOrderId, String aidianmaoMemberToken, String payPassword);

    /**
     * 天猫店铺买家确认收货
     *
     * @param tmallOrderId         天猫订单id
     * @param aidianmaoMemberToken 当前用户凭据
     * @return
     */
    Result takeOrder(String tmallOrderId, String aidianmaoMemberToken);

    /**
     * 订单支付分两步 第一步先支付20%
     * @param tmallOrderId
     * @param aidianmaoMemberToken
     * @param payPassword
     * @return
     */
    Result payOrderOneStept(String tmallOrderId, String aidianmaoMemberToken, String payPassword);

    /**
     * 第二步交90%
     * @param tmallOrderId
     * @param aidianmaoMemberToken
     * @param payPassword
     * @return
     */
    Result payOrderTwoStept(String tmallOrderId, String aidianmaoMemberToken, String payPassword);

    /**
     * 分页查询我买到的网店
     * @param pageNum
     * @param pageSize
     * @param order
     * @return
     */
    Result getList(Integer pageNum, Integer pageSize,  TmallOrder order,String memberId);

    /**
     * 服务动态(最近的天猫店铺一条)
     * @return
     */
    Result getDynamic();

    /**
     * 根据卖家用户id查询订单(带上isSaleSure参数查询可操作订单，不带查询所有订单)
     * @param memberId
     * @return
     */
    Result getSaleOrder(String memberId,Integer pageNum, Integer pageSize,String isSaleSure);

    /**
     * 查询卖家所有的订单
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getAllSaleOrder(String memberId,Integer pageNum, Integer pageSize);
    /**
     * 查询买家可操作订单
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getBuyOrder(String memberId,Integer pageNum, Integer pageSize);

    /**
     * 根据id查询订单
     * @param id
     * @return
     */
    Result getOrderDetail(String id);

    /**
     * 查询可以操作订单
     * @param memberId
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getDealOrder(String memberId,Integer pageNum, Integer pageSize);

    /**
     * 取消订单
     * @param id
     * @return
     */
    Result cancelOrder(String id,String memberId);

    /**
     * 卖家确认
     * @param id
     * @param memberId
     * @return
     */
    Result sureOrder(String id,String memberId);

    /**
     * 卖家待交接确认出售
     * @param id
     * @param memberId
     * @return
     */
    Result sureToConnect(String id,String memberId);
}
