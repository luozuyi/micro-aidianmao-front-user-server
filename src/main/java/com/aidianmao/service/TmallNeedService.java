package com.aidianmao.service;

import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.TmallNeed;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface TmallNeedService {

    /**
     *  增加求购
     * @param tmallNeed  求购对象
     * @param aidianmaoMemberToken   用户
     * @return
     */

    Result addTmallNeed(TmallNeed tmallNeed, String aidianmaoMemberToken, ProvincesCitysCountrys provincesCitysCountrys);


    /**
     *  分页查询求购信息
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @param params
     * @return
     */
    Result pageTmallNeedList(String pageNum, String pageSize, Map<String, Object> params);

    /**
     * 分页根据用户id查询所求购信息
     * @param pageNum   当前页
     * @param pageSize 一页显示多少条
     * @param aidianmaoMemberToken
     * @return
     */
    Result getUserTmallNeed(Integer pageNum, Integer pageSize,String aidianmaoMemberToken);

    /**
     * 根据主键id获取天猫求购详情
     * @param id  主键id
     * @return
     */
    Result getTmallNeed(String id);

    /**
     * 修改求购信息
     * @param tmallNeed
     * @return
     */
    Result updateTmallNeed(TmallNeed tmallNeed);

    /**
     * 下架求购
     * @param aidianmaoMemberToken
     * @return
     */
    Result deleteTmallNeed(String id,String aidianmaoMemberToken);
}
