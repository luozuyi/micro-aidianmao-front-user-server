package com.aidianmao.service;

import com.aidianmao.entity.Channel;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface ChannelService {

    /**
     * 动态分页条件查询栏目
     * @param pageNum   当前页
     * @param pageSize  一页显示多少条
     * @param params    条件参数
     * @return
     */
    Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 添加栏目
     * @param channel 栏目对象
     * @return
     */
    Result addChannel(Channel channel);

    /**
     * 删除栏目
     * @param id
     * @return
     */
    Result delChannel(String id);

    /**
     * 获取栏目列表
     * @return
     */
    Result getList();

    /**
     * 获取父栏目列表
     * @return
     */
    Result getParentChannel();
}
