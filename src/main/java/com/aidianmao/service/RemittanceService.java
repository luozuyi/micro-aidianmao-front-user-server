package com.aidianmao.service;

import com.aidianmao.entity.Remittance;
import com.aidianmao.utils.Result;
import org.springframework.web.multipart.MultipartFile;

public interface RemittanceService {
    /**
     * 线下支付
     * @param remittance 支付对象
     * @return
     */
    Result add(Remittance remittance, String aidianmaoMemberToken);

    /**
     * 获取图片路径
     * @param file
     * @return
     */
    Result addSelfProductDetailPhoto(MultipartFile file);


    /**
     * 线下汇款筛选
     * @param remittance
     * @param aidianmaoMemberToken
     * @return
     */
    Result getList(Remittance remittance, String aidianmaoMemberToken,Integer pageNum, Integer pageSize);
}
