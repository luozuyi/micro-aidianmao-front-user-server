package com.aidianmao.service;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface AssessTmallService {

    /**
     * 添加估价
     *
     * @param assessTmall          估价对象
     * @return
     */
    Result addAssessTmall(AssessTmall assessTmall,String aidianmaoMemberToken);


    /**
     * 分页查询充值列表
     * @param pageNum 当前页
     * @param pageSize 一页显示多少条
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params);

    /**
     * 获取总估价店铺数量
     * @return
     */
    Result getCount();
}
