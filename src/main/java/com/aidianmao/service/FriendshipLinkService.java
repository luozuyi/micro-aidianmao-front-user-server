package com.aidianmao.service;

import com.aidianmao.utils.Result;

/**
 *
 * @author W1665
 * @date 2018/12/14
 */
public interface FriendshipLinkService {

    /**
     * 获取友情链接列表
     * @return
     */
    Result getFriendshipLinkList();
}
