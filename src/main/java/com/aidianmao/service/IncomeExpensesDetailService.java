package com.aidianmao.service;

import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.utils.Result;

public interface IncomeExpensesDetailService {

    /**
     * 多条件筛选查询收支明细
     * @param record
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    Result getAllBySelective(IncomeExpensesDetail record,String aidianmaoMemberToken,Integer pageNum,Integer pageSize);
}
