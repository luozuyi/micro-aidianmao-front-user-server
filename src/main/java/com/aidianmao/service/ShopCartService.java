package com.aidianmao.service;

import com.aidianmao.entity.ShopCart;
import com.aidianmao.utils.Result;

import java.util.Map;

public interface ShopCartService {

    /**
     * 通过会员id查询购物车
     * @param aidianmaoMemberToken
     * @return
     */
    Result getShopCart(String aidianmaoMemberToken,Integer pageNum,Integer pageSize);

    /**
     * 加入购物车
     * @param tmallId
     * @param aidianmaoMemberToken
     * @return
     */
    Result addShopCart(String tmallId,String aidianmaoMemberToken);

    /**
     * 订单付款后删除购物车里该条记录
     * @param tmallId
     * @param memberId
     * @return
     */
    Result deleteShopCart(String tmallId,String memberId);

    /**
     * 直接从购物车删除
     * @param id
     * @return
     */
    Result deletShopCart(String id);

    /**
     * 分页查询购店车
     * @param pageNum
     * @param pageSize
     * @param params
     * @return
     */
    Result pageList(Integer pageNum, Integer pageSize,  Map<String, Object> params);
}
