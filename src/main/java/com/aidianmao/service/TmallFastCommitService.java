package com.aidianmao.service;

import com.aidianmao.entity.TmallFastCommit;
import com.aidianmao.utils.Result;

public interface TmallFastCommitService {

    /**
     * 快速挂店，新增
     * @param record
     * @return
     */
    Result fastSale(TmallFastCommit record, String aidianmaoMemberToken);
}
