package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface CaptchaService {

    /**
     * 人机验证
     * @param resp
     * @return
     */
    Result checkAuth(String resp);
}
