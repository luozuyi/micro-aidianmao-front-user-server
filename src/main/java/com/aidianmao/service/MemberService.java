package com.aidianmao.service;

import com.aidianmao.utils.Result;

public interface MemberService {
    /**
     * 用户注册 第三步
     * @param telephone 手机号
     * @param password 密码
     * @param surePassword 确认密码
     * @param telePhoneCode 验正密码
     * @return
     */
    Result regist(String telephone,String password,String surePassword,String telePhoneCode,String userName);

    /**
     * 手机号校验是否已经注册
     * @param telephone
     * @return
     */
    Result telephoneCheck(String telephone);

    /**
     * 用户名校验是否已经注册
     * @param userName
     * @return
     */
    Result userNameCheck(String userName);


    /**
     * 根据Id查询用户信息
     * @param aidianmaoMemberToken
     * @return
     */
    Result moneyDetail(String aidianmaoMemberToken);

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @param aidianmaoMemberToken
     * @return
     */
    Result modifyPassword(String oldPassword,String newPassword,String newPasswordtwo,String aidianmaoMemberToken);

    /**
     * 基本信息修改
     * @param newIdCard   新身份证号
     * @param newrealName    新名字
     * @param aidianmaoMemberToken
     * @return
     */
    Result updateMember(String newIdCard,String newrealName,String aidianmaoMemberToken);


    /**
     * 修改支付密码
     * @param verification  验证码
     * @param newPayPassword    新支付密码
     * @param newPayPasswordTwo     再次输入新支付密码
     * @param aidianmaoMemberToken
     * @return
     */
    Result updatePayPassword(String verification,String newPayPassword,String newPayPasswordTwo,String aidianmaoMemberToken);


    /**
     * 找回密码
     * @param phone  手机号
     * @param verification  验证码
     * @param newPassword    新密码
     * @param newPasswordTwo     再次输入密码
     * @return
     */
    Result retrievePhone(String phone,String verification,String newPassword,String newPasswordTwo);
}
