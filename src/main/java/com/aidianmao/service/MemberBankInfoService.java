package com.aidianmao.service;

import com.aidianmao.entity.MemberBankInfo;
import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.utils.Result;

public interface MemberBankInfoService {
    /**
     * 添加会员银行卡
     * @param memberBankInfo 银行卡对象
     * @param provincesCitysCountrys 详细地址
     * @param aidianmaoMemberToken 凭据
     * @return
     */
    Result add(MemberBankInfo memberBankInfo, ProvincesCitysCountrys provincesCitysCountrys,String aidianmaoMemberToken);
}
