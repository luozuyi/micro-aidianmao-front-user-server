package com.aidianmao.repository;

import com.aidianmao.esEntity.EsShopName;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopNameRepository extends ElasticsearchRepository<EsShopName,String> {
}
