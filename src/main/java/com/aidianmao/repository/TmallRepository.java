package com.aidianmao.repository;

import com.aidianmao.esEntity.EsTmallShop;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TmallRepository extends ElasticsearchRepository<EsTmallShop, String> {
}
