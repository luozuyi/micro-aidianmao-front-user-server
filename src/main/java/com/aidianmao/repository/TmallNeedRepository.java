package com.aidianmao.repository;


import com.aidianmao.esEntity.EsTmallNeed;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TmallNeedRepository extends ElasticsearchRepository<EsTmallNeed,String> {
}
