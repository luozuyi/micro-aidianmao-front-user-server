package com.aidianmao.esEntity;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName="shopname",type="shopname",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsShopName {

    private String id;

    private String shopName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
