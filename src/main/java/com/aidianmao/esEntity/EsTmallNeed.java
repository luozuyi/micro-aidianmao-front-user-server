package com.aidianmao.esEntity;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Document(indexName="tmallneed",type="tmallneed",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsTmallNeed implements Serializable {
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 求购主题
     */
    private String needSubject;
    /**
     * 求购者电话
     */
    private String phone;
    /**
     * 求购产品类目0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健 8:珠宝/首饰 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像 13:玩乐/收藏 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     *
     */
    private String needType;
    /**
     * 天猫商城类型 0:旗舰店  1: 专营店 2: 专卖店
     */
    private String tmallType;
    /**
     * 商标类型 0:R标  1:TM标
     */
    private String brandType;
    /**
     * 求购数量
     */
    private String count;
    /**
     * 求购价格
     */
    private String price;
    /**
     * 店铺地址
     */
    private String addressId;
    /**
     * 买家描述
     */
    private String remark;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     */
    private String taxPayerType;
    /**
     * 所需证件 身份证 手持身份证 营业执照 手持营业执照
     */
    private String needCredentials;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 审核状态 0:审核中 1:审核通过 2:审核不通过 3:下架
     */
    private String status;
    /**
     * 审核人关联的id
     */
    private String adminId;

    /**
     * 审核时间
     */
    private String  approveTime;
    /**
     * 创店时间(年份)
     */
    private String createShopTime;
    /**
     * 审核备注
     */
    private String note;
    /**
     * 价格区间，截止价格
     */
    private String endPrice;

    public String getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(String endPrice) {
        this.endPrice = endPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getNeedSubject() {
        return needSubject;
    }

    public void setNeedSubject(String needSubject) {
        this.needSubject = needSubject;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNeedType() {
        return needType;
    }

    public void setNeedType(String needType) {
        this.needType = needType;
    }

    public String getTmallType() {
        return tmallType;
    }

    public void setTmallType(String tmallType) {
        this.tmallType = tmallType;
    }

    public String getBrandType() {
        return brandType;
    }

    public void setBrandType(String brandType) {
        this.brandType = brandType;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType;
    }

    public String getNeedCredentials() {
        return needCredentials;
    }

    public void setNeedCredentials(String needCredentials) {
        this.needCredentials = needCredentials;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getCreateShopTime() {
        return createShopTime;
    }

    public void setCreateShopTime(String createShopTime) {
        this.createShopTime = createShopTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
