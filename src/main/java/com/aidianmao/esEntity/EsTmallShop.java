package com.aidianmao.esEntity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * 天猫店铺
 */
@Document(indexName="tmall",type="tmall",indexStoreType="fs",shards=5,replicas=1,refreshInterval="-1")
public class EsTmallShop implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    @Id
    private String id;

    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 行业类型 0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类
     * 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健
     * 8:珠宝/首饰 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像
     * 13:玩乐/收藏 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     */
    private String industryType;
    /**
     * 商城类型  0:旗舰店  1: 专营店 2: 专卖店
     */
    private String shopType;
    /**
     * 商标类型 0:R标  1: TM标
     */
    private String trademarkType;
    /**
     * 宝贝与描述相符评分
     */
    private String productDescriptionPoint;
    /**
     * 卖家服务态度评分
     */
    private String serviceAttitudePoint;
    /**
     * 卖家发货速度评分
     */
    private String deliverySpeedPoint;
    /**
     * 消费者保障金
     */
    private String shopDeposit;
    /**
     * 店铺技术年费
     */
    private String shopTechServiceFee;
    /**
     * 公司注册资金
     */
    private String companyRegisterMoney;

    /**
     * 店铺价格
     */
    private String shopPrice;
    /**
     * 是否完成营业额指标  0:完成  1: 没有完成
     */
    private String isCompletedTurnoverIndex;

    /**
     * 是否扣分 0:是 1：否
     */
    private String isDeduction;
    /**
     * 一般违规扣分
     */
    private String generalViolationPoint;
    /**
     * 严重违规扣分
     */
    private String seriousViolationPoint;
    /**
     * 店铺介绍
     */
    private String shopProfile;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     */
    private String taxPayerType;
    /**
     * 城市Id
     */
    private String provincesId;
    /**
     * 审核时间
     */
    private String approveTime;
    /**
     *宝贝与描述相符评分图标(1.低、2.持平、3.高)
     */
    private String productDescriptionPointIco;
    /**
     *卖家服务态度评分图标(1.低、2.持平、3.高)
     */
    private String serviceAttitudePointIco;
    /**
     *卖家发货速度评分图标(1.低、2.持平、3.高)
     */
    private String deliverySpeedPointIco;
    /**
     * 状态
     */
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductDescriptionPointIco() {
        return productDescriptionPointIco;
    }

    public void setProductDescriptionPointIco(String productDescriptionPointIco) {
        this.productDescriptionPointIco = productDescriptionPointIco;
    }

    public String getServiceAttitudePointIco() {
        return serviceAttitudePointIco;
    }

    public void setServiceAttitudePointIco(String serviceAttitudePointIco) {
        this.serviceAttitudePointIco = serviceAttitudePointIco;
    }

    public String getDeliverySpeedPointIco() {
        return deliverySpeedPointIco;
    }

    public void setDeliverySpeedPointIco(String deliverySpeedPointIco) {
        this.deliverySpeedPointIco = deliverySpeedPointIco;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType;
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId;
    }

    public String getGeneralViolationPoint() {
        return generalViolationPoint;
    }

    public void setGeneralViolationPoint(String generalViolationPoint) {
        this.generalViolationPoint = generalViolationPoint;
    }

    public String getSeriousViolationPoint() {
        return seriousViolationPoint;
    }

    public void setSeriousViolationPoint(String seriousViolationPoint) {
        this.seriousViolationPoint = seriousViolationPoint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getTrademarkType() {
        return trademarkType;
    }

    public void setTrademarkType(String trademarkType) {
        this.trademarkType = trademarkType;
    }

    public String getProductDescriptionPoint() {
        return productDescriptionPoint;
    }

    public void setProductDescriptionPoint(String productDescriptionPoint) {
        this.productDescriptionPoint = productDescriptionPoint;
    }

    public String getServiceAttitudePoint() {
        return serviceAttitudePoint;
    }

    public void setServiceAttitudePoint(String serviceAttitudePoint) {
        this.serviceAttitudePoint = serviceAttitudePoint;
    }

    public String getDeliverySpeedPoint() {
        return deliverySpeedPoint;
    }

    public void setDeliverySpeedPoint(String deliverySpeedPoint) {
        this.deliverySpeedPoint = deliverySpeedPoint;
    }

    public String getShopDeposit() {
        return shopDeposit;
    }

    public void setShopDeposit(String shopDeposit) {
        this.shopDeposit = shopDeposit;
    }

    public String getShopTechServiceFee() {
        return shopTechServiceFee;
    }

    public void setShopTechServiceFee(String shopTechServiceFee) {
        this.shopTechServiceFee = shopTechServiceFee;
    }

    public String getCompanyRegisterMoney() {
        return companyRegisterMoney;
    }

    public void setCompanyRegisterMoney(String companyRegisterMoney) {
        this.companyRegisterMoney = companyRegisterMoney;
    }

    public String getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(String shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getIsCompletedTurnoverIndex() {
        return isCompletedTurnoverIndex;
    }

    public void setIsCompletedTurnoverIndex(String isCompletedTurnoverIndex) {
        this.isCompletedTurnoverIndex = isCompletedTurnoverIndex;
    }

    public String getIsDeduction() {
        return isDeduction;
    }

    public void setIsDeduction(String isDeduction) {
        this.isDeduction = isDeduction;
    }

    public String getShopProfile() {
        return shopProfile;
    }

    public void setShopProfile(String shopProfile) {
        this.shopProfile = shopProfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EsTmallShop that = (EsTmallShop) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(shopName, that.shopName) &&
                Objects.equals(industryType, that.industryType) &&
                Objects.equals(shopType, that.shopType) &&
                Objects.equals(trademarkType, that.trademarkType) &&
                Objects.equals(productDescriptionPoint, that.productDescriptionPoint) &&
                Objects.equals(serviceAttitudePoint, that.serviceAttitudePoint) &&
                Objects.equals(deliverySpeedPoint, that.deliverySpeedPoint) &&
                Objects.equals(shopDeposit, that.shopDeposit) &&
                Objects.equals(shopTechServiceFee, that.shopTechServiceFee) &&
                Objects.equals(companyRegisterMoney, that.companyRegisterMoney) &&
                Objects.equals(shopPrice, that.shopPrice) &&
                Objects.equals(isCompletedTurnoverIndex, that.isCompletedTurnoverIndex) &&
                Objects.equals(isDeduction, that.isDeduction) &&
                Objects.equals(generalViolationPoint, that.generalViolationPoint) &&
                Objects.equals(seriousViolationPoint, that.seriousViolationPoint) &&
                Objects.equals(shopProfile, that.shopProfile);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, shopName, industryType, shopType, trademarkType, productDescriptionPoint, serviceAttitudePoint, deliverySpeedPoint, shopDeposit, shopTechServiceFee, companyRegisterMoney, shopPrice, isCompletedTurnoverIndex, isDeduction, generalViolationPoint, seriousViolationPoint, shopProfile);
    }
}
