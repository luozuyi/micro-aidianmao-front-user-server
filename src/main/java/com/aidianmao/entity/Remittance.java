package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Remittance implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     *充值会员id
     */
    private String memberId;
    /**
     * 收款账户
     */
    private String makeCollectionsAccout;
    /**
     * 汇款金额
     */
    private BigDecimal money;
    /**
     * 汇款凭证图片
     */
    private String paymentDocumentImage;
    /**
     * 汇款银行id
     */
    private String remitBankId;
    /**
     * 汇款日期
     */
    private Date remitTime;
    /**
     *状态 0:审核中 1:成功 2:失败
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 审核人id
     */
    private String adminId;
    /**
     * 汇款人姓名
     */
    private String name;
    /**
     * 转账详细支行名称
     */
    private String remitBankName;
    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public String getRemitBankName() {
        return remitBankName;
    }

    public void setRemitBankName(String remitBankName) {
        this.remitBankName = remitBankName;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getMakeCollectionsAccout() {
        return makeCollectionsAccout;
    }

    public void setMakeCollectionsAccout(String makeCollectionsAccout) {
        this.makeCollectionsAccout = makeCollectionsAccout == null ? null : makeCollectionsAccout.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getPaymentDocumentImage() {
        return paymentDocumentImage;
    }

    public void setPaymentDocumentImage(String paymentDocumentImage) {
        this.paymentDocumentImage = paymentDocumentImage == null ? null : paymentDocumentImage.trim();
    }

    public String getRemitBankId() {
        return remitBankId;
    }

    public void setRemitBankId(String remitBankId) {
        this.remitBankId = remitBankId == null ? null : remitBankId.trim();
    }

    public Date getRemitTime() {
        return remitTime;
    }

    public void setRemitTime(Date remitTime) {
        this.remitTime = remitTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}