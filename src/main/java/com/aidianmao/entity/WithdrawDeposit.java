package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class WithdrawDeposit implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除状态
     */
    private String delFlag;
    /**
     * 个人银行信息
     */
    private String memberBankInfoId;
    /**
     * 提现金额
     */
    private BigDecimal money;
    /**
     * 到账金额
     */
    private BigDecimal actualAppropriation;
    /**
     * 提现手续费
     */
    private BigDecimal serviceFee;
    /**
     * 状态 0:审核中 1:打款中 2:成功 3:失败
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberBankInfoId() {
        return memberBankInfoId;
    }

    public void setMemberBankInfoId(String memberBankInfoId) {
        this.memberBankInfoId = memberBankInfoId == null ? null : memberBankInfoId.trim();
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getActualAppropriation() {
        return actualAppropriation;
    }

    public void setActualAppropriation(BigDecimal actualAppropriation) {
        this.actualAppropriation = actualAppropriation;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}