package com.aidianmao.entity;

import java.io.Serializable;

/**
 * 城市
 * 
 * @author Catch22
 * @date 2018年5月31日
 */
public class Citys implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	/**
	 * 城市名字
	 */
	private String name;
	/**
	 * 城市编号
	 */
	private Character code;
	/**
	 * 省份ID
	 */
	private String provincesId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Character getCode() {
		return code;
	}

	public void setCode(Character code) {
		this.code = code;
	}

	public String getProvincesId() {
		return provincesId;
	}

	public void setProvincesId(String provincesId) {
		this.provincesId = provincesId;
	}
}
