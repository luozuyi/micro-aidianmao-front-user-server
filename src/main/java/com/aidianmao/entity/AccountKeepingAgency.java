package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class AccountKeepingAgency implements Serializable {
    /**
     * 主键id
     */
    private String id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 删除标志位
     */
    private String delFlag;

    /**
     * 城市id
     */
    private String citysName;

    /**
     *  提交电话
     */
    private String phone;
    /**
     * ip
     */
    private String ipAddress;
    /**
     * 是否交接  0:是 1:否
     */
    private String isTakeOver;

    public String getIsTakeOver() {
        return isTakeOver;
    }

    public void setIsTakeOver(String isTakeOver) {
        this.isTakeOver = isTakeOver;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getCitysName() {
        return citysName;
    }

    public void setCitysName(String citysName) {
        this.citysName = citysName;
    }
}