package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class CustomerQqRecord implements Serializable{
    /**
     * 主键id
     */
    private String id;
    /**
     * 时间
     */
    private Date createTime;
    /**
     * 删除标志位  0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 客户ip
     */
    private String ip;
    /**
     * 用户id
     */
    private String memberId;
    /**
     * 管理员名字
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}