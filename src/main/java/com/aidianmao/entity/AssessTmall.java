package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AssessTmall implements Serializable {
    /**
     * 主键id
     * */
    private String id;
    /**
     *创建时间
     * */
    private Date createTime;
    /**
     *删除标志位 0:正常 1：删除 2：待删除
     * */
    private String delFlag;
    /**
     *会员id
     * */
    private String memberId;
    /**
     *店铺链接
     * */
    private String shopUrl;
    /**
     * 年营业额
     * */
    private BigDecimal yearTurnover;
    /**
     *月营业额
     * */
    private BigDecimal monthTurnover;
    /**
     * 客单价
     * */
    private BigDecimal perTicketSales;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     * */
    private String taxPayerType;
    /**
     * 电话
     * */
    private String phone;
    /**
     * qq号码
     * */
    private String qq;
    /**
     * 微信号码
     * */
    private String wechat;
    /**
     * 估价结果
     * */
    private BigDecimal assessResult;
    /**
     * 销售员id
     */
    private String adminId;

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl;
    }

    public BigDecimal getYearTurnover() {
        return yearTurnover;
    }

    public void setYearTurnover(BigDecimal yearTurnover) {
        this.yearTurnover = yearTurnover;
    }

    public BigDecimal getMonthTurnover() {
        return monthTurnover;
    }

    public void setMonthTurnover(BigDecimal monthTurnover) {
        this.monthTurnover = monthTurnover;
    }

    public BigDecimal getPerTicketSales() {
        return perTicketSales;
    }

    public void setPerTicketSales(BigDecimal perTicketSales) {
        this.perTicketSales = perTicketSales;
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public BigDecimal getAssessResult() {
        return assessResult;
    }

    public void setAssessResult(BigDecimal assessResult) {
        this.assessResult = assessResult;
    }
}
