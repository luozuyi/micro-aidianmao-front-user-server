package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 天猫订单冻结明细实体类
 */
public class TmallOrderFreeze implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 天猫订单id
     */
    private String tmallOrderId;
    /**
     * 冻结金额
     */
    private BigDecimal freezeMoney;
    /**
     * 冻结状态 0: 冻结中 1: 已解冻
     */
    private String status;
    /**
     * 用户id
     */
    private String memberId;
    /**
     * 场景 0:定金 1:店铺余款 2:全额店款
     */
    private String moneyScene;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getTmallOrderId() {
        return tmallOrderId;
    }

    public void setTmallOrderId(String tmallOrderId) {
        this.tmallOrderId = tmallOrderId == null ? null : tmallOrderId.trim();
    }

    public BigDecimal getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(BigDecimal freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMoneyScene() {
        return moneyScene;
    }

    public void setMoneyScene(String moneyScene) {
        this.moneyScene = moneyScene;
    }
}