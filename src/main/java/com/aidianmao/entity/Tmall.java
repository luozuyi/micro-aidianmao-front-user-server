package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * 天猫店铺
 */
public class Tmall implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 会员ID
     */
    private String memberId;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 店铺介绍
     */
    private String shopProfile;
    /**
     * 店铺网址
     */
    private String shopUrl;
    /**
     * 店主名称
     */
    private String shopOwner;
    /**
     * 店铺价格
     */
    private BigDecimal shopPrice;
    /**
     * 消费者保障金
     */
    private BigDecimal shopDeposit;
    /**
     * 是否退还消费者保障金 0: 需要退还 1:不需要退还
     */
    private String isReturnDeposit;
    /**
     * 店铺技术年费
     */
    private BigDecimal shopTechServiceFee;
    /**
     * 是否退还技术年费 0: 需要退还 1:不需要退还
     */
    private String isReturnTechServiceFee;
    /**
     * 商城类型  0:旗舰店  1: 专营店 2: 专卖店
     */
    private String shopType;
    /**
     * 商标类型 0:R标  1: TM标
     */
    private String trademarkType;
    /**
     * 可以经营的二级类目
     */
    private String saleSecondCategory;
    /**
     * 是否带货 0:带货  1: 不带货
     */
    private String isCarryGoods;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     */
    private String taxPayerType;
    /**
     * 是否有贷款 0:有贷款  1: 没有贷款
     */
    private String isLoan;
    /**
     * 去年年营业额
     */
    private BigDecimal lastYearTurnover;
    /**
     * 今年完成的营业额
     */
    private BigDecimal completedTurnover;
    /**
     * 是否完成营业额指标  0:完成  1: 没有完成
     */
    private String isCompletedTurnoverIndex;
    /**
     * 公司注册资金
     */
    private BigDecimal companyRegisterMoney;
    /**
     * 贷款额度
     */
    private BigDecimal loanLimit;
    /**
     * 行业类型 0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类
     * 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健
     * 8:珠宝/首饰 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像
     * 13:玩乐/收藏 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     */
    private String industryType;
    /**
     * 当前主营行业 0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类
     * 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健 8:珠宝/首饰
     * 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像 13:玩乐/收藏
     * 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     */
    private String currentMainCamp;
    /**
     * 客单价
     */
    private BigDecimal perTicketSales;
    /**
     * 天猫入驻时间
     */
    private Date shopEnterTime;
    /**
     * 商标所属人 0:个人  1: 公司
     */
    private String trademarkOwner;
    /**
     * 宝贝与描述相符评分
     */
    private String productDescriptionPoint;
    /**
     * 卖家服务态度评分
     */
    private String serviceAttitudePoint;
    /**
     * 卖家发货速度评分
     */
    private String deliverySpeedPoint;
    /**
     * 一般违规扣分
     */
    private String generalViolationPoint;
    /**
     * 严重违规扣分
     */
    private String seriousViolationPoint;
    /**
     * 售假违规扣分
     */
    private String counterfeitViolationPoint;
    /**
     * 访问量(人气数)
     */
    private Integer visitPageViewsCount;
    /**
     * 是否可以过户 0:可以过户  1: 不可以过户
     */
    private String isTransfer;
    /**
     * 是否有债务纠纷 0:没有债务纠纷  1: 有债务纠纷
     */
    private String isDisputeOverObligation;
    /**
     * 店铺资质 正品保障 消费者保障 七天退换
     */
    private String shopQualification;
    /**
     * 可以提供的证件 身份证 手持身份证 营业执照 手持营业执照
     */
    private String credentials;
    /**
     * 店铺图片
     */
    private String shopImage;
    /**
     * TM标是否入品牌库  0:是  1: 否
     */
    private String isTmTrademarkAddBrand;
    /**
     * 店铺被收藏数
     */
    private Integer shopCollectCount;
    /**
     * 联系人姓名
     */
    private String contactName;
    /**
     * 联系人电话
     */
    private String contactPhone;
    /**
     * 联系人QQ
     */
    private String contactQq;
    /**
     * 联系人微信号
     */
    private String contactWechat;
    /**
     * 公司注册地区
     */
    private String companyAddressId;
    /**
     * 0:审核中 1:审核通过 2:审核不通过 3:下架 4:已付定金 5:已付全款 6:交接中 7:已售出
     */
    private String status;
    /**
     * 审核备注
     */
    private String auditingComment;
    /**
     * 审核时间
     */
    private String approveTime;
    /**
     * 商城编号
     */
    private String tmallSerialNumber;
    /**
     * 城市名称
     */
    private String citysName;
    /**
     *宝贝与描述相符评分图标(1.低、2.持平、3.高)
     */
    private String productDescriptionPointIco;
    /**
     *卖家服务态度评分图标(1.低、2.持平、3.高)
     */
    private String serviceAttitudePointIco;
    /**
     *卖家发货速度评分图标(1.低、2.持平、3.高)
     */
    private String deliverySpeedPointIco;
    /**
     * 费率规则关联id
     */
    private String tmallCostRuleId;
    /**
     * 商标是否转让(0:是 1:否)
      */
    private String isTrademarkTransfer;
    /**
     * 前端展示时间，截取入住时间的年月日
     */
    private String showTime;
    /**
     * 紧急联系人电话
     */
    private String urgentPhone;
    /**
     * 交易状态
     */
    private String tradeStatusInfo;
    /**
     * 展示信息
     */
    private String playInfo;
    /**
     * 省id
      */
    private String citysId;
    /**
     * 市id
     */
    private String provincesId;
    /**
     *店铺图片(营业额截图)
     */
    private String shopTurnoverImage;
    /**
     *店铺图片(违规扣分截图)
     */
    private String shopViolationImage;

    public String getShopTurnoverImage() {
        return shopTurnoverImage;
    }

    public void setShopTurnoverImage(String shopTurnoverImage) {
        this.shopTurnoverImage = shopTurnoverImage;
    }

    public String getShopViolationImage() {
        return shopViolationImage;
    }

    public void setShopViolationImage(String shopViolationImage) {
        this.shopViolationImage = shopViolationImage;
    }

    public String getCitysId() {
        return citysId;
    }

    public void setCitysId(String citysId) {
        this.citysId = citysId;
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId;
    }

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public String getTradeStatusInfo() {
        return tradeStatusInfo;
    }

    public void setTradeStatusInfo(String tradeStatusInfo) {
        this.tradeStatusInfo = tradeStatusInfo;
    }

    public String getUrgentPhone() {
        return urgentPhone;
    }

    public void setUrgentPhone(String urgentPhone) {
        this.urgentPhone = urgentPhone;
    }

    public String getShowTime() {
        return showTime;
    }

    public void setShowTime(String showTime) {
        this.showTime = showTime;
    }

    public String getIsTrademarkTransfer() {
        return isTrademarkTransfer;
    }

    public void setIsTrademarkTransfer(String isTrademarkTransfer) {
        this.isTrademarkTransfer = isTrademarkTransfer;
    }

    public String getTmallCostRuleId() {
        return tmallCostRuleId;
    }

    public void setTmallCostRuleId(String tmallCostRuleId) {
        this.tmallCostRuleId = tmallCostRuleId;
    }

    public String getProductDescriptionPointIco() {
        return productDescriptionPointIco;
    }

    public void setProductDescriptionPointIco(String productDescriptionPointIco) {
        this.productDescriptionPointIco = productDescriptionPointIco;
    }

    public String getServiceAttitudePointIco() {
        return serviceAttitudePointIco;
    }

    public void setServiceAttitudePointIco(String serviceAttitudePointIco) {
        this.serviceAttitudePointIco = serviceAttitudePointIco;
    }

    public String getDeliverySpeedPointIco() {
        return deliverySpeedPointIco;
    }

    public void setDeliverySpeedPointIco(String deliverySpeedPointIco) {
        this.deliverySpeedPointIco = deliverySpeedPointIco;
    }

    public String getCitysName() {
        return citysName;
    }

    public void setCitysName(String citysName) {
        this.citysName = citysName;
    }

    public String getTmallSerialNumber() {
        return tmallSerialNumber;
    }

    public void setTmallSerialNumber(String tmallSerialNumber) {
        this.tmallSerialNumber = tmallSerialNumber;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName == null ? null : shopName.trim();
    }

    public String getShopProfile() {
        return shopProfile;
    }

    public void setShopProfile(String shopProfile) {
        this.shopProfile = shopProfile == null ? null : shopProfile.trim();
    }

    public String getShopUrl() {
        return shopUrl;
    }

    public void setShopUrl(String shopUrl) {
        this.shopUrl = shopUrl == null ? null : shopUrl.trim();
    }

    public String getShopOwner() {
        return shopOwner;
    }

    public void setShopOwner(String shopOwner) {
        this.shopOwner = shopOwner == null ? null : shopOwner.trim();
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public BigDecimal getShopDeposit() {
        return shopDeposit;
    }

    public void setShopDeposit(BigDecimal shopDeposit) {
        this.shopDeposit = shopDeposit;
    }

    public String getIsReturnDeposit() {
        return isReturnDeposit;
    }

    public void setIsReturnDeposit(String isReturnDeposit) {
        this.isReturnDeposit = isReturnDeposit == null ? null : isReturnDeposit.trim();
    }

    public BigDecimal getShopTechServiceFee() {
        return shopTechServiceFee;
    }

    public void setShopTechServiceFee(BigDecimal shopTechServiceFee) {
        this.shopTechServiceFee = shopTechServiceFee;
    }

    public String getIsReturnTechServiceFee() {
        return isReturnTechServiceFee;
    }

    public void setIsReturnTechServiceFee(String isReturnTechServiceFee) {
        this.isReturnTechServiceFee = isReturnTechServiceFee == null ? null : isReturnTechServiceFee.trim();
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType == null ? null : shopType.trim();
    }

    public String getTrademarkType() {
        return trademarkType;
    }

    public void setTrademarkType(String trademarkType) {
        this.trademarkType = trademarkType == null ? null : trademarkType.trim();
    }

    public String getSaleSecondCategory() {
        return saleSecondCategory;
    }

    public void setSaleSecondCategory(String saleSecondCategory) {
        this.saleSecondCategory = saleSecondCategory == null ? null : saleSecondCategory.trim();
    }

    public String getIsCarryGoods() {
        return isCarryGoods;
    }

    public void setIsCarryGoods(String isCarryGoods) {
        this.isCarryGoods = isCarryGoods == null ? null : isCarryGoods.trim();
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType == null ? null : taxPayerType.trim();
    }

    public String getIsLoan() {
        return isLoan;
    }

    public void setIsLoan(String isLoan) {
        this.isLoan = isLoan == null ? null : isLoan.trim();
    }

    public BigDecimal getLastYearTurnover() {
        return lastYearTurnover;
    }

    public void setLastYearTurnover(BigDecimal lastYearTurnover) {
        this.lastYearTurnover = lastYearTurnover;
    }

    public BigDecimal getCompletedTurnover() {
        return completedTurnover;
    }

    public void setCompletedTurnover(BigDecimal completedTurnover) {
        this.completedTurnover = completedTurnover;
    }

    public String getIsCompletedTurnoverIndex() {
        return isCompletedTurnoverIndex;
    }

    public void setIsCompletedTurnoverIndex(String isCompletedTurnoverIndex) {
        this.isCompletedTurnoverIndex = isCompletedTurnoverIndex == null ? null : isCompletedTurnoverIndex.trim();
    }

    public BigDecimal getCompanyRegisterMoney() {
        return companyRegisterMoney;
    }

    public void setCompanyRegisterMoney(BigDecimal companyRegisterMoney) {
        this.companyRegisterMoney = companyRegisterMoney;
    }

    public BigDecimal getLoanLimit() {
        return loanLimit;
    }

    public void setLoanLimit(BigDecimal loanLimit) {
        this.loanLimit = loanLimit;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType == null ? null : industryType.trim();
    }

    public String getCurrentMainCamp() {
        return currentMainCamp;
    }

    public void setCurrentMainCamp(String currentMainCamp) {
        this.currentMainCamp = currentMainCamp == null ? null : currentMainCamp.trim();
    }

    public BigDecimal getPerTicketSales() {
        return perTicketSales;
    }

    public void setPerTicketSales(BigDecimal perTicketSales) {
        this.perTicketSales = perTicketSales;
    }

    public Date getShopEnterTime() {
        return shopEnterTime;
    }

    public void setShopEnterTime(Date shopEnterTime) {
        this.shopEnterTime = shopEnterTime;
    }

    public String getTrademarkOwner() {
        return trademarkOwner;
    }

    public void setTrademarkOwner(String trademarkOwner) {
        this.trademarkOwner = trademarkOwner == null ? null : trademarkOwner.trim();
    }

    public String getProductDescriptionPoint() {
        return productDescriptionPoint;
    }

    public void setProductDescriptionPoint(String productDescriptionPoint) {
        this.productDescriptionPoint = productDescriptionPoint == null ? null : productDescriptionPoint.trim();
    }

    public String getServiceAttitudePoint() {
        return serviceAttitudePoint;
    }

    public void setServiceAttitudePoint(String serviceAttitudePoint) {
        this.serviceAttitudePoint = serviceAttitudePoint == null ? null : serviceAttitudePoint.trim();
    }

    public String getDeliverySpeedPoint() {
        return deliverySpeedPoint;
    }

    public void setDeliverySpeedPoint(String deliverySpeedPoint) {
        this.deliverySpeedPoint = deliverySpeedPoint == null ? null : deliverySpeedPoint.trim();
    }

    public String getGeneralViolationPoint() {
        return generalViolationPoint;
    }

    public void setGeneralViolationPoint(String generalViolationPoint) {
        this.generalViolationPoint = generalViolationPoint == null ? null : generalViolationPoint.trim();
    }

    public String getSeriousViolationPoint() {
        return seriousViolationPoint;
    }

    public void setSeriousViolationPoint(String seriousViolationPoint) {
        this.seriousViolationPoint = seriousViolationPoint == null ? null : seriousViolationPoint.trim();
    }

    public String getCounterfeitViolationPoint() {
        return counterfeitViolationPoint;
    }

    public void setCounterfeitViolationPoint(String counterfeitViolationPoint) {
        this.counterfeitViolationPoint = counterfeitViolationPoint == null ? null : counterfeitViolationPoint.trim();
    }

    public Integer getVisitPageViewsCount() {
        return visitPageViewsCount;
    }

    public void setVisitPageViewsCount(Integer visitPageViewsCount) {
        this.visitPageViewsCount = visitPageViewsCount;
    }

    public String getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(String isTransfer) {
        this.isTransfer = isTransfer == null ? null : isTransfer.trim();
    }

    public String getIsDisputeOverObligation() {
        return isDisputeOverObligation;
    }

    public void setIsDisputeOverObligation(String isDisputeOverObligation) {
        this.isDisputeOverObligation = isDisputeOverObligation == null ? null : isDisputeOverObligation.trim();
    }

    public String getShopQualification() {
        return shopQualification;
    }

    public void setShopQualification(String shopQualification) {
        this.shopQualification = shopQualification == null ? null : shopQualification.trim();
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials == null ? null : credentials.trim();
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage == null ? null : shopImage.trim();
    }

    public String getIsTmTrademarkAddBrand() {
        return isTmTrademarkAddBrand;
    }

    public void setIsTmTrademarkAddBrand(String isTmTrademarkAddBrand) {
        this.isTmTrademarkAddBrand = isTmTrademarkAddBrand == null ? null : isTmTrademarkAddBrand.trim();
    }

    public Integer getShopCollectCount() {
        return shopCollectCount;
    }

    public void setShopCollectCount(Integer shopCollectCount) {
        this.shopCollectCount = shopCollectCount;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName == null ? null : contactName.trim();
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone == null ? null : contactPhone.trim();
    }

    public String getContactQq() {
        return contactQq;
    }

    public void setContactQq(String contactQq) {
        this.contactQq = contactQq == null ? null : contactQq.trim();
    }

    public String getContactWechat() {
        return contactWechat;
    }

    public void setContactWechat(String contactWechat) {
        this.contactWechat = contactWechat == null ? null : contactWechat.trim();
    }

    public String getCompanyAddressId() {
        return companyAddressId;
    }

    public void setCompanyAddressId(String companyAddressId) {
        this.companyAddressId = companyAddressId == null ? null : companyAddressId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getAuditingComment() {
        return auditingComment;
    }

    public void setAuditingComment(String auditingComment) {
        this.auditingComment = auditingComment == null ? null : auditingComment.trim();
    }


}