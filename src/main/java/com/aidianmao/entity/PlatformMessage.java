package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 站内信
 */
public class PlatformMessage implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位
     */
    private String delFlag;
    /**
     * 内容
     */
    private String content;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 主题
     */
    private String title;
    /**
     * 状态 0:未读,1已读
     */
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public PlatformMessage() {
    }

    public PlatformMessage(String id, Date createTime, String delFlag, String content, String memberId, String title, String status) {
        this.id = id;
        this.createTime = createTime;
        this.delFlag = delFlag;
        this.content = content;
        this.memberId = memberId;
        this.title = title;
        this.status = status;
    }
}