package com.aidianmao.entity;

import java.io.Serializable;

/**
 * 省份
 * 
 * @author Catch22
 * @date 2018年5月31日
 */
public class Provinces implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	/**
	 * 名字
	 */
	private String name;

	private Integer weight;
	/**
	 * 代码
	 */
	private Character code;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Character getCode() {
		return code;
	}

	public void setCode(Character code) {
		this.code = code;
	}

}
