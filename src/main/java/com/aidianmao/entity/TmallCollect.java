package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 天猫收藏实体类
 */
public class TmallCollect implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位
     */
    private String delFlag;
    /**
     * 会员Id
     */
    private String memberId;
    /**
     * 天猫id
     */
    private String tmallId;

    public TmallCollect() {
    }

    public TmallCollect(String memberId, String tmallId) {
        this.memberId = memberId;
        this.tmallId = tmallId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getTmallId() {
        return tmallId;
    }

    public void setTmallId(String tmallId) {
        this.tmallId = tmallId == null ? null : tmallId.trim();
    }
}