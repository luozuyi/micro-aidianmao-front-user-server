package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TmallOrderLess implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 订单号
     */
    private String code;
    /**
     * 店铺名
     */
    private String shopName;
    /**
     * 店铺类型
     */
    private String shopType;
    /**
     * 店铺价格
     */
    private BigDecimal price;
    /**
     * 商城类目
     */
    private String industryType;
    /**
     * 消费者保障金
     */
    private BigDecimal shopDeposit;
    /**
     * 技术年费
     */
    private BigDecimal shopTechServiceFee;
    /**
     * 商城编号
     */
    private String tmallSerialNumber;
    /**
     * 服务费
     */
    private BigDecimal serviceFee;
    /**
     * 订单状态 0:待付款 1:已付定金 2:已付全款(补齐余款) 3:交易成功 4:已取消 5:已终止 6:异常订单(无资金调整) 7:异常订单(有资金调整) 8:交接中
     */
    private String status;
    /**
     * 是否退还消保金
     */
    private String isReturnDeposit;
    /**
     * 是否退还技术年费
     */
    private String isReturnTechServiceFee;
    /**
     * 买家id
     */
    private String buyMemberId;
    /**
     * 卖家id
     */
    private String saleMemberId;
    /**
     * 店铺价格
     */
    private String shopPrice;
    /**
     * 卖家确认出售
     */
    private String isSaleSure;
    /**
     * 显示信息
     */
    private String playInfo;
    /**
     * 店铺id
     */
    private String tmallId;

    public String getTmallId() {
        return tmallId;
    }

    public void setTmallId(String tmallId) {
        this.tmallId = tmallId;
    }

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public String getIsSaleSure() {
        return isSaleSure;
    }

    public void setIsSaleSure(String isSaleSure) {
        this.isSaleSure = isSaleSure;
    }

    public String getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(String shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getBuyMemberId() {
        return buyMemberId;
    }

    public void setBuyMemberId(String buyMemberId) {
        this.buyMemberId = buyMemberId;
    }

    public String getSaleMemberId() {
        return saleMemberId;
    }

    public void setSaleMemberId(String saleMemberId) {
        this.saleMemberId = saleMemberId;
    }

    public String getIsReturnDeposit() {
        return isReturnDeposit;
    }

    public void setIsReturnDeposit(String isReturnDeposit) {
        this.isReturnDeposit = isReturnDeposit;
    }

    public String getIsReturnTechServiceFee() {
        return isReturnTechServiceFee;
    }

    public void setIsReturnTechServiceFee(String isReturnTechServiceFee) {
        this.isReturnTechServiceFee = isReturnTechServiceFee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public BigDecimal getShopDeposit() {
        return shopDeposit;
    }

    public void setShopDeposit(BigDecimal shopDeposit) {
        this.shopDeposit = shopDeposit;
    }

    public BigDecimal getShopTechServiceFee() {
        return shopTechServiceFee;
    }

    public void setShopTechServiceFee(BigDecimal shopTechServiceFee) {
        this.shopTechServiceFee = shopTechServiceFee;
    }

    public String getTmallSerialNumber() {
        return tmallSerialNumber;
    }

    public void setTmallSerialNumber(String tmallSerialNumber) {
        this.tmallSerialNumber = tmallSerialNumber;
    }

    public BigDecimal getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(BigDecimal serviceFee) {
        this.serviceFee = serviceFee;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
