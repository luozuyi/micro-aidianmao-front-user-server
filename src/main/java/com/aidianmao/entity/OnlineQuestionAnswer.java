package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class OnlineQuestionAnswer implements Serializable {
    /**
     * 主键id
     * */
    private String id;
    /**
     * 创建时间
     * */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     * */
    private String delFlag;
    /**
     * 提问人id
     * */
    private String memberId;
    /**
     * 回复者id
     * */
    private String adminId;
    /**
     * 标题
     * */
    private String title;
    /**
     * 提问
     * */
    private String question;
    /**
     * 回复
     * */
    private String answer;
    /**
     * 回复时间
     * */
    private Date answerTime;
    /**
     * 提问类型 0:代理记账 1:天猫商城 2:求购信息 3:店铺估价
     * */
    private String type;
    /**
     * 浏览量
     * */
    private Integer browers;
    /**
     * 交易状态：0:交易前 1:交易中 2:交易后 3:其他
     */
    private String tradeType;
    /**
     * 状态 0:已提问 1:已回复
     */
    private String status;
    /**
     * 用户名称
     */
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Date getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Date answerTime) {
        this.answerTime = answerTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public Integer getBrowers() {
        return browers;
    }

    public void setBrowers(Integer browers) {
        this.browers = browers;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
}
