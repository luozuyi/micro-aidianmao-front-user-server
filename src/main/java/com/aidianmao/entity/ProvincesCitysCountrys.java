package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class ProvincesCitysCountrys implements Serializable {
    /**
     * 主键id
     */
    private String id;
    /**
     * 城市id
     */
    private String citysId;
    /**
     * 区id
     */
    private String countrysId;
    /**
     * 省id
     */
    private String provincesId;
    /**
     * 省名
     */
    private String provincesName;
    /**
     * 市名
     */
    private String citysName;
    /**
     * 区名
     */
    private String countrysName;
    /**
     * 详细地址名称
     */
    private String addressName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCitysId() {
        return citysId;
    }

    public void setCitysId(String citysId) {
        this.citysId = citysId == null ? null : citysId.trim();
    }

    public String getCountrysId() {
        return countrysId;
    }

    public void setCountrysId(String countrysId) {
        this.countrysId = countrysId == null ? null : countrysId.trim();
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId == null ? null : provincesId.trim();
    }

    public String getProvincesName() {
        return provincesName;
    }

    public void setProvincesName(String provincesName) {
        this.provincesName = provincesName == null ? null : provincesName.trim();
    }

    public String getCitysName() {
        return citysName;
    }

    public void setCitysName(String citysName) {
        this.citysName = citysName == null ? null : citysName.trim();
    }

    public String getCountrysName() {
        return countrysName;
    }

    public void setCountrysName(String countrysName) {
        this.countrysName = countrysName == null ? null : countrysName.trim();
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName == null ? null : addressName.trim();
    }

    public ProvincesCitysCountrys() {
    }

    public ProvincesCitysCountrys(String id, String addressName) {
        this.id = id;
        this.addressName = addressName;
    }

    public ProvincesCitysCountrys(String id, String citysId, String provincesId, String addressName) {
        this.id = id;
        this.citysId = citysId;
        this.provincesId = provincesId;
        this.addressName = addressName;
    }

    public ProvincesCitysCountrys(String id, String citysId, String provincesId, String provincesName, String citysName, String addressName) {
        this.id = id;
        this.citysId = citysId;
        this.provincesId = provincesId;
        this.provincesName = provincesName;
        this.citysName = citysName;
        this.addressName = addressName;
    }
}