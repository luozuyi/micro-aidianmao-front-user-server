package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 天猫店铺部分信息
 */
public class TmallLess implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 店铺介绍
     */
    private String shopProfile;
    /**
     * 行业类型 0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类
     * 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健
     * 8:珠宝/首饰 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像
     * 13:玩乐/收藏 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     */
    private String industryType;
    /**
     * 商城类型  0:旗舰店  1: 专营店 2: 专卖店
     */
    private String shopType;
    /**
     * 商标类型 0:R标  1: TM标
     */
    private String trademarkType;
    /**
     * 宝贝与描述相符评分
     */
    private String productDescriptionPoint;
    /**
     * 卖家服务态度评分
     */
    private String serviceAttitudePoint;
    /**
     * 卖家发货速度评分
     */
    private String deliverySpeedPoint;
    /**
     * 消费者保障金
     */
    private BigDecimal shopDeposit;
    /**
     * 店铺技术年费
     */
    private BigDecimal shopTechServiceFee;
    /**
     * 公司注册资金
     */
    private BigDecimal companyRegisterMoney;

    /**
     * 店铺价格
     */
    private BigDecimal shopPrice;
    /**
     * 是否完成营业额指标  0:完成  1: 没有完成
     */
    private String isCompletedTurnoverIndex;

    /**
     * 是否扣分 0:是 1：否
     */
    private String isDeduction;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     */
    private String taxPayerType;

    /**
     * 查询起始金额输入
     */
    private String startNum;

    /**
     * 查询终止金额输入
     */
    private String endNum;

    /**
     * 城市Id
     */
    private String provincesId;
    /**
     * 一般违规扣分
     */
    private String generalViolationPoint;
    /**
     * 严重违规扣分
     */
    private String seriousViolationPoint;

    /**
     * 状态 店铺在平台的状态 0:审核中 1:审核通过 2:审核不通过 3:下架 4:已付款 5:交接中 6:已售出
     */
    private String status;
    /**
     * 时间
     */
    private Date createTime;
    /**
     * 天猫商城编号
     */
    private String tmallSerialNumber;
    /**
     * 审核时间
     */
    private String approveTime;
    /**
     * 价格排序字段
     * 1:升序 2:降序
     */
    private String shopPriceOrder;
    /**
     * 上架时间排序字段
     * 1:由近到远 2:由远到近
     */
    private String approveTimeOrder;
    /**
     *宝贝与描述相符评分图标(1.低、2.持平、3.高)
     */
    private String productDescriptionPointIco;
    /**
     *卖家服务态度评分图标(1.低、2.持平、3.高)
     */
    private String serviceAttitudePointIco;
    /**
     *卖家发货速度评分图标(1.低、2.持平、3.高)
     */
    private String deliverySpeedPointIco;

    public String getProductDescriptionPointIco() {
        return productDescriptionPointIco;
    }

    public void setProductDescriptionPointIco(String productDescriptionPointIco) {
        this.productDescriptionPointIco = productDescriptionPointIco;
    }

    public String getServiceAttitudePointIco() {
        return serviceAttitudePointIco;
    }

    public void setServiceAttitudePointIco(String serviceAttitudePointIco) {
        this.serviceAttitudePointIco = serviceAttitudePointIco;
    }

    public String getDeliverySpeedPointIco() {
        return deliverySpeedPointIco;
    }

    public void setDeliverySpeedPointIco(String deliverySpeedPointIco) {
        this.deliverySpeedPointIco = deliverySpeedPointIco;
    }

    public String getShopPriceOrder() {
        return shopPriceOrder;
    }

    public void setShopPriceOrder(String shopPriceOrder) {
        this.shopPriceOrder = shopPriceOrder;
    }

    public String getApproveTimeOrder() {
        return approveTimeOrder;
    }

    public void setApproveTimeOrder(String approveTimeOrder) {
        this.approveTimeOrder = approveTimeOrder;
    }

    public String getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(String approveTime) {
        this.approveTime = approveTime;
    }

    public String getTmallSerialNumber() {
        return tmallSerialNumber;
    }

    public void setTmallSerialNumber(String tmallSerialNumber) {
        this.tmallSerialNumber = tmallSerialNumber;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getGeneralViolationPoint() {
        return generalViolationPoint;
    }

    public void setGeneralViolationPoint(String generalViolationPoint) {
        this.generalViolationPoint = generalViolationPoint;
    }

    public String getSeriousViolationPoint() {
        return seriousViolationPoint;
    }

    public void setSeriousViolationPoint(String seriousViolationPoint) {
        this.seriousViolationPoint = seriousViolationPoint;
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId;
    }

    public String getStartNum() {
        return startNum;
    }

    public void setStartNum(String startNum) {
        this.startNum = startNum;
    }

    public String getEndNum() {
        return endNum;
    }

    public void setEndNum(String endNum) {
        this.endNum = endNum;
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType;
    }

    public String getProductDescriptionPoint() {
        return productDescriptionPoint;
    }

    public void setProductDescriptionPoint(String productDescriptionPoint) {
        this.productDescriptionPoint = productDescriptionPoint;
    }

    public String getServiceAttitudePoint() {
        return serviceAttitudePoint;
    }

    public void setServiceAttitudePoint(String serviceAttitudePoint) {
        this.serviceAttitudePoint = serviceAttitudePoint;
    }

    public String getDeliverySpeedPoint() {
        return deliverySpeedPoint;
    }

    public void setDeliverySpeedPoint(String deliverySpeedPoint) {
        this.deliverySpeedPoint = deliverySpeedPoint;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopProfile() {
        return shopProfile;
    }

    public void setShopProfile(String shopProfile) {
        this.shopProfile = shopProfile;
    }

    public String getShopType() {
        return shopType;
    }

    public void setShopType(String shopType) {
        this.shopType = shopType;
    }

    public String getTrademarkType() {
        return trademarkType;
    }

    public void setTrademarkType(String trademarkType) {
        this.trademarkType = trademarkType;
    }

    public BigDecimal getShopDeposit() {
        return shopDeposit;
    }

    public void setShopDeposit(BigDecimal shopDeposit) {
        this.shopDeposit = shopDeposit;
    }

    public BigDecimal getShopTechServiceFee() {
        return shopTechServiceFee;
    }

    public void setShopTechServiceFee(BigDecimal shopTechServiceFee) {
        this.shopTechServiceFee = shopTechServiceFee;
    }

    public BigDecimal getCompanyRegisterMoney() {
        return companyRegisterMoney;
    }

    public void setCompanyRegisterMoney(BigDecimal companyRegisterMoney) {
        this.companyRegisterMoney = companyRegisterMoney;
    }

    public BigDecimal getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(BigDecimal shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getIsCompletedTurnoverIndex() {
        return isCompletedTurnoverIndex;
    }

    public void setIsCompletedTurnoverIndex(String isCompletedTurnoverIndex) {
        this.isCompletedTurnoverIndex = isCompletedTurnoverIndex;
    }

    public String getIsDeduction() {
        return isDeduction;
    }

    public void setIsDeduction(String isDeduction) {
        this.isDeduction = isDeduction;
    }
}
