package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

public class MemberBankInfo implements Serializable{
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 银行id
     */
    private String bankId;
    /**
     * 银行账号
     */
    private String account;

    /**
     * 第二次输入银行卡号
     */
    private String accountAgain;
    /**
     * 详细支行信息id
     */
    private String provinceCityCountyId;
    /**
     * 是否默认 0:默认 1:不是默认
     */
    private String isDefault;

    private String realName;

    private String bankDetail;

    private String citysId;

    private String provincesId;
    /**
     * 主行名称
     */
    private String bankName;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCitysId() {
        return citysId;
    }

    public void setCitysId(String citysId) {
        this.citysId = citysId;
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId;
    }

    public String getAccountAgain() {
        return accountAgain;
    }

    public void setAccountAgain(String accountAgain) {
        this.accountAgain = accountAgain;
    }

    public String getBankDetail() {
        return bankDetail;
    }

    public void setBankDetail(String bankDetail) {
        this.bankDetail = bankDetail;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId == null ? null : bankId.trim();
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getProvinceCityCountyId() {
        return provinceCityCountyId;
    }

    public void setProvinceCityCountyId(String provinceCityCountyId) {
        this.provinceCityCountyId = provinceCityCountyId == null ? null : provinceCityCountyId.trim();
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault == null ? null : isDefault.trim();
    }
}