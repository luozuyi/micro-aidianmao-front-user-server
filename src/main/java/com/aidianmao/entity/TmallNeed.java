package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class TmallNeed implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 求购主题
     */
    private String needSubject;
    /**
     * 求购者电话
     */
    private String phone;
    /**
     * 求购产品类目0:服饰鞋包 1:美容护理 2:母婴用品 3:3C数码类 4:运动/户外 5:家装家饰 6:家居用品 7:食品/保健 8:珠宝/首饰 9:游戏/话费 10:生活服务 11:汽车配件 12:书籍音像 13:玩乐/收藏 14:万用百搭 15:其他行业 16:医药健康 17:大家电
     */
    private String needType;
    /**
     * 天猫商城类型 0:旗舰店  1: 专营店 2: 专卖店
     */
    private String tmallType;
    /**
     * 商标类型 0:R标  1:TM标
     */
    private String brandType;
    /**
     * 求购数量
     */
    private Integer count;
    /**
     * 价格区间(起始价格)
     */
    private BigDecimal price;
    /**
     * 店铺地址
     */
    private String addressId;
    /**
     * 买家描述
     */
    private String remark;
    /**
     * 纳税人性质  0:一般纳税人  1: 小规模纳税人
     */
    private String taxPayerType;
    /**
     * 所需证件 身份证 手持身份证 营业执照 手持营业执照
     */
    private String needCredentials;
    /**
     * 会员id
     */
    private String memberId;
    /**
     * 审核状态 0:审核中 1:审核通过 2:审核不通过 3:下架
     */
    private String status;
    /**
     * 审核人关联的id
     */
    private String adminId;
    /**
     * 审核时间
     */
    private Date approveTime;
    /**
     * 创店时间(年份)
     */
    private String shopCreateTime;
    /**
     * 审核备注
     */
    private String note;
    /**
     * 价格区间(截至价格)
     */
    private BigDecimal endPrice;
    /**
     * 姓名
     */
    private String name;
    /**
     * 编号
     */
    private String code;
    /**
     * 城市Id
     */
    private String citysId;
    /**
     * 省id
     */
    private String provincesId;
    /**
     * 显示信息
     */
    private String playInfo;

    public String getPlayInfo() {
        return playInfo;
    }

    public void setPlayInfo(String playInfo) {
        this.playInfo = playInfo;
    }

    public String getCitysId() {
        return citysId;
    }

    public void setCitysId(String citysId) {
        this.citysId = citysId;
    }

    public String getProvincesId() {
        return provincesId;
    }

    public void setProvincesId(String provincesId) {
        this.provincesId = provincesId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getNeedSubject() {
        return needSubject;
    }

    public void setNeedSubject(String needSubject) {
        this.needSubject = needSubject == null ? null : needSubject.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getNeedType() {
        return needType;
    }

    public void setNeedType(String needType) {
        this.needType = needType == null ? null : needType.trim();
    }

    public String getTmallType() {
        return tmallType;
    }

    public void setTmallType(String tmallType) {
        this.tmallType = tmallType == null ? null : tmallType.trim();
    }

    public String getBrandType() {
        return brandType;
    }

    public void setBrandType(String brandType) {
        this.brandType = brandType == null ? null : brandType.trim();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId == null ? null : addressId.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getTaxPayerType() {
        return taxPayerType;
    }

    public void setTaxPayerType(String taxPayerType) {
        this.taxPayerType = taxPayerType == null ? null : taxPayerType.trim();
    }

    public String getNeedCredentials() {
        return needCredentials;
    }

    public void setNeedCredentials(String needCredentials) {
        this.needCredentials = needCredentials == null ? null : needCredentials.trim();
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public Date getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(Date approveTime) {
        this.approveTime = approveTime;
    }

    public String getShopCreateTime() {
        return shopCreateTime;
    }

    public void setShopCreateTime(String shopCreateTime) {
        this.shopCreateTime = shopCreateTime == null ? null : shopCreateTime.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public BigDecimal getEndPrice() {
        return endPrice;
    }

    public void setEndPrice(BigDecimal endPrice) {
        this.endPrice = endPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}