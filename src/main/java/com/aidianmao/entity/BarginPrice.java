package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BarginPrice implements Serializable {
    /**
     * 主键id
     * */
    private String id;
    /**
     * 创建时间
     * */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     * */
    private String delFlag;
    /**
     * 会员id(非必填)
     * */
    private String memberId;
    /**
     * 期望价格(不能低于原价的80%)
     * */
    private BigDecimal price;
    /**
     * 电话号码
     * */
    private String phone;
    /**
     * 天猫店铺id
     * */
    private String tmallId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTmallId() {
        return tmallId;
    }

    public void setTmallId(String tmallId) {
        this.tmallId = tmallId;
    }
}
