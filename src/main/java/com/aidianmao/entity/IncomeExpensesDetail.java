package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class IncomeExpensesDetail implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 删除标志位 0:正常 1：删除 2：待删除
     */
    private String delFlag;
    /**
     * 收支类型 0:收入 1:支出
     */
    private String type;
    /**
     * 场景 0:充值 1:tmall订单支付 2:提现
     */
    private String moneyScene;
    /**
     * 金额
     */
    private BigDecimal amountMoney;
    /**
     * 会员id
     */
    private String memberId;

    /**
     * 查询操作时间起始值
     */
    private String startCTime;

    /**
     * 查询操作时间终止值
     */
    private String endCTime;

    /**
     * 查询起始金额输入
     */
    private BigDecimal startNum;

    /**
     * 查询终止金额输入
     */
    private BigDecimal endNum;

    public String getStartCTime() {
        return startCTime;
    }

    public void setStartCTime(String startCTime) {
        this.startCTime = startCTime;
    }

    public String getEndCTime() {
        return endCTime;
    }

    public void setEndCTime(String endCTime) {
        this.endCTime = endCTime;
    }

    public BigDecimal getStartNum() {
        return startNum;
    }

    public void setStartNum(BigDecimal startNum) {
        this.startNum = startNum;
    }

    public BigDecimal getEndNum() {
        return endNum;
    }

    public void setEndNum(BigDecimal endNum) {
        this.endNum = endNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getMoneyScene() {
        return moneyScene;
    }

    public void setMoneyScene(String moneyScene) {
        this.moneyScene = moneyScene == null ? null : moneyScene.trim();
    }

    public BigDecimal getAmountMoney() {
        return amountMoney;
    }

    public void setAmountMoney(BigDecimal amountMoney) {
        this.amountMoney = amountMoney;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    public IncomeExpensesDetail() {
    }



    public IncomeExpensesDetail(String id, Date createTime, String delFlag, String type, String moneyScene, BigDecimal amountMoney, String memberId) {
        this.id = id;
        this.createTime = createTime;
        this.delFlag = delFlag;
        this.type = type;
        this.moneyScene = moneyScene;
        this.amountMoney = amountMoney;
        this.memberId = memberId;

    }
}