package com.aidianmao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 冻结明细
 */
public class FreezeDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 冻结金额
     */
    private BigDecimal freezeMoney;
    /**
     * 冻结状态 0: 冻结中 1: 已解冻
     */
    private String status;

    /**
     * 冻结类型 0：天猫订单 1：提现
     */
    private String type;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getFreezeMoney() {
        return freezeMoney;
    }

    public void setFreezeMoney(BigDecimal freezeMoney) {
        this.freezeMoney = freezeMoney;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FreezeDetail() {
    }

    public FreezeDetail(Date createTime, BigDecimal freezeMoney, String status, String type) {
        this.createTime = createTime;
        this.freezeMoney = freezeMoney;
        this.status = status;
        this.type = type;
    }
}
