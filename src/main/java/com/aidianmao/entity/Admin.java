package com.aidianmao.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 管理人员实体类
 */
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键Id
     */
    private String id;
    /**
     * 管理员名字
     */
    private String adminName;
    /**
     * 头像图片路径地址
     */
    private String headImage;
    /**
     * 角色id
     */
    private String sysRoleId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 是否禁用0：否1：是
     */
    private String delFlag;
    /**
     * 最后登陆时间
     */
    private Date lastLoginTime;
    /**
     * 电话号码
     */
    private String telephone;
    /**
     * qq
     */
    private String qq;
    /**
     * 真实名字
     */
    private String realName;
    /**
     * 登陆次数
     */
    private Long loginCount;
    /**
     * 当前登陆ip
     */
    private String currentLoginIp;
    /**
     * 最后登陆ip
     */
    private String lastLoginIp;
    /**
     * 登录密码
     */
    private String password;
    /**
     * 小组id
     */
    private String groupId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage == null ? null : headImage.trim();
    }

    public String getSysRoleId() {
        return sysRoleId;
    }

    public void setSysRoleId(String sysRoleId) {
        this.sysRoleId = sysRoleId == null ? null : sysRoleId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag == null ? null : delFlag.trim();
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Long getLoginCount() {
        return loginCount;
    }

    public void setLoginCount(Long loginCount) {
        this.loginCount = loginCount;
    }

    public String getCurrentLoginIp() {
        return currentLoginIp;
    }

    public void setCurrentLoginIp(String currentLoginIp) {
        this.currentLoginIp = currentLoginIp == null ? null : currentLoginIp.trim();
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp == null ? null : lastLoginIp.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId == null ? null : groupId.trim();
    }
}