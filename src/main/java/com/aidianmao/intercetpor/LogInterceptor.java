package com.aidianmao.intercetpor;

import com.aidianmao.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogInterceptor implements HandlerInterceptor {
    private static Logger logger = LoggerFactory.getLogger(LogInterceptor.class);
    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        String ip = CommonUtil.getIpAddr(request);
        String method = request.getMethod();
        String url = request.getRequestURI();
        String uri = request.getRequestURI();
        logger.info("ip:"+ip+"  "+"url:"+url+"  uri:"+uri+"   "+method);
    }
}
