package com.aidianmao.intercetpor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Bean
    public HandlerInterceptor getMyInterceptor(){
        return new AddIntercetpor();
    }
    @Bean
    public HandlerInterceptor getLogoutInterceptor(){
        return new LogoutInterceptor();
    }
    @Bean
    public HandlerInterceptor getLogInterceptor(){
        return new LogInterceptor();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getMyInterceptor()).addPathPatterns("/**/auth/**");
        registry.addInterceptor(getLogoutInterceptor()).addPathPatterns("/**/user/logout");
        registry.addInterceptor(getLogInterceptor()).addPathPatterns("/**");
    }


}