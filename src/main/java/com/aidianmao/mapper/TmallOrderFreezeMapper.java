package com.aidianmao.mapper;

import com.aidianmao.entity.TmallOrderFreeze;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TmallOrderFreezeMapper {
    int deleteByPrimaryKey(String id);

    int insert(TmallOrderFreeze record);

    int insertSelective(TmallOrderFreeze record);

    TmallOrderFreeze selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallOrderFreeze record);

    int updateByPrimaryKey(TmallOrderFreeze record);

    List<TmallOrderFreeze> selectByTmallOrderId(String tmallOrderId);

    List<TmallOrderFreeze> selectByMemberId(String memberId);
}