package com.aidianmao.mapper;

import com.aidianmao.entity.AssessTmall;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AssessTmallMapper {
    int deleteByPrimaryKey(String id);

    int insert(AssessTmall assessTmall);

    int insertSelective(AssessTmall assessTmall);

    AssessTmall selecturl(String url);

    List<AssessTmall> selectAll();

    List<ModelMap> selectAllBySelection(Map<String, Object> params);

    Integer selectCount();
}
