package com.aidianmao.mapper;

import com.aidianmao.entity.FriendshipLink;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface FriendshipLinkMapper {
    int deleteByPrimaryKey(String id);

    int insert(FriendshipLink record);

    int insertSelective(FriendshipLink record);

    FriendshipLink selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(FriendshipLink record);

    int updateByPrimaryKey(FriendshipLink record);

    List<FriendshipLink> selectByOrder();
}