package com.aidianmao.mapper;

import com.aidianmao.entity.Accessory;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AccessoryMapper {
    
    int deleteByPrimaryKey(String id);

    int insert(Accessory record);

    int insertSelective(Accessory record);

    Accessory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Accessory record);

    int updateByPrimaryKey(Accessory record);
	
	public List<Accessory> selectAllProductPhotoBySelection(Map<String, Object> params);
	
	public List<Accessory> selectAllSelfProductPhotoBySelection(Map<String, Object> params);

}