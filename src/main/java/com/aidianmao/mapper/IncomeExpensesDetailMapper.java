package com.aidianmao.mapper;

import com.aidianmao.entity.IncomeExpensesDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface IncomeExpensesDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(IncomeExpensesDetail record);

    int insertSelective(IncomeExpensesDetail record);

    IncomeExpensesDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(IncomeExpensesDetail record);

    int updateByPrimaryKey(IncomeExpensesDetail record);

    List<IncomeExpensesDetail> selectSelective(IncomeExpensesDetail record);
}