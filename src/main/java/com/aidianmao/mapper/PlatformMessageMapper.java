package com.aidianmao.mapper;

import com.aidianmao.entity.PlatformMessage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface PlatformMessageMapper {
    int deleteByPrimaryKey(String id);

    int insert(PlatformMessage record);

    int insertSelective(PlatformMessage record);

    PlatformMessage selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(PlatformMessage record);

    int updateByPrimaryKey(PlatformMessage record);

    List<PlatformMessage> selectByMemberId(PlatformMessage record);
}