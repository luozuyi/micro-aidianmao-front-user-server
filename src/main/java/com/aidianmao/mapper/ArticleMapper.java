package com.aidianmao.mapper;

import com.aidianmao.entity.Article;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ArticleMapper {
    int deleteByPrimaryKey(String id);

    int insert(Article record);

    int insertSelective(Article record);

    Article selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Article record);

    int updateByPrimaryKey(Article record);

    List<ModelMap> selectAllBySelection(Map<String, Object> params);

    List<Map<String,Object>> selectByChannelId(String channelId);

    List<String> selectByParentId(String parentId);

    List<Map<String,Object>> selectDetailById(String id);

}