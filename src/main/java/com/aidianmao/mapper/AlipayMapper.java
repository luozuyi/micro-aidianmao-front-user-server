package com.aidianmao.mapper;


import com.aidianmao.entity.Alipay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AlipayMapper {
    int deleteByPrimaryKey(String id);

    int insert(Alipay alipay);

    int insertSelective(Alipay alipay);

    List<Alipay> selectAll();

    List<ModelMap> selectAllBySelection(Map<String, Object> params);

    List<Alipay> selectByMemberId(Alipay alipay);

    List<ModelMap> selectAllBySelectionId(Map<String, Object> params);

    List<Alipay> selectBySelection(Alipay alipay);
}
