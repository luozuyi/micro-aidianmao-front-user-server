package com.aidianmao.mapper;

import com.aidianmao.entity.Remittance;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface RemittanceMapper {
    int deleteByPrimaryKey(String id);

    int insert(Remittance record);

    int insertSelective(Remittance record);

    Remittance selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Remittance record);

    int updateByPrimaryKey(Remittance record);

    List<Remittance> selectSelective(Remittance record);
}