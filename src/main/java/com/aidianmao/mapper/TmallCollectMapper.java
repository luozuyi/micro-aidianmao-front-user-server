package com.aidianmao.mapper;

import com.aidianmao.entity.TmallCollect;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface TmallCollectMapper {
    int deleteByPrimaryKey(String id);

    int insert(TmallCollect record);

    int insertSelective(TmallCollect record);

    TmallCollect selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallCollect record);

    int updateByPrimaryKey(TmallCollect record);

    int selectIsCollect(TmallCollect record);

    TmallCollect selectByMemberAndTmall(TmallCollect record);
}