package com.aidianmao.mapper;

import com.aidianmao.entity.ShopCart;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface ShopCartMapper {
    int deleteByPrimaryKey(String id);

    int insert(ShopCart record);

    int insertSelective(ShopCart record);

    ShopCart selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ShopCart record);

    int updateByPrimaryKey(ShopCart record);

    List<ShopCart> selectShopCart(String memberId);

    ShopCart selectSelective(ShopCart record);


    List<ModelMap> selectAllBySelection(Map<String, Object> params);

}