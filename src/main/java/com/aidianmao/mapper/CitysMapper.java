package com.aidianmao.mapper;

import com.aidianmao.entity.Citys;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;

@Mapper
@Repository
public interface CitysMapper extends BaseMapper {
    Citys selectKey(String citysName);

    List<Citys> selectByName(String citysName);

    List<Citys> selectByProvinceId(String provincesId);

    Citys selectById(String id);
}
