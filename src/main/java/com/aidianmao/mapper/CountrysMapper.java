package com.aidianmao.mapper;

import com.aidianmao.entity.Countrys;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface CountrysMapper extends BaseMapper {
    Countrys selectKey(String id);
}
