package com.aidianmao.mapper;

import com.aidianmao.entity.WithdrawDeposit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface WithdrawDepositMapper {
    int deleteByPrimaryKey(String id);

    int insert(WithdrawDeposit record);

    int insertSelective(WithdrawDeposit record);

    WithdrawDeposit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(WithdrawDeposit record);

    int updateByPrimaryKey(WithdrawDeposit record);

    List<WithdrawDeposit> selectByMemberId(WithdrawDeposit record);
}