package com.aidianmao.mapper;

import com.aidianmao.entity.Tmall;
import com.aidianmao.entity.TmallLess;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TmallMapper {
    int deleteByPrimaryKey(String id);

    int insert(Tmall record);

    int insertSelective(Tmall record);

    Tmall selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Tmall record);

    int updateByPrimaryKey(Tmall record);

    List<TmallLess> selectAllTmall(TmallLess record);

    List<Tmall> selectSelective(Tmall record);

    List<TmallLess> selectForCollect(String memberId);

    List<TmallLess> selectFourTmall();

    Integer limitSale(String memberId);

    Integer selectTmallCount();
}