package com.aidianmao.mapper;

import com.aidianmao.entity.Proposal;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ProposalMapper {
    int deleteByPrimaryKey(String id);

    int insert(Proposal record);

    int insertSelective(Proposal record);

    Proposal selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Proposal record);

    int updateByPrimaryKey(Proposal record);

    List<Proposal> selectByMemberId(String memberId);

    Integer selectByDay(String memberId);
}