package com.aidianmao.mapper;

import com.aidianmao.entity.BarginPrice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface BarginPriceMapper {
    int deleteByPrimaryKey(String id);

    int insert(BarginPrice barginPrice);

    int insertSelective(BarginPrice barginPrice);

    List<BarginPrice> selectAll();

    List<ModelMap> selectAllBySelection(Map<String, Object> params);
}
