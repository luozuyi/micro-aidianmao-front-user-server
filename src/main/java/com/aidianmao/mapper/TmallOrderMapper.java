package com.aidianmao.mapper;

import com.aidianmao.entity.TmallOrder;
import com.aidianmao.entity.TmallOrderLess;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallOrderMapper {
    int deleteByPrimaryKey(String id);

    int insert(TmallOrder record);

    int insertSelective(TmallOrder record);

    TmallOrder selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallOrder record);

    int updateByPrimaryKey(TmallOrder record);

    List<Map<String,Object>> selectAll(Map<String, Object> params);

    TmallOrder selectByBuyMemberIdAndTmallId(TmallOrder record);

    List<TmallOrder> selectByTmallId(String tmallId);

    List<TmallOrder> selectAllBySelection(Map<String, Object> params);

    List<TmallOrder> selectByTime();

    List<TmallOrderLess> selectForSale(TmallOrder record);

    List<TmallOrderLess> selectList(TmallOrder record);

    TmallOrderLess selectById(String id);

    List<TmallOrderLess> selectListSelective(TmallOrder record);

    List<TmallOrderLess> selectForBuy(String buyMemberId);
}