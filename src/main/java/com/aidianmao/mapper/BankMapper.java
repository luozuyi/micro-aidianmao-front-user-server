package com.aidianmao.mapper;

import com.aidianmao.entity.Bank;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BankMapper {
    int deleteByPrimaryKey(String id);

    int insert(Bank record);

    int insertSelective(Bank record);

    Bank selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Bank record);

    int updateByPrimaryKey(Bank record);

    List<Bank> selectAll();
}