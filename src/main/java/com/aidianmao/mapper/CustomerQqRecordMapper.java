package com.aidianmao.mapper;

import com.aidianmao.entity.CustomerQqRecord;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CustomerQqRecordMapper {
    List<Map<String,Object>> selectAll(Map<String, Object> params);

    int deleteByPrimaryKey(String id);

    int insert(CustomerQqRecord record);

    int insertSelective(CustomerQqRecord record);

    CustomerQqRecord selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CustomerQqRecord record);

    int updateByPrimaryKey(CustomerQqRecord record);
}