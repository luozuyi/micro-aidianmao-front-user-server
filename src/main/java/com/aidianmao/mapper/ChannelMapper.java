package com.aidianmao.mapper;

import com.aidianmao.entity.Channel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface ChannelMapper {
    int deleteByPrimaryKey(String id);

    int insert(Channel record);

    int insertSelective(Channel record);

    Channel selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Channel record);

    int updateByPrimaryKey(Channel record);

    List<ModelMap> selectAllBySelection(Map<String, Object> params);

    List<Channel> selectAll();

    List<Channel> selectParentChannel();

    Channel selectByChannelName(String record);

    List<Channel> selectByParentId(String id);
}