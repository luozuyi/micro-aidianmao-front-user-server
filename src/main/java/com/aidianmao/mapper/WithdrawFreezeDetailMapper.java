package com.aidianmao.mapper;

import com.aidianmao.entity.WithdrawFreezeDetail;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface WithdrawFreezeDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(WithdrawFreezeDetail record);

    int insertSelective(WithdrawFreezeDetail record);

    WithdrawFreezeDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(WithdrawFreezeDetail record);

    int updateByPrimaryKey(WithdrawFreezeDetail record);

    List<WithdrawFreezeDetail> selectByMemberId(String memberId);
}