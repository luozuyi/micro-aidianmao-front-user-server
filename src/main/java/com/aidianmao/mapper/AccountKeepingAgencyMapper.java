package com.aidianmao.mapper;

import com.aidianmao.entity.AccountKeepingAgency;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface AccountKeepingAgencyMapper{
    int deleteByPrimaryKey(String id);

    int insert(AccountKeepingAgency record);

    int insertSelective(AccountKeepingAgency record);

    AccountKeepingAgency selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(AccountKeepingAgency record);

    int updateByPrimaryKey(AccountKeepingAgency record);

    List<AccountKeepingAgency> selectAll();

    List<ModelMap> selectAllBySelection(Map<String, Object> params);

    List<AccountKeepingAgency> selectByTime();

    Integer limitAccountKeepingAgency(String ipAddress);
}