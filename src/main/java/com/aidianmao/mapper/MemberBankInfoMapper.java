package com.aidianmao.mapper;

import com.aidianmao.entity.MemberBankInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MemberBankInfoMapper {
    int deleteByPrimaryKey(String id);

    int insert(MemberBankInfo record);

    int insertSelective(MemberBankInfo record);

    MemberBankInfo selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(MemberBankInfo record);

    int updateByPrimaryKey(MemberBankInfo record);

    List<MemberBankInfo> selectByAccount(String account);

    List<MemberBankInfo> selectByMemberId(String memberId);

    List<MemberBankInfo> selectMemberBankInfo(String memberId);

    MemberBankInfo selectDefault(String memberId);
}