package com.aidianmao.mapper;

import com.aidianmao.entity.TmallNeed;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface TmallNeedMapper {
    int deleteByPrimaryKey(String id);

    int insert(TmallNeed record);

    int insertSelective(TmallNeed record);

    TmallNeed selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallNeed record);

    int updateByPrimaryKey(TmallNeed record);

    List<TmallNeed> selectAllBySelection(Map<String, Object> params);

    List<TmallNeed> selectSelection(TmallNeed record);

    Integer selectByDay(String memberId);

    Integer selectTmallCount();

}