package com.aidianmao.mapper;


import com.aidianmao.entity.OnlineQuestionAnswer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface OnlineQuestionAnswerMapper {
    int deleteByPrimaryKey(String id);

    int insert(OnlineQuestionAnswer onlineQuestionAnswer);

    int insertSelective(OnlineQuestionAnswer onlineQuestionAnswer);

    OnlineQuestionAnswer selecturl(String url);

    List<OnlineQuestionAnswer> selectAll();

    List<ModelMap> selectAllBySelection(OnlineQuestionAnswer record);

    List<ModelMap> selectAllByMember(OnlineQuestionAnswer record);

    List<OnlineQuestionAnswer> selectByExample();

    Integer selectByDay(String memberId);

    Integer selectCountSelection(OnlineQuestionAnswer record);

    Integer selectAllCount(OnlineQuestionAnswer onlineQuestionAnswer);

    OnlineQuestionAnswer selectById(String id);

    int updateByPrimaryKeySelective(OnlineQuestionAnswer record);
}
