package com.aidianmao.mapper;

import com.aidianmao.entity.TmallFastCommit;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TmallFastCommitMapper {
    int deleteByPrimaryKey(String id);

    int insert(TmallFastCommit record);

    int insertSelective(TmallFastCommit record);

    TmallFastCommit selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TmallFastCommit record);

    int updateByPrimaryKey(TmallFastCommit record);

    Integer limitFastCommit(String ipAddress);
}