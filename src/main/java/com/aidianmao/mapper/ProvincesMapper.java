package com.aidianmao.mapper;

import com.aidianmao.entity.Provinces;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProvincesMapper extends BaseMapper {

    Provinces selectByKey(String id);

    Provinces selectKey(String id);

    List<Provinces> selectByAll();
}
