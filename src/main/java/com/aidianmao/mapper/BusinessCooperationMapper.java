package com.aidianmao.mapper;

import com.aidianmao.entity.BusinessCooperation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface BusinessCooperationMapper {
    int deleteByPrimaryKey(String id);

    int insert(BusinessCooperation record);

    int insertSelective(BusinessCooperation record);

    BusinessCooperation selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(BusinessCooperation record);

    int updateByPrimaryKey(BusinessCooperation record);

    Integer limitCooperation(String ipAddress);
}