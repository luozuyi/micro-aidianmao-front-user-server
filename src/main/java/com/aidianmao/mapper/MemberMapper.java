package com.aidianmao.mapper;

import com.aidianmao.entity.Member;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MemberMapper {
    int deleteByPrimaryKey(String id);

    int insert(Member record);

    int insertSelective(Member record);

    Member selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Member record);

    int updateByPrimaryKey(Member record);

    Member selectByUserName(String userName);

    Member selectByUserNameOrPhone(String loginWords);

    Member selectByPhone(String phone);

    String selectByPassword(String id);

    int updateByPassword(Member member);

    String selectAdminByMember(String id);

}