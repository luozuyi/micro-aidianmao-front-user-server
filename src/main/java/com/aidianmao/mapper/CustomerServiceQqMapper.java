package com.aidianmao.mapper;

import com.aidianmao.entity.CustomerServiceQq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CustomerServiceQqMapper {
    int deleteByPrimaryKey(String id);

    int insert(CustomerServiceQq record);

    int insertSelective(CustomerServiceQq record);

    CustomerServiceQq selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(CustomerServiceQq record);

    int updateByPrimaryKey(CustomerServiceQq record);

    List<CustomerServiceQq> selectByType(String type);

}