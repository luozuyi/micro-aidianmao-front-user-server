package com.aidianmao.serviceImpl;

import com.aidianmao.entity.TmallFastCommit;
import com.aidianmao.mapper.TmallFastCommitMapper;
import com.aidianmao.service.TmallFastCommitService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PatternUtil;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
@Transactional
public class TmallFastCommitServiceImpl implements TmallFastCommitService {

    @Autowired
    private TmallFastCommitMapper tmallFastCommitMapper;
    @Autowired
    private HttpServletRequest request;

    /**
     * 快速挂店
     * @param record
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result fastSale(TmallFastCommit record, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        String ipAddress = CommonUtil.getIpAddr(request);
        try {
            if (StringUtils.isEmpty(record.getPhone())){
                code="-2";
                msg="手机号码不能为空";
            }else if(!PatternUtil.patternString(record.getPhone(),"mobile")){
                code="-3";
                msg="手机号格式不正确";
            }else if (tmallFastCommitMapper.limitFastCommit(ipAddress)>=5){
                code="-4";
                msg="您当日无法再进行此操作";
            } else {
                record.setId(CommonUtil.getUUID());
                record.setCreateTime(new Date());
                record.setDelFlag("0");
                record.setStatus("0");
                record.setIpAddress(ipAddress);
                tmallFastCommitMapper.insertSelective(record);
                code = Constants.SUCCESS;
                msg = "提交成功";
            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
