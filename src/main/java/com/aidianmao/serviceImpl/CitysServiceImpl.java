package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Citys;
import com.aidianmao.mapper.CitysMapper;
import com.aidianmao.service.CitysService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PatternUtil;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class CitysServiceImpl extends BaseServiceImpl<CitysMapper> implements CitysService {

    @Autowired
    private CitysMapper citysMapper;

    @Override
    public Result getAllCity(String provincesId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Citys> citys = citysMapper.selectByProvinceId(provincesId);
            result.setData(citys);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
