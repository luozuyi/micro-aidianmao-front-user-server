package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Alipay;
import com.aidianmao.entity.Bank;
import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.entity.Remittance;
import com.aidianmao.mapper.BankMapper;
import com.aidianmao.mapper.ProvincesCitysCountrysMapper;
import com.aidianmao.mapper.RemittanceMapper;
import com.aidianmao.service.RemittanceService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class RemittanceServiceImpl implements RemittanceService{
    @Autowired
    private RemittanceMapper remittanceMapper;
    @Autowired
    private FastFileStorageClient fastFileStorageClient;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Override
    public Result add(Remittance remittance, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(remittance.getMakeCollectionsAccout())){
                code = "-3";
                msg = "转账账户不能为空";
            }else if(remittance.getMoney() == null){
                code = "-4";
                msg = "转账金额不能为空";
            }else if(remittance.getMoney().compareTo(BigDecimal.ZERO)<=0){
                code = "-9";
                msg = "转账金额不能为负或者零";
            }else if(StringUtils.isBlank(remittance.getPaymentDocumentImage())){
                code = "-5";
                msg = "汇款图片不能为空";
            }else if(StringUtils.isBlank(remittance.getRemitBankName())){
                code = "-6";
                msg = "转账银行不能为空";
            }else if(remittance.getRemitTime() == null){
                code = "-7";
                msg = "转账日期不能为空";
            } else if (StringUtils.isEmpty(remittance.getName())) {
                code="-8";
                msg="转账人姓名不能为空";
            } else {
                //新增详细支行的记录
                String id=CommonUtil.getUUID();
                provincesCitysCountrysMapper.insertSelective(new ProvincesCitysCountrys(id,remittance.getRemitBankName()));
                remittance.setId(CommonUtil.getUUID());
                remittance.setCreateTime(new Date());
                remittance.setDelFlag("0");
                remittance.setStatus("0");
                remittance.setMemberId(memberId);
                remittance.setRemitBankId(id);
                remittanceMapper.insertSelective(remittance);
                code = Constants.SUCCESS;
                msg = "成功";

            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    private boolean isImage(String str){
        String [] strs = {"png","jpg","jpeg","gif","bmp"};
        for (String s : strs) {
            if(s.equalsIgnoreCase(str)){
                return true;
            }
        }
        return false;
    }

    /**
     * @param file
     * @return
     */
    @Override
    public Result addSelfProductDetailPhoto(MultipartFile file) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            String fileName = file.getOriginalFilename();
            long size = file.getSize();
            if (size>2000000L){
                code="-4";
                msg="图片大小不能超过2m";
            }else {
                String prefix = fileName.substring(fileName.lastIndexOf(".") + 1);
                if (isImage(prefix)) {
                    StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), FilenameUtils.getExtension(file.getOriginalFilename()), null);
                    String fileUrl = storePath.getFullPath();
                    result.setData(fileUrl);
                    code = "0";
                    msg = "成功";
                } else {
                    code = "-3";
                    msg = "只能上传图片";
                }
            }
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    @Override
    public Result getList(Remittance remittance, String aidianmaoMemberToken,Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String msg = "初始化";
        String code = Constants.FAIL;
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            remittance.setMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Remittance> remittances = remittanceMapper.selectSelective(remittance);
            PageInfo<Remittance> page = new PageInfo<>(remittances);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = "-2";
            msg = "上传出错";
            e.printStackTrace();
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

}
