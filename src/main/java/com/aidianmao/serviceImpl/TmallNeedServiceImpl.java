package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.esEntity.EsTmallNeed;
import com.aidianmao.mapper.*;
import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.service.TmallNeedService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@Transactional
@Service
public class TmallNeedServiceImpl implements TmallNeedService {

    @Autowired
    private TmallNeedMapper tmallNeedMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Autowired
    private CitysMapper citysMapper;
    @Autowired
    private ProvincesMapper provincesMapper;
    @Autowired
    private CountrysMapper countrysMapper;
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private CustomerServiceQqService serviceQqService;


    @Override
    public Result addTmallNeed(TmallNeed tmallNeed, String aidianmaoMemberToken, ProvincesCitysCountrys provincesCitysCountrys){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(tmallNeed.getNeedSubject())){
                code="-3";
                msg="求购标题不能为空";
            }else if(tmallNeed.getNeedSubject().length() < 6 || tmallNeed.getNeedSubject().length() > 30){
                code = "-19";
                msg = "求购标题在6-30字符";
            }else if(StringUtils.isBlank(tmallNeed.getRemark())){
                code="-4";
                msg="求购描述不能为空";
            }else if(tmallNeed.getRemark().length() < 20 || tmallNeed.getRemark().length() > 200){
                code = "-18";
                msg = "求购描述在20-200字符";
            }else if(tmallNeed.getPrice()==null&&tmallNeed.getPrice().compareTo(BigDecimal.ZERO)==-1){
                code="-5";
                msg="求购价格不能为空且不能小于零";
            }else if(tmallNeed.getEndPrice()==null){
                code="-6";
                msg="价格区间(截至价格)不能为空";
            }else if(tmallNeed.getCount()==null&&tmallNeed.getCount()==-1){
                code="-7";
                msg="求购数量不能为空且不能小于零";
            }else if(tmallNeed.getCount()>=100){
                code="-20";
                msg="求购数量请在100以内";
            }else if(StringUtils.isBlank(tmallNeed.getNeedType())){
                code="-8";
                msg="求购产品类目不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getTmallType())){
                code="-9";
                msg="天猫商城类型不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getBrandType())){
                code="-10";
                msg="商标类型不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getPhone())){
                code="-11";
                msg="手机号不能为空";
            }else if(StringUtils.isBlank(tmallNeed.getName())){
                code="-12";
                msg="名字不能为空";
            }/*else if(tmallNeed.getName().length()<2||tmallNeed.getName().length()>5){
                code="-21";
                msg="名字不能小于2大于5";
            }*/else if (!PatternUtil.patternString(tmallNeed.getPhone(),"mobile")){
                code="-13";
                msg="手机号格式错误";
            }else if(tmallNeed.getCount()<=0){
                code="-14";
                msg="求购数量不能为零也不能为负数";
            }else if(tmallNeed.getPrice().compareTo(BigDecimal.ZERO)==-1||tmallNeed.getPrice().equals(BigDecimal.ZERO)){
                code="-15";
                msg="求购价格不能为零也不能为负数";
            }else if(StringUtils.isBlank(tmallNeed.getShopCreateTime())){
                code="-16";
                msg="创店时间(年份)不能为空";
            }else if(tmallNeed.getPrice().compareTo(tmallNeed.getEndPrice())==1){
                code="-17";
                msg="起始价格不能大于结束价格";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-39";
                msg = "请选择省";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-40";
                msg = "请选择城市";
            }else if (StringUtils.isEmpty(tmallNeed.getTaxPayerType())) {
                code="-41";
                msg="纳税人性质不能为空";
            } else {
//                Citys citys=citysMapper.selectKey(provincesCitysCountrys.getCitysId());
//                Provinces provinces=provincesMapper.selectKey(provincesCitysCountrys.getProvincesId());
//                Countrys country=countrysMapper.selectKey(provincesCitysCountrys.getCountrysId());
//                if(citys==null){
//                    code="-17";
//                    msg="此市不存在";
//                }else if (provinces==null){
//                    code="-18";
//                    msg="此省不存在";
//                }else if (country==null){
//                    code="-19";
//                    msg="此区不存在";
//                }else{
//                    String provincesCitysCountrysId = CommonUtil.getUUID();
//                    provincesCitysCountrys.setId(provincesCitysCountrysId);
//                    provincesCitysCountrysMapper.insert(provincesCitysCountrys);
                    //遍历数组
//                    String str="";
//                    for (int i = 0; i < array.length; i++) {
//                        str += array[i];
//                        if(i<array.length-1){
//                            str += ",";
//                        }
//                    }
                    String provincesCitysCountrysId = CommonUtil.getUUID();
                    provincesCitysCountrys.setId(provincesCitysCountrysId);
                    provincesCitysCountrysMapper.insertSelective(provincesCitysCountrys);
                    tmallNeed.setId(CommonUtil.getUUID());
                    tmallNeed.setCreateTime(new Date());
                    tmallNeed.setDelFlag("0");
                    //求购编号，按照当前时间+6位随机数
                    tmallNeed.setCode(RandomCodeUtil.getRandomCode(6));
                    tmallNeed.setMemberId(memberId);
                    tmallNeed.setStatus("0");
                    tmallNeed.setAddressId(provincesCitysCountrysId);
//                    tmallNeed.setNeedCredentials(str);
//                    tmallNeed.setAddressId(provincesCitysCountrysId);
                    Integer list=tmallNeedMapper.selectByDay(memberId);
                    if(list<50){
                        tmallNeedMapper.insertSelective(tmallNeed);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }else {
                        code="-18";
                        msg="您当日已无法再进行此操作！";
                    }
//                }
            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageTmallNeedList(String pageNum, String pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        Integer size = tmallNeedMapper.selectTmallCount();
        try{
            if (StringUtils.isBlank(pageNum)){
                pageNum="1";
            }
            if (StringUtils.isBlank(pageSize)){
                pageSize="10";
            }
            if(params.get("price")!=null && !StringUtils.isBlank(params.get("price").toString())){
                if(Integer.parseInt(params.get("price").toString())==1){
                    params.put("a",0);
                    params.put("b",30000);
                }else if(Integer.parseInt(params.get("price").toString())==2){
                    params.put("a",30000);
                    params.put("b",50000);
                }else if(Integer.parseInt(params.get("price").toString())==3){
                    params.put("a",50000);
                    params.put("b",100000);
                }else if(Integer.parseInt(params.get("price").toString())==4){
                    params.put("a",100000);
                    params.put("b",200000);
                }else if(Integer.parseInt(params.get("price").toString())==5){
                    params.put("a",200000);
                    params.put("b",300000);
                }else{
                    params.put("a",300000);
                }
            }
            if(params.get("approve")!=null && !StringUtils.isBlank(params.get("approve").toString())){
                if(Integer.parseInt(params.get("approve").toString())==1) {
                    params.put("c", 1);
                }
            }
            if(params.get("sorts")!=null && !StringUtils.isBlank(params.get("sorts").toString())){
                if(Integer.parseInt(params.get("sorts").toString())==1) {
                    params.put("d", 1);
                }else {
                    params.put("d", 2);
                }
            }
//            params.put("delFlag", "0");
//            PageHelperNew.startPage(pageNum, pageSize);
            TermQueryBuilder termQueryBuilder1=null;
            //new 一个BoolQueryBuilder，类似于StringBuffer，然后判断是否为空，将条件拼接
            BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();
            termQueryBuilder1=termQuery("status", "1");
            boolQueryBuilder.must(termQueryBuilder1);
            if (StringUtils.isNotEmpty((String) params.get("tmallType"))){
                //termQuery是完全匹配，
                termQueryBuilder1=termQuery("tmallType", params.get("tmallType"));
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty((String) params.get("brandType"))){
                //termQuery是完全匹配，
                termQueryBuilder1=termQuery("brandType", params.get("brandType"));
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty((String) params.get("needType"))){
                //termQuery是完全匹配，
                termQueryBuilder1=termQuery("needType", params.get("needType"));
                boolQueryBuilder.must(termQueryBuilder1);
            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==1){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(0).lte(30000));
//            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==2){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(30000).lte(50000));
//            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==3){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(50000).lte(100000));
//            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==4){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(100000).lte(200000));
//            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==5){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(200000).lte(300000));
//            }
//            if (params.get("price")!=null&&Integer.parseInt(params.get("price").toString())==6){
//                //filter是过滤，rangeQuery是过滤的条件
//                boolQueryBuilder.filter(rangeQuery("price").gt(300000));
//            }
            if (StringUtils.isNotEmpty((String) params.get("needSubject"))){
                boolQueryBuilder.must(matchQuery("needSubject",params.get("needSubject")));
            }
            NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder);
            Pageable pageable= PageRequest.of(0, size);
            //得到的最终的结果进行build创建一个query语句
            SearchQuery searchQuery=nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("createTime").order(SortOrder.DESC)).build();
            List<EsTmallNeed> tmallNeeds = elasticsearchTemplate.queryForList(searchQuery, EsTmallNeed.class);
            //如果多条件筛选无结果，则返回客服qq，默认type=1
            if (tmallNeeds==null||tmallNeeds.size()==0){
                return serviceQqService.contactQq("1");
            }
            ListPageUtil page = new ListPageUtil(tmallNeeds.size(),Integer.parseInt(pageNum),Integer.parseInt(pageSize),tmallNeeds);
//            for (EsTmallNeed tmall :
//                    tmallNeeds) {
//                //没有一般违规扣分和严重违规扣分时显示不扣分。0
//                if (tmall.getGeneralViolationPoint()!=null&&tmall.getSeriousViolationPoint()!=null){
//                    tmall.setIsDeduction("0");
//                }else {
//                    tmall.setIsDeduction("1");
//                }
//            }
//            List<EsTmallNeed> modelMapList = tmallNeeds;
//            List<EsTmallNeed> a = new ArrayList<>();
//            for(int i=0;i<modelMapList.size();i++){
//                EsTmallNeed tmallNeed=modelMapList.get(i);
//                if(tmallNeed.getApproveTime()==null){
//                    code="-3";
//                    msg="查询求购未通过审核";
//                }else{
//                    a.add(tmallNeed);
//                }
//            }
//            PageInfo<EsTmallNeed> page = new PageInfo<>(a);

//            PageInfo page1 = new PageInfo(tmallNeeds);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getUserTmallNeed(Integer pageNum, Integer pageSize,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            TmallNeed tmallNeed = new TmallNeed();
            tmallNeed.setMemberId(memberId);
            List<TmallNeed> tmallNeedList = tmallNeedMapper.selectSelection(tmallNeed);
            for (TmallNeed tmallneed :tmallNeedList) {
                if (StringUtils.equals(tmallneed.getStatus(),"0")){
                    tmallneed.setPlayInfo("未审核");
                }else if (StringUtils.equals(tmallneed.getStatus(),"1")){
                    tmallneed.setPlayInfo("求购中");
                }else if (StringUtils.equals(tmallneed.getStatus(),"2")||StringUtils.equals(tmallneed.getStatus(),"3")){
                    tmallneed.setPlayInfo("已结束");
                }
            }
            PageInfo<TmallNeed> page = new PageInfo<>(tmallNeedList);
            result.setData(page);
            code=Constants.SUCCESS;
            msg="查询成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getTmallNeed(String id){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isBlank(id)){
                code="-3";
                msg="求购id不能为空";
            }else {
                TmallNeed tmallNeed = tmallNeedMapper.selectByPrimaryKey(id);
                if (StringUtils.isBlank(tmallNeed.getAddressId())) {
                    code = "-4";
                    msg = "求购地址不能为空";
                }
                ProvincesCitysCountrys provincesCitysCountrys = provincesCitysCountrysMapper.selectByPrimaryKey(tmallNeed.getAddressId());
                tmallNeed.setCitysId(provincesCitysCountrys.getCitysId());
                tmallNeed.setProvincesId(provincesCitysCountrys.getProvincesId());
                result.setData(tmallNeed);
                code = Constants.SUCCESS;
                msg = "查询成功";
            }
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 修改求购信息
     * @param tmallNeed
     * @return
     */
    @Override
    public Result updateTmallNeed(TmallNeed tmallNeed) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            tmallNeedMapper.updateByPrimaryKeySelective(tmallNeed);
            code=Constants.SUCCESS;
            msg="修改成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result deleteTmallNeed(String id,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isEmpty(id)){
                code="-3";
                msg="求购id不能为空";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                TmallNeed tmallNeed = tmallNeedMapper.selectByPrimaryKey(id);
                if (!StringUtils.equals(tmallNeed.getMemberId(), memberId)) {
                    code = "-4";
                    msg = "不是您的求购店铺，无法下架";
                } else if (!StringUtils.equals(tmallNeed.getStatus(), "0")) {
                    code = "-5";
                    msg = "该求购店铺暂时无法下架";
                } else {
                    tmallNeed.setStatus("3");
                    tmallNeedMapper.updateByPrimaryKeySelective(tmallNeed);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
