package com.aidianmao.serviceImpl;

import com.aidianmao.entity.ShopCart;
import com.aidianmao.entity.Tmall;
import com.aidianmao.mapper.ShopCartMapper;
import com.aidianmao.mapper.TmallMapper;
import com.aidianmao.service.ShopCartService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ShopCartServiceImpl implements ShopCartService {

    @Autowired
    private ShopCartMapper dao;

    @Autowired
    private TmallMapper tmallMapper;

    /**
     * 获取购物车
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result getShopCart(String aidianmaoMemberToken,Integer pageNum,Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //通过token得到memberId
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            ArrayList<Tmall> tmalls = new ArrayList<>();
            //根据memberId查询购物车
            List<ShopCart> shopCarts = dao.selectShopCart(memberId);
            for (ShopCart s :
                    shopCarts) {
                Tmall tmall = tmallMapper.selectByPrimaryKey(s.getTmallId());
                tmalls.add(tmall);
            }
            PageInfo page = new PageInfo(tmalls);
            result.setData(page);
            code=Constants.SUCCESS;
            msg="查询成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 添加购物车
     * @param tmallId
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result addShopCart(String tmallId,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            //通过token得到memberId
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (StringUtils.isEmpty(tmallId)){
                return new Result("-3","店铺id不能为空");
            }
            ShopCart shopCart = new ShopCart();
            shopCart.setMemberId(memberId);
            shopCart.setTmallId(tmallId);
            //判断是否已经在购店车存在
            ShopCart cart = dao.selectSelective(shopCart);
            if (cart!=null){
                return new Result("-4","该店铺已经存在");
            }
            shopCart.setId(CommonUtil.getUUID());
            shopCart.setCreateTime(new Date());
            shopCart.setDelFlag("0");
            dao.insert(shopCart);
            code=Constants.SUCCESS;
            msg="提交成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 从购物车删除
     * @param tmallId
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result deleteShopCart(String tmallId, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(tmallId)){
                return new Result("-2","店铺id不能为空");
            }
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            ShopCart shopCart = new ShopCart();
            shopCart.setTmallId(tmallId);
            shopCart.setMemberId(memberId);
            shopCart = dao.selectSelective(shopCart);
            if (shopCart==null){
                return new Result("-3","购物车里该店铺不存在或已被删除");
            }
            shopCart.setDelFlag("1");
            dao.updateByPrimaryKeySelective(shopCart);
            code=Constants.SUCCESS;
            msg="删除成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 直接从购物车删除
     * @param id
     * @return
     */
    @Override
    public Result deletShopCart(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                return new Result("-2","购物车id不能为空");
            }
            ShopCart shopCart = dao.selectByPrimaryKey(id);
            if (shopCart==null){
                return new Result("-3","购物车里该店铺不存在或已被删除");
            }
            shopCart.setDelFlag("1");
            dao.updateByPrimaryKeySelective(shopCart);
            code=Constants.SUCCESS;
            msg="删除成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize,  Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("delFlag", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList = dao.selectAllBySelection(params);
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
