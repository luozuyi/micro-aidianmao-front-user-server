package com.aidianmao.serviceImpl;

import com.aidianmao.entity.BarginPrice;
import com.aidianmao.entity.Tmall;
import com.aidianmao.mapper.BarginPriceMapper;
import com.aidianmao.mapper.TmallMapper;
import com.aidianmao.service.BarginPriceService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.record.chart.BarRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class BarginPriceServiceImpl implements BarginPriceService {
    @Autowired
    private BarginPriceMapper barginPriceMapper;

    @Autowired
    private TmallMapper tmallMapper;

    @Override
    public Result addBarginPrice(BigDecimal price, String phone, String tmallId, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (price == null) {
                code = "-3";
                msg = "期望价格不能为空";
            } else {
                if(!PatternUtil.patternString(phone,"mobile")){
                    code = "-4";
                    msg = "手机号格式不正确";
                }else{
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
                    BigDecimal bigDecimal = new BigDecimal(0.8);
                    BigDecimal tmallShopPrice = tmall.getShopPrice().multiply(bigDecimal);
                    int a = price.compareTo(tmallShopPrice);
                    if (a == -1) {
                        code = "-5";
                        msg = "期望价格不能少于原价的百分之八十";
                    } else {
                        BarginPrice barginPrice = new BarginPrice();
                        barginPrice.setId(CommonUtil.getUUID());
                        barginPrice.setCreateTime(new Date());
                        barginPrice.setDelFlag("0");
                        barginPrice.setMemberId(memberId);
                        barginPrice.setPrice(price);
                        barginPrice.setPhone(phone);
                        barginPrice.setTmallId(tmallId);
                        barginPriceMapper.insert(barginPrice);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addBarginprice(BigDecimal price, String phone, String tmallId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (price == null) {
                code = "-3";
                msg = "期望价格不能为空";
            } else if (StringUtils.isBlank(phone)) {
                code = "-4";
                msg = "手机号不能为空";
            } else {
                if(!PatternUtil.patternString(phone,"mobile")){
                    code = "-6";
                    msg = "手机号格式不正确";
                }else {
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
                    BigDecimal bigDecimal = new BigDecimal(0.8);
                    BigDecimal tmallShopPrice = tmall.getShopPrice().multiply(bigDecimal);
                    int a = price.compareTo(tmallShopPrice);
                    if (a == -1) {
                        code = "-5";
                        msg = "期望价格不能少于原价的百分之八十";
                    } else {
                        BarginPrice barginPrice = new BarginPrice();
                        barginPrice.setId(CommonUtil.getUUID());
                        barginPrice.setCreateTime(new Date());
                        barginPrice.setDelFlag("0");
                        barginPrice.setPrice(price);
                        barginPrice.setPhone(phone);
                        barginPrice.setTmallId(tmallId);
                        barginPriceMapper.insert(barginPrice);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result getList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<BarginPrice> barginPrices = barginPriceMapper.selectAll();
            result.setData(barginPrices);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("delFlag", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList = barginPriceMapper.selectAllBySelection(params);
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
