package com.aidianmao.serviceImpl;

import com.aidianmao.entity.PlatformMessage;
import com.aidianmao.mapper.PlatformMessageMapper;
import com.aidianmao.service.PlatformMessageService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

@Transactional
@Service
public class PlatformMessageServiceImpl implements PlatformMessageService {

    @Autowired
    private PlatformMessageMapper platformMessageMapper;

    /**
     * 查询站内信
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getList(String aidianmaoMemberToken,String status, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PlatformMessage platformMessage = new PlatformMessage();
            platformMessage.setMemberId(memberId);
            if (StringUtils.isNotEmpty(status)){
                platformMessage.setStatus(status);
            }
            PageHelperNew.startPage(pageNum, pageSize);
            List<PlatformMessage> platformMessages = platformMessageMapper.selectByMemberId(platformMessage);
            PageInfo page = new PageInfo(platformMessages);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    /**
     * 根据Id查询站内信,同时如果站内信状态为未读，修改成已读
     * @param id
     * @return
     */
    @Override
    public Result getPlatformMessage(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                return new Result("-3","站内信id不能为空");
            }
            PlatformMessage platformMessage = platformMessageMapper.selectByPrimaryKey(id);
            //如果该站内信为未读，修改状态为已读
            if (StringUtils.equals(platformMessage.getStatus(),"0")){
                platformMessage.setStatus("1");
                platformMessageMapper.updateByPrimaryKeySelective(platformMessage);
            }
            result.setData(platformMessage);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;

    }

    /**
     * 批量修改站内信的内容
     * @param ids
     * @return
     */
    @Override
    public Result updateAll(String[] ids,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (ids==null || ids.length==0){
                return new Result("-2","没有未读的系统通知");
            }
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            for (String id:
                 ids) {
                PlatformMessage platformMessage = platformMessageMapper.selectByPrimaryKey(id);
                //如果该站内信为未读，修改状态为已读
                if (StringUtils.equals(platformMessage.getStatus(),"0") && StringUtils.equals(memberId,platformMessage.getMemberId())){
                    platformMessage.setStatus("1");
                    platformMessageMapper.updateByPrimaryKeySelective(platformMessage);
                }
            }
            code = Constants.SUCCESS;
            msg = "修改成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    /**
     * 删除站内信
     * @param ids
     * @return
     */
    @Override
    public Result delete(String[] ids,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (ids==null || ids.length==0){
                return new Result("-2","没有选中任何系统通知");
            }
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            for (String id:
                    ids) {
                PlatformMessage platformMessage = platformMessageMapper.selectByPrimaryKey(id);
                //如果该用户的站内信，删除
                if (StringUtils.equals(memberId, platformMessage.getMemberId())){
                    platformMessage.setDelFlag("1");
                    platformMessageMapper.updateByPrimaryKeySelective(platformMessage);
                }
            }
            code = Constants.SUCCESS;
            msg = "修改成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
