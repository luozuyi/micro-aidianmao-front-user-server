package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Proposal;
import com.aidianmao.mapper.ProposalMapper;
import com.aidianmao.service.ProposalService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class ProposalServiceImpl implements ProposalService {

    @Autowired
    private ProposalMapper proposalMapper;


    /**
     * 新增建议
     * @param proposal
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result addProposal(Proposal proposal, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(proposal.getProposal())){
                return new Result("-2","建议不能为空");
            }
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            proposal.setId(CommonUtil.getUUID());
            proposal.setCreateTime(new Date());
            proposal.setDelFlag("0");
            proposal.setMemberId(memberId);
            proposal.setStatus("0");
            Integer list=proposalMapper.selectByDay(memberId);
            if(list<5){
                proposalMapper.insertSelective(proposal);
                code=Constants.SUCCESS;
                msg="提议成功";
            }else {
                code = "-3";
                msg = "您当日已无法再进行此操作！";
            }
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }

    /**
     * 获取我的建议
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getProposals(String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Proposal> proposals = proposalMapper.selectByMemberId(memberId);
            PageInfo page = new PageInfo(proposals);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setMsg(msg);
        result.setCode(code);
        return result;
    }
}
