package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.AccountKeepingAgencyService;
import com.aidianmao.service.ShopCartService;
import com.aidianmao.service.TmallOrderService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.*;

@Transactional
@Service
public class TmallOrderServiceImpl implements TmallOrderService {
    @Autowired
    private TmallOrderMapper tmallOrderMapper;
    @Autowired
    private TmallMapper tmallMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private TmallOrderFreezeMapper tmallOrderFreezeMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Autowired
    private PlatformFundDetailMapper platformFundDetailMapper;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;

    @Autowired
    private AccountKeepingAgencyService accountKeepingAgencyService;
    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum,pageSize);
            List<Map<String, Object>> mapList = tmallOrderMapper.selectAll(params);
            PageInfo<Map<String,Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result placingOrder(String tmallId, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String buyMemberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(tmallId)){
                code = "-3";
                msg = "天猫店铺不能为空";
            }else{
                Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
                if(tmall == null){
                    code = "-4";
                    msg = "下单的店铺不存在";
                }else if(!tmall.getStatus().equals(Constants.Status.ADUITPASS.getType())){
                    code = "-5";
                    msg = "只有正在出售的店铺才能下单";
                }else{
                    String saleMemberId = tmall.getMemberId();
                    if(buyMemberId.equals(saleMemberId)){
                        code = "-6";
                        msg = "不能给自己的店铺下单";
                    }else{
                        /*封装参数查询*/
                        TmallOrder params = new TmallOrder();
                        params.setBuyMemberId(buyMemberId);
                        params.setTmallId(tmallId);
                        TmallOrder tmallOrder_db = tmallOrderMapper.selectByBuyMemberIdAndTmallId(params);
                        if(tmallOrder_db != null){
                            code = "-7";
                            msg = "该店铺您已经下过订单";
                        }else{
                            /*店铺价格*/
                            BigDecimal shopPrice = tmall.getShopPrice();
                            /*消费者保障金*/
                            BigDecimal shopDeposit = tmall.getShopDeposit();
                            /*技术年费*/
                            BigDecimal shopTechServiceFee = tmall.getShopTechServiceFee();
                            /*服务费*/
                            BigDecimal serviceFee = shopPrice.multiply(new BigDecimal(0.1));
                            /*是否退还消费者保障金*/
                            String isReturnDeposit = tmall.getIsReturnDeposit();
                            /*技术年费是否需要退还*/
                            String isReturnTechServiceFee = tmall.getIsReturnTechServiceFee();
                            /*订单总价格 = 店铺价格+服务费+需要退还的保障金+需要退还的技术年费*/
                            BigDecimal totalPrice = shopPrice.add(serviceFee);
                            if(isReturnDeposit.equals(Constants.IsReturnDeposit.RETURN.getIsReturn())){
                                totalPrice = totalPrice.add(shopDeposit);
                            }
                            if(isReturnTechServiceFee.equals(Constants.IsReturnTechServiceFee.RETURN.getIsReturn())){
                                totalPrice = totalPrice.add(shopTechServiceFee);
                            }
                            TmallOrder tmallOrder = new TmallOrder();
                            tmallOrder.setId(CommonUtil.getUUID());
                            tmallOrder.setCreateTime(new Date());
                            tmallOrder.setDelFlag("0");
                            tmallOrder.setSaleMemberId(saleMemberId);
                            tmallOrder.setBuyMemberId(buyMemberId);
                            //新增订单时，订单显示待付款
                            tmallOrder.setStatus(Constants.OrderStatus.TYPE0.getType());
                            tmallOrder.setCode(RandomCodeUtil.getRandomCode(6));
                            tmallOrder.setPrice(totalPrice);
                            tmallOrder.setServiceFee(serviceFee);
                            tmallOrder.setTmallId(tmallId);
                            //卖家是否确认，默认否
                            tmallOrder.setIsSaleSure("0");
                            tmallOrderMapper.insertSelective(tmallOrder);
                            result.setData(tmallOrder.getId());
                            code = Constants.SUCCESS;
                            msg = "下单成功";
                        }
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 全款购买
     * @param tmallOrderId         订单id
     * @param aidianmaoMemberToken 凭据
     * @param payPassword          支付密码
     * @return
     */
    @Override
    public Result payOrder(String tmallOrderId, String aidianmaoMemberToken, String payPassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String currentMemberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(currentMemberId);
            if(StringUtils.isBlank(tmallOrderId)){
                code = "-3";
                msg = "天猫订单不能为空";
            }else if(StringUtils.isBlank(payPassword)){
                code = "-4";
                msg = "支付密码不能为空";
            } else if(member==null){
                code = "-14";
                msg = "当前用户不存在";
            }else if(!StringUtils.equals(payPassword,member.getPayPassword())){
                code = "-4";
                msg = "支付密码不正确";
            } else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(tmallOrderId);
                if(tmallOrder == null){
                    code = "-5";
                    msg = "支付的订单不存在";
                }else{
                    String buyMemberId = tmallOrder.getBuyMemberId();
                    String tmallId = tmallOrder.getTmallId();
                    //String saleMemberId = tmallOrder.getSaleMemberId();
                    BigDecimal tmallOrderPrice = tmallOrder.getPrice();
                    if(!currentMemberId.equals(buyMemberId)){
                        code = "-6";
                        msg = "不是您的订单无法支付";
                    }else {
                        Member buyMember = memberMapper.selectByPrimaryKey(buyMemberId);
                        BigDecimal buyMemberMoney = buyMember.getMoney();
                        BigDecimal buyMemberFreezeMoney = buyMember.getFreezeMoney();
                        if(buyMemberMoney.compareTo(tmallOrderPrice) == -1){
                            code = "-7";
                            msg = "可用余额不够，无法支付";
                        }else{
                            /*买家可用余额减少，冻结资金增加*/
                            buyMember.setMoney(buyMemberMoney.subtract(tmallOrderPrice));
                            buyMember.setFreezeMoney(buyMemberFreezeMoney.add(tmallOrderPrice));
                            memberMapper.updateByPrimaryKeySelective(buyMember);
                            /*添加冻结*/
                            TmallOrderFreeze tmallOrderFreeze = new TmallOrderFreeze();
                            tmallOrderFreeze.setId(CommonUtil.getUUID());
                            tmallOrderFreeze.setCreateTime(new Date());
                            tmallOrderFreeze.setDelFlag("0");
                            tmallOrderFreeze.setFreezeMoney(tmallOrderPrice);
                            tmallOrderFreeze.setTmallOrderId(tmallOrderId);
                            tmallOrderFreeze.setStatus(Constants.TmallOrderFreezeStatus.TYPE0.getType());
                            tmallOrderFreeze.setMemberId(currentMemberId);
                            tmallOrderFreeze.setMoneyScene("2");
                            tmallOrderFreezeMapper.insertSelective(tmallOrderFreeze);
                            /*修改订单的状态为已经付款*/
                            tmallOrder.setStatus(Constants.OrderStatus.TYPE2.getType());
                            tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                            /*修改店铺的状态为已经付款*/
                            Tmall tmall_db = tmallMapper.selectByPrimaryKey(tmallId);
                            tmall_db.setStatus(Constants.Status.HASPAY.getType());
                            tmallMapper.updateByPrimaryKeySelective(tmall_db);
                            /*修改其他订单状态*/
                            List<TmallOrder> tmallOrders = tmallOrderMapper.selectByTmallId(tmallId);
                            for (TmallOrder tmallOrderItem:tmallOrders) {
                                //除了此订单外的其余订单都设置已取消
                                if(!tmallOrderItem.getId().equals(tmallOrderId)){
                                    tmallOrderItem.setStatus(Constants.OrderStatus.TYPE4.getType());
                                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrderItem);
                                }
                            }
                            /*添加金额的支出明细*/
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            //全款购买，用户的收支明细金额为订单总金额
                            incomeExpensesDetail.setAmountMoney(tmallOrderPrice);
                            incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE1.getType());
                            //全款购买
                            incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE6.getType());
                            incomeExpensesDetail.setMemberId(buyMemberId);
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                            code = Constants.SUCCESS;
                            msg = "付款成功";
                        }
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 确认收货
     * @param tmallOrderId         天猫订单id
     * @param aidianmaoMemberToken 当前用户凭据
     * @return
     */
    @Override
    public Result takeOrder(String tmallOrderId, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String currentMemberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(tmallOrderId)){
                code = "-3";
                msg = "天猫订单不能为空";
            }else{
                /*
                * 1.查询当前订单是否存在，不存在给提示，存在进入步骤2
                * 2.订单存在需要一下判断
                * 2.1判断订单是否是当前买家用户的订单，只有自己的订单才能付款
                * 2.2判断当前订单状态是否已经付款，付款的才能确认收货
                * 3.通过订单信息查询冻结的订单解冻
                * 4.查询订单卖家信息，给卖家加钱，订单总价减去两倍服务费，修改保存
                * 5.给卖家新增收支明细
                * 6.把买家的冻结金额，减去订单的总价格，修改保存
                * 7.添加买家资金明细
                * 9.修改订单状态为已经完成
                * 10.修改店铺状态为已经已售出
                * 11.添加平台收入明细
                * */
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(tmallOrderId);
                if(tmallOrder == null){
                    code = "-4";
                    msg = "订单不存在";
                }else if(!tmallOrder.getBuyMemberId().equals(currentMemberId)){
                    code = "-5";
                    msg = "不是当前买家用户的订单，不能确认收货";
                }else if (StringUtils.equals(tmallOrder.getIsSaleSure(),"0")){
                    code="-9";
                    msg="只有卖家确认之后买家才能确认收货";
                } else if(!tmallOrder.getStatus().equals(Constants.OrderStatus.TYPE8.getType())){
                    code = "-6";
                    msg = "只有交接中的订单才能确认收货";
                }else{
                    String saleMemberId = tmallOrder.getSaleMemberId();
                    /*买家，和买家的总结金额*/
                    Member buyMember = memberMapper.selectByPrimaryKey(currentMemberId);
                    /*修改店铺状态为已经售出*/
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder.getTmallId());
                    BigDecimal buyMemberFreezeMoney = buyMember.getFreezeMoney();
                    /*卖家,和卖家的钱*/
                    Member saleMember = memberMapper.selectByPrimaryKey(saleMemberId);
                    BigDecimal saleMemberMoney = saleMember.getMoney();
                    /*订单总价格，和订单服务费*/
                    BigDecimal tmallOrderPrice = tmallOrder.getPrice();
                    BigDecimal tmallOrderServiceFee = tmallOrder.getServiceFee();
                    /*买家冻结金额减少，修改保存*/
                    buyMember.setFreezeMoney(buyMemberFreezeMoney.subtract(tmallOrderPrice));
                    memberMapper.updateByPrimaryKeySelective(buyMember);
                    /*卖家的钱加上订单价格减去两倍服务费*/
                    saleMember.setMoney(saleMemberMoney.add(tmallOrderPrice).subtract(tmallOrderServiceFee).subtract(tmallOrderServiceFee));
                    memberMapper.updateByPrimaryKeySelective(saleMember);
                    /*添加卖家收支明细，收入*/
                    IncomeExpensesDetail saleIncomeExpensesDetail = new IncomeExpensesDetail();
                    saleIncomeExpensesDetail.setId(CommonUtil.getUUID());
                    saleIncomeExpensesDetail.setCreateTime(new Date());
                    saleIncomeExpensesDetail.setDelFlag("0");
                    //卖家的收支明细是订单总额减去店铺价格的百分之20
                    saleIncomeExpensesDetail.setAmountMoney(tmallOrderPrice.subtract(tmall.getShopPrice().multiply(new BigDecimal(0.2))));
                    saleIncomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE0.getType());
                    //确认收货，交易成功
                    saleIncomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE11.getType());
                    saleIncomeExpensesDetail.setMemberId(saleMemberId);
                    incomeExpensesDetailMapper.insertSelective(saleIncomeExpensesDetail);

                   /* *//*添加买家收支明细，支出*//*
                    IncomeExpensesDetail buyerIncomeExpensesDetail = new IncomeExpensesDetail();
                    buyerIncomeExpensesDetail.setId(CommonUtil.getUUID());
                    buyerIncomeExpensesDetail.setCreateTime(new Date());
                    buyerIncomeExpensesDetail.setDelFlag("0");
                    buyerIncomeExpensesDetail.setAmountMoney(tmallOrderPrice);
                    buyerIncomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE1.getType());
                    //确认收货，交易成功
                    buyerIncomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE11.getType());
                    buyerIncomeExpensesDetail.setMemberId(saleMemberId);
                    incomeExpensesDetailMapper.insertSelective(buyerIncomeExpensesDetail);*/
                    /*修改订单冻结表状态为解冻*/
                    List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByTmallOrderId(tmallOrder.getId());
                    for (TmallOrderFreeze tmallOrderFreeze:tmallOrderFreezes) {
                        tmallOrderFreeze.setStatus(Constants.TmallOrderFreezeStatus.TYPE1.getType());
                        tmallOrderFreezeMapper.updateByPrimaryKeySelective(tmallOrderFreeze);
                    }
                    /*修改订单状态为交易成功*/
                    tmallOrder.setStatus(Constants.OrderStatus.TYPE3.getType());
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    tmall.setStatus(Constants.Status.HASSALE.getType());
                    tmallMapper.updateByPrimaryKeySelective(tmall);
                    /*添加平台收支明细*/
                    PlatformFundDetail platformFundDetail = new PlatformFundDetail();
                    platformFundDetail.setId(CommonUtil.getUUID());
                    platformFundDetail.setCreateTime(new Date());
                    platformFundDetail.setDelFlag("0");
                    platformFundDetail.setRelationId(tmallOrderId);
                    platformFundDetail.setAmountMoney(tmallOrderServiceFee.add(tmallOrderServiceFee));
                    platformFundDetail.setMoneyScene(Constants.PlatformFundDetailMoneyScene.TYPE0.getType());
                    platformFundDetail.setRelationType(Constants.PlatformFundDetailRelationType.TYPE0.getType());
                    platformFundDetail.setType(Constants.PlatformFundDetailType.TYPE0.getType());
                    platformFundDetailMapper.insertSelective(platformFundDetail);
                    //买家站内信
                    String content="尊敬的爱店猫用户：" +"\r\n"+
                            "　 您已成功购买店铺"+"【"+tmall.getShopName()+"】"+"("+ tmall.getShopUrl()+")。"+" 其中使用账户金额"+tmallOrder.getPrice()+"元。目前账户余额"+buyMember.getMoney()+"元。 请等待工作人员联系您交接店铺";
                    String title="恭喜您！您已成功购买店铺";
                    platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",content,tmallOrder.getBuyMemberId(),title,"0"));
                    //卖家站内信
                    String saleContent="尊敬的爱店猫用户：" +"\r\n"+
                            "　  恭喜您！您的店铺"+"【"+tmall.getShopName()+"】"+"("+ tmall.getShopUrl()+")。"+"已被拍下购买，请等待工作人员联系您交接店铺，进行确认出售。";
                    String saleTitle="恭喜您！您的店铺已被拍下购买";
                    platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",saleContent,tmallOrder.getSaleMemberId(),saleTitle,"0"));

                    code = Constants.SUCCESS;
                    msg = "确认收货成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 付定金
     * @param tmallOrderId
     * @param aidianmaoMemberToken
     * @param payPassword
     * @return
     */
    @Override
    public Result payOrderOneStept(String tmallOrderId, String aidianmaoMemberToken, String payPassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String currentMemberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(currentMemberId);
            if(StringUtils.isBlank(tmallOrderId)){
                code = "-3";
                msg = "天猫订单不能为空";
            }else if(StringUtils.isBlank(payPassword)){
                code = "-4";
                msg = "支付密码不能为空";
            } else if(member==null){
                code = "-14";
                msg = "当前用户不存在";
            }else if(!StringUtils.equals(payPassword,member.getPayPassword())){
                code = "-4";
                msg = "支付密码不正确";
            }else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(tmallOrderId);
                if(tmallOrder == null){
                    code = "-5";
                    msg = "支付的订单不存在";
                }else{
                    String buyMemberId = tmallOrder.getBuyMemberId();
                    String tmallId = tmallOrder.getTmallId();
                    //String saleMemberId = tmallOrder.getSaleMemberId();
                    //BigDecimal tmallOrderPrice = tmallOrder.getPrice();
                    BigDecimal serviceFee = tmallOrder.getServiceFee();
                    BigDecimal tmallOrderEarnest = serviceFee.multiply(new BigDecimal(2));
                    if(!currentMemberId.equals(buyMemberId)){
                        code = "-6";
                        msg = "不是您的订单无法支付";
                    }else {
                        Member buyMember = memberMapper.selectByPrimaryKey(buyMemberId);
                        BigDecimal buyMemberMoney = buyMember.getMoney();
                        BigDecimal buyMemberFreezeMoney = buyMember.getFreezeMoney();
                        if(buyMemberMoney.compareTo(tmallOrderEarnest) == -1){
                            code = "-7";
                            msg = "可用余额不够，无法支付";
                        }else{
                            /*买家可用余额减少，冻结资金增加*/
                            buyMember.setMoney(buyMemberMoney.subtract(tmallOrderEarnest));
                            buyMember.setFreezeMoney(buyMemberFreezeMoney.add(tmallOrderEarnest));
                            memberMapper.updateByPrimaryKeySelective(buyMember);
                            /*添加冻结*/
                            TmallOrderFreeze tmallOrderFreeze = new TmallOrderFreeze();
                            tmallOrderFreeze.setId(CommonUtil.getUUID());
                            tmallOrderFreeze.setCreateTime(new Date());
                            tmallOrderFreeze.setDelFlag("0");
                            tmallOrderFreeze.setFreezeMoney(tmallOrderEarnest);
                            tmallOrderFreeze.setTmallOrderId(tmallOrderId);
                            tmallOrderFreeze.setStatus(Constants.TmallOrderFreezeStatus.TYPE0.getType());
                            tmallOrderFreeze.setMemberId(currentMemberId);
                            tmallOrderFreeze.setMoneyScene("0");
                            tmallOrderFreezeMapper.insertSelective(tmallOrderFreeze);
                            /*修改订单的状态为已经付定金*/
                            tmallOrder.setStatus(Constants.OrderStatus.TYPE1.getType());
                            tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                            /*修改店铺的状态为已经付定金*/
                            Tmall tmall_db = tmallMapper.selectByPrimaryKey(tmallId);
                            tmall_db.setStatus(Constants.Status.PREPAY.getType());
                            tmallMapper.updateByPrimaryKeySelective(tmall_db);
                            /*修改其他订单状态为已取消*/
                            List<TmallOrder> tmallOrders = tmallOrderMapper.selectByTmallId(tmallId);
                            for (TmallOrder tmallOrderItem:tmallOrders) {
                                if(!tmallOrderItem.getId().equals(tmallOrderId)){
                                    tmallOrderItem.setStatus(Constants.OrderStatus.TYPE4.getType());
                                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrderItem);
                                }
                            }
                            /*添加金额的支出明细*/
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            incomeExpensesDetail.setAmountMoney(tmallOrderEarnest);
                            incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE1.getType());
                            //已付定金
                            incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE8.getType());
                            incomeExpensesDetail.setMemberId(buyMemberId);
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                            //新增站内信
                            String content="恭喜您！您的店铺"+"【"+tmall_db.getShopName()+"】"+tmall_db.getShopUrl() +"已被拍下预定，请等待工作人员联系您交接店铺，进行确认出售。";
                            String title="恭喜您！您的店铺已被拍下预定";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",content,tmall_db.getMemberId(),title,"0"));
                            BigDecimal bigDecimal=new BigDecimal("4");
                            String contenttwo="尊敬的爱店猫用户：\n" +
                                    "　 您已成功支付定金"+tmallOrderEarnest+"元，成功预定店铺"+"【"+tmall_db.getShopName()+"】"+tmall_db.getShopUrl()+"，店铺预留期为7天，请确认您预留期内了补齐余款，预留期结束前未支付全款且非卖方原因，此金额将不予退还，还需补齐余款"+tmallOrderEarnest.multiply(bigDecimal)+"元。 其中使用账户金额"+tmallOrderEarnest+"元，优惠劵0元。目前账户余额"+buyMember.getMoney()+"元，优惠券余额0元。 请等待工作人员联系您交接店铺";
                            String titletwo="恭喜您！您已成功预定店铺";
                            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",contenttwo,tmallOrder.getBuyMemberId(),titletwo,"0"));
                            code = Constants.SUCCESS;
                            msg = "提交成功";
                        }
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 补齐余款
     * @param tmallOrderId
     * @param aidianmaoMemberToken
     * @param payPassword
     * @return
     */
    @Override
    public Result payOrderTwoStept(String tmallOrderId, String aidianmaoMemberToken, String payPassword) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String currentMemberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(currentMemberId);
            if(StringUtils.isBlank(tmallOrderId)){
                code = "-3";
                msg = "天猫订单不能为空";
            }else if(StringUtils.isBlank(payPassword)){
                code = "-4";
                msg = "支付密码不能为空";
            } else if(member==null){
                code = "-14";
                msg = "当前用户不存在";
            }else if(!StringUtils.equals(payPassword,member.getPayPassword())){
                code = "-4";
                msg = "支付密码不正确";
            }else {
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(tmallOrderId);
                if(tmallOrder == null){
                    code = "-5";
                    msg = "支付的订单不存在";
                }else if (StringUtils.equals(tmallOrder.getIsSaleSure(),"0")){
                    code="-9";
                    msg="只有卖家确认之后买家才能确认收货";
                }else if(!tmallOrder.getStatus().equals(Constants.OrderStatus.TYPE1.getType())){
                    code = "-6";
                    msg = "只有已定金的订单才能付款剩余的90%";
                }else{
                    String buyMemberId = tmallOrder.getBuyMemberId();
                    String tmallId = tmallOrder.getTmallId();
                    /*修改店铺的状态为已经付款*/
                    Tmall tmall_db = tmallMapper.selectByPrimaryKey(tmallId);
                    //订单总价格
                    BigDecimal price = tmallOrder.getPrice();
                    //余款,订单总额减去店铺价格的20%
                    BigDecimal tmallOrderSpare = price.subtract(tmall_db.getShopPrice().multiply(new BigDecimal(0.2)));
                    if(!currentMemberId.equals(buyMemberId)){
                        code = "-7";
                        msg = "不是您的订单无法支付";
                    }else {
                        Member buyMember = memberMapper.selectByPrimaryKey(buyMemberId);
                        BigDecimal buyMemberMoney = buyMember.getMoney();
                        BigDecimal buyMemberFreezeMoney = buyMember.getFreezeMoney();
                        if(buyMemberMoney.compareTo(tmallOrderSpare) == -1){
                            code = "-8";
                            msg = "可用余额不够，无法支付";
                        }else{
                            /*买家可用余额减少，冻结资金增加*/
                            buyMember.setMoney(buyMemberMoney.subtract(tmallOrderSpare));
                            buyMember.setFreezeMoney(buyMemberFreezeMoney.add(tmallOrderSpare));
                            memberMapper.updateByPrimaryKeySelective(buyMember);
                            /*添加冻结*/
                            TmallOrderFreeze tmallOrderFreeze = new TmallOrderFreeze();
                            tmallOrderFreeze.setId(CommonUtil.getUUID());
                            tmallOrderFreeze.setCreateTime(new Date());
                            tmallOrderFreeze.setDelFlag("0");
                            tmallOrderFreeze.setFreezeMoney(tmallOrderSpare);
                            tmallOrderFreeze.setTmallOrderId(tmallOrderId);
                            tmallOrderFreeze.setStatus(Constants.TmallOrderFreezeStatus.TYPE0.getType());
                            tmallOrderFreeze.setMemberId(currentMemberId);
                            tmallOrderFreeze.setMoneyScene("1");
                            tmallOrderFreezeMapper.insertSelective(tmallOrderFreeze);
                            /*修改订单的状态为已经付余款*/
                            tmallOrder.setStatus(Constants.OrderStatus.TYPE2.getType());
                            tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                            tmall_db.setStatus(Constants.Status.HASPAY.getType());
                            tmallMapper.updateByPrimaryKeySelective(tmall_db);
                            /*添加金额的支出明细*/
                            IncomeExpensesDetail incomeExpensesDetail = new IncomeExpensesDetail();
                            incomeExpensesDetail.setId(CommonUtil.getUUID());
                            incomeExpensesDetail.setCreateTime(new Date());
                            incomeExpensesDetail.setDelFlag("0");
                            incomeExpensesDetail.setAmountMoney(tmallOrderSpare);
                            incomeExpensesDetail.setType(Constants.IncomeExpensesType.TYPE1.getType());
                            //店铺余款
                            incomeExpensesDetail.setMoneyScene(Constants.IncomeExpensesMoneyScene.TYPE7.getType());
                            incomeExpensesDetail.setMemberId(buyMemberId);
                            incomeExpensesDetailMapper.insertSelective(incomeExpensesDetail);
                            code = Constants.SUCCESS;
                            msg = "提交成功";
                        }
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getList(Integer pageNum, Integer pageSize,  TmallOrder order,String aidianmaoMemberToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            order.setBuyMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<TmallOrderLess> tmallOrderLesses = tmallOrderMapper.selectList(order);
            for (TmallOrderLess tmallorderLess :
                    tmallOrderLesses) {
                if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE0.getType())) {
                    tmallorderLess.setPlayInfo("待付款");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE4.getType())) {
                    tmallorderLess.setPlayInfo("已取消");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE1.getType())) {
                    tmallorderLess.setPlayInfo("锁定已预约");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE2.getType()) && StringUtils.equals(tmallorderLess.getIsSaleSure(), "0")) {
                    tmallorderLess.setPlayInfo("锁定待交接");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE8.getType())) {
                    tmallorderLess.setPlayInfo("锁定交接中");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE3.getType())) {
                    tmallorderLess.setPlayInfo("已售出交易完成");
                } else if (StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE6.getType()) || StringUtils.equals(tmallorderLess.getStatus(), Constants.OrderStatus.TYPE7.getType())) {
                    tmallorderLess.setPlayInfo("异常已终止");
                }
            }
            PageInfo<TmallOrderLess> page = new PageInfo<>(tmallOrderLesses);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static String shoptype,trademarkType,industryType;
    @Override
    public Result getDynamic(){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<TmallOrder> tmallOrder=tmallOrderMapper.selectByTime();
            List<String[]> list=new ArrayList<>();
            for(int i=0;i<tmallOrder.size();i++) {
                TmallOrder tmallOrder1 = tmallOrder.get(i);
                Tmall tmall = tmallMapper.selectByPrimaryKey(tmallOrder1.getTmallId());
                Member member = memberMapper.selectByPrimaryKey(tmallOrder1.getBuyMemberId());
                if (Constants.ShopType.FLAGSHIPSTORE.getType().equals(tmall.getShopType())) {
                    shoptype = "旗舰店";
                }
                if (Constants.ShopType.MONOPOLYSTORE.getType().equals(tmall.getShopType())) {
                    shoptype = "专营店";
                }
                if (Constants.ShopType.MONOSALESTORE.getType().equals(tmall.getShopType())) {
                    shoptype = "专卖店";
                }
                if (Constants.TrademarkType.R.getType().equals(tmall.getTrademarkType())) {
                    trademarkType = "R标";
                }
                if (Constants.TrademarkType.TM.getType().equals(tmall.getTrademarkType())) {
                    trademarkType = "TM标";
                }
                if (Constants.IndustryType.TYPE0.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE0.getInfo();
                }
                if (Constants.IndustryType.TYPE1.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE1.getInfo();
                }
                if (Constants.IndustryType.TYPE2.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE2.getInfo();
                }
                if (Constants.IndustryType.TYPE3.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE3.getInfo();
                }
                if (Constants.IndustryType.TYPE4.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE4.getInfo();
                }
                if (Constants.IndustryType.TYPE5.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE5.getInfo();
                }
                if (Constants.IndustryType.TYPE6.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE6.getInfo();
                }
                if (Constants.IndustryType.TYPE7.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE7.getInfo();
                }
                if (Constants.IndustryType.TYPE8.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE8.getInfo();
                }
                if (Constants.IndustryType.TYPE9.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE9.getInfo();
                }
                if (Constants.IndustryType.TYPE10.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE10.getInfo();
                }
                if (Constants.IndustryType.TYPE11.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE11.getInfo();
                }
                if (Constants.IndustryType.TYPE12.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE12.getInfo();
                }
                if (Constants.IndustryType.TYPE13.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE13.getInfo();
                }
                if (Constants.IndustryType.TYPE14.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE14.getInfo();
                }
                if (Constants.IndustryType.TYPE15.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE15.getInfo();
                }
                if (Constants.IndustryType.TYPE16.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE16.getInfo();
                }
                if (Constants.IndustryType.TYPE17.getType().equals(tmall.getIndustryType())) {
                    industryType = Constants.IndustryType.TYPE17.getInfo();
                }
                StringBuffer buf = new StringBuffer();
                buf.append(member.getUserName());
                int size = member.getUserName().length() - 3;
                buf.replace(3, size, "****");
                Long date = (new Date()).getTime() - tmallOrder1.getEndTime().getTime();
                String a = date / (1000 * 60) + ",用户 ," + buf + ", 以," + tmallOrder1.getPrice().setScale(0, BigDecimal.ROUND_UP) + ",购得" + tmall.getShopName() + " (" + shoptype + "，" + trademarkType + "，" + industryType + ")";
                String[] a1 = a.split(",");
                list.add(a1);
                if (i == 5) {
                    break;
                }
            }
            //代理记账动态信息获取
            List<String[]> list1 = accountKeepingAgencyService.getList();
            list.addAll(list1);
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getSaleOrder(String aidianmaoMemberToken,Integer pageNum, Integer pageSize,String isSaleSure) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            TmallOrder tmallOrder = new TmallOrder();
            tmallOrder.setSaleMemberId(memberId);
            tmallOrder.setIsSaleSure(isSaleSure);
            PageHelperNew.startPage(pageNum, pageSize);
            List<TmallOrderLess> tmallOrders = tmallOrderMapper.selectForSale(tmallOrder);
            Iterator<TmallOrderLess> iterator = tmallOrders.iterator();
            while(iterator.hasNext()){
                TmallOrderLess tmallOrder1 = iterator.next();
                if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE1.getType())&& StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")){
                    tmallOrder1.setPlayInfo("锁定已预约");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE2.getType()) && StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")){
                    tmallOrder1.setPlayInfo("锁定交接中");
                }else {
                    iterator.remove();   //注意这个地方
                }
            }
            PageInfo<TmallOrderLess> tmallOrderPageInfo = new PageInfo<>(tmallOrders);
            result.setData(tmallOrderPageInfo);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getAllSaleOrder(String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            TmallOrder tmallOrder = new TmallOrder();
            tmallOrder.setSaleMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<TmallOrderLess> tmallOrders = tmallOrderMapper.selectForSale(tmallOrder);
            for (TmallOrderLess tmallOrder1:
                    tmallOrders) {
                if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE1.getType())&& StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")){
                    tmallOrder1.setPlayInfo("锁定已预约");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE2.getType()) && StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")){
                    tmallOrder1.setPlayInfo("锁定交接中");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE2.getType()) && StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")){
                    tmallOrder1.setPlayInfo("锁定待交接");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE8.getType())){
                    tmallOrder1.setPlayInfo("锁定交接中");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE3.getType())){
                    tmallOrder1.setPlayInfo("已售出交易完成");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE6.getType()) || StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE7.getType())){
                    tmallOrder1.setPlayInfo("异常已终止");
                }
            }
            PageInfo<TmallOrderLess> tmallOrderPageInfo = new PageInfo<>(tmallOrders);
            result.setData(tmallOrderPageInfo);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 查询买家可操作订单
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getBuyOrder(String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            List<TmallOrderLess> tmallOrders = tmallOrderMapper.selectForBuy(memberId);
            Iterator<TmallOrderLess> iterator = tmallOrders.iterator();
            while(iterator.hasNext()){
                TmallOrderLess tmallOrder1 = iterator.next();
                if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE1.getType()) && StringUtils.equals(tmallOrder1.getIsSaleSure(),"1")){
                    tmallOrder1.setPlayInfo("锁定已预约");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE8.getType())&& StringUtils.equals(tmallOrder1.getIsSaleSure(),"1")&&StringUtils.equals(tmallOrder1.getBuyMemberId(),memberId)){
                    tmallOrder1.setPlayInfo("锁定交接中");
                }else {
                    iterator.remove();
                }
            }
            PageInfo<TmallOrderLess> tmallOrderPageInfo = new PageInfo<>(tmallOrders);
            result.setData(tmallOrderPageInfo);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getOrderDetail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                code="-2";
                msg="订单id不能为空";
            }else {
                TmallOrderLess tmallOrderLess = tmallOrderMapper.selectById(id);
                result.setData(tmallOrderLess);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 个人中心首页可操作订单
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getDealOrder(String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            ArrayList<TmallOrderLess> tmallOrderLesses = new ArrayList<>();
            TmallOrder tmallOrder = new TmallOrder();
            tmallOrder.setBuyMemberId(memberId);
            //查询当前用户是买家的订单
            List<TmallOrderLess> tmallOrderLesses1 = tmallOrderMapper.selectListSelective(tmallOrder);
            tmallOrder.setBuyMemberId(null);
            tmallOrder.setSaleMemberId(memberId);
            //查询当前用户是卖家的订单
            List<TmallOrderLess> tmallOrderLesses2 = tmallOrderMapper.selectListSelective(tmallOrder);
            tmallOrderLesses.addAll(tmallOrderLesses1);
            tmallOrderLesses.addAll(tmallOrderLesses2);
            Iterator<TmallOrderLess> iterator = tmallOrderLesses.iterator();
            //筛选出可操作订单
            while(iterator.hasNext()){
                TmallOrderLess tmallOrder1 = iterator.next();
                if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE1.getType())){
                    tmallOrder1.setPlayInfo("锁定已预约");
                    //如果此时订单状态卖家未确认，而且当前用户是买家，则删除此不可操作订单
                    if (StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")&&StringUtils.equals(tmallOrder1.getBuyMemberId(),memberId)){
                        iterator.remove();
                    //如果此时订单状态卖家已确认，当前用户是卖家，则删除次不可操作订单
                    }else if (StringUtils.equals(tmallOrder1.getIsSaleSure(),"1")&&StringUtils.equals(tmallOrder1.getSaleMemberId(),memberId)){
                        iterator.remove();
                    }
                    //如果当前订单状态是已付全款，而且当前用户是卖家，而且未确认
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE2.getType())&&StringUtils.equals(tmallOrder1.getIsSaleSure(),"0")&&StringUtils.equals(tmallOrder1.getSaleMemberId(),memberId)){
                    tmallOrder1.setPlayInfo("锁定待交接");
                }else if (StringUtils.equals(tmallOrder1.getStatus(),Constants.OrderStatus.TYPE8.getType())&& StringUtils.equals(tmallOrder1.getIsSaleSure(),"1")&&StringUtils.equals(tmallOrder1.getBuyMemberId(),memberId)){
                    tmallOrder1.setPlayInfo("锁定交接中");
                }else {
                    iterator.remove();
                }
            }
            PageInfo<TmallOrderLess> tmallOrderPageInfo = new PageInfo<>(tmallOrderLesses);
            result.setData(tmallOrderPageInfo);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result cancelOrder(String id,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                code="-2";
                msg="订单不存在";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                code = Constants.SUCCESS;
                msg = "成功";
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if(tmallOrder==null){
                    code="-4";
                    msg="当前订单不存在";
                }else if (!StringUtils.equals(memberId,tmallOrder.getBuyMemberId())){
                    code="-3";
                    msg="不是您的订单，无法取消";
                }else{
                    tmallOrder.setStatus(Constants.OrderStatus.TYPE4.getType());
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    code=Constants.SUCCESS;
                    msg="取消成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 卖家确认出售
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result sureOrder(String id, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                code="-2";
                msg="订单不存在";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if(tmallOrder==null){
                    code="-4";
                    msg="当前订单不存在";
                }else if (!StringUtils.equals(memberId,tmallOrder.getSaleMemberId())){
                    code="-3";
                    msg="不是您店铺的订单，无法确认";
                }else{
                    tmallOrder.setIsSaleSure("1");
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    code=Constants.SUCCESS;
                    msg="确认成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 卖家第二次确认，修改订单状态至交接中
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result sureToConnect(String id, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                code="-2";
                msg="订单不存在";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                TmallOrder tmallOrder = tmallOrderMapper.selectByPrimaryKey(id);
                if (!StringUtils.equals(memberId,tmallOrder.getSaleMemberId())){
                    code="-3";
                    msg="不是您店铺的订单，无法确认";
                }else{
                    //修改订单状态为交接中
                    tmallOrder.setStatus("8");
                    tmallOrderMapper.updateByPrimaryKeySelective(tmallOrder);
                    code=Constants.SUCCESS;
                    msg="确认成功";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


}
