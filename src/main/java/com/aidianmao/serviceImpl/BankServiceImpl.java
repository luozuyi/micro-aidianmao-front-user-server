package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Bank;
import com.aidianmao.mapper.BankMapper;
import com.aidianmao.service.BankService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class BankServiceImpl implements BankService{
    @Autowired
    private BankMapper bankMapper;
    @Override
    public Result list() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Bank> bankList = bankMapper.selectAll();
            result.setData(bankList);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
