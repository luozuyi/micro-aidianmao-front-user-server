package com.aidianmao.serviceImpl;

import com.aidianmao.service.CaptchaService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.HttpClientUtil;
import com.aidianmao.utils.JsonUtil;
import com.aidianmao.utils.Result;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;


@Transactional
@Service
public class CaptchaServiceImpl implements CaptchaService {

        public static final String APIKEY = "d0b74e51bead4efcf0d71437d3f729d2";

        public static final String VALIDATE_URL = "https://captcha.luosimao.com/api/site_verify";


        @Override
        public Result checkAuth(String resp) {
            Result result = new Result();
            String code = Constants.FAIL;
            String msg = "初始化";
            boolean flag = false;
            try {
                HttpClient httpclient = new HttpClient();
                PostMethod post = new PostMethod(VALIDATE_URL);
                post.setRequestHeader("Accept", "application/json");
                post.getParams().setParameter("http.protocol.content-charset", "utf-8");
                post.addParameter("api_key", APIKEY);
                post.addParameter("response", resp);
                httpclient.executeMethod(post);
                String httpResponse = new String(post.getResponseBody(), "utf-8");
                JSONObject jsonObj = new JSONObject(httpResponse);
                int error_code = jsonObj.getInt("error");
                String res = jsonObj.getString("res");
                if (error_code == 0 && "success".compareTo(res) == 0) {
                    flag = true;
                    code=Constants.SUCCESS;
                    msg="Send message success";
                } else {
                    String error_msg = jsonObj.getString("msg");
                    msg="error_msg";
                    code=error_code+"";
                }
            } catch (Exception e) {
                e.printStackTrace();
                code = Constants.ERROR;
                msg = "系统繁忙";
            }
            result.setData(flag);
            result.setMsg(msg);
            result.setCode(code);
            return result;
        }

}
