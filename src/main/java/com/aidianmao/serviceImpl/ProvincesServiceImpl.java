package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Provinces;
import com.aidianmao.mapper.ProvincesMapper;
import com.aidianmao.service.ProvincesService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

@Transactional
@Service
public class ProvincesServiceImpl extends BaseServiceImpl<ProvincesMapper> implements ProvincesService {
    @Autowired
    private ProvincesMapper provincesMapper;

    @Override
    public Result getProvinces(){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<Provinces> provinces=provincesMapper.selectByAll();
            result.setData(provinces);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
