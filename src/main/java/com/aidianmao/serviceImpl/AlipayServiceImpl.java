package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Alipay;
import com.aidianmao.mapper.AlipayMapper;
import com.aidianmao.service.AlipayService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AlipayServiceImpl implements AlipayService {
    @Autowired
    private AlipayMapper alipayMapper;

    /**
     * 新增充值
     * @param alipay 充值对象
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result addAlipay(Alipay alipay, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (StringUtils.isBlank(alipay.getName())) {
                code = "-3";
                msg = "支付宝账号名字不能为空";
            } else if (StringUtils.isBlank(alipay.getTradeNo())) {
                code = "-4";
                msg = "交易号不能为空";
            } else if (alipay.getPayMoney()==null||alipay.getPayMoney().compareTo(new BigDecimal(0))==-1||alipay.getPayMoney().compareTo(new BigDecimal(0))==0){
                code="-5";
                msg="交易金额不能为空或充值金额不能为零";
            } else {
                alipay.setId(CommonUtil.getUUID());
                alipay.setCreateTime(new Date());
                alipay.setDelFlag("0");
                if (alipay.getPayTime()==null) {
                    alipay.setPayTime(new Date());
                }
                alipay.setStatus("0");
                alipay.setMemberId(memberId);
                alipayMapper.insertSelective(alipay);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 获取所有的充值记录(暂不使用)
     * @return
     */
    @Override
    public Result getList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Alipay> alipayList = alipayMapper.selectAll();
            result.setData(alipayList);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }


    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Alipay alipay,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            alipay.setMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Alipay> modelMapList = alipayMapper.selectBySelection(alipay);
            PageInfo<Alipay> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 根据memberId查询充值记录，多条件筛选
     * @param alipay
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getByMemberId(Alipay alipay,String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            alipay.setMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<Alipay> alipays = alipayMapper.selectByMemberId(alipay);
            PageInfo page = new PageInfo(alipays);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}

