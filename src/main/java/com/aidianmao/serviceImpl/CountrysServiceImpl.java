package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.CountrysMapper;
import com.aidianmao.service.CountrysService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class CountrysServiceImpl extends BaseServiceImpl<CountrysMapper> implements CountrysService {

}
