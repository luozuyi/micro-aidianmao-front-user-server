package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.service.AdminService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper dao;

    @Override
    public Result checkCustomer(String qq) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(qq)){
                return new Result(Constants.FAIL,"客服qq不能为空");
            }
            int i = dao.checkCustomer(qq);
            if (i<=0){
                code=Constants.FAIL;
                msg="客服验证失败";
            }else {
                code=Constants.SUCCESS;
                msg="客服验证成功";
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
