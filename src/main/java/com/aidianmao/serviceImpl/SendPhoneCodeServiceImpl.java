package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Member;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.service.SendPhoneCodeService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.*;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.apache.coyote.http11.Constants.a;

@Transactional
@Service
public class SendPhoneCodeServiceImpl implements SendPhoneCodeService {
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private HttpServletRequest request;

    public static final String APIKEY = "d0b74e51bead4efcf0d71437d3f729d2";

    public static final String VALIDATE_URL = "https://captcha.luosimao.com/api/site_verify";

    @Override
    public Result getRegistTelephoneCode(String telephone,String captcha) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(captcha)){
                code="-3";
                msg="人机验证结果不能为空";
            }else {
                boolean flag = false;
                HttpClient httpclient = new HttpClient();
                PostMethod post = new PostMethod(VALIDATE_URL);
                post.setRequestHeader("Accept", "application/json");
                post.getParams().setParameter("http.protocol.content-charset", "utf-8");
                post.addParameter("api_key", APIKEY);
                post.addParameter("response", captcha);
                httpclient.executeMethod(post);
                String httpResponse = new String(post.getResponseBody(), "utf-8");
                JSONObject jsonObj = new JSONObject(httpResponse);
                int error_code = jsonObj.getInt("error");
                String res = jsonObj.getString("res");
                if (error_code == 0 && "success".compareTo(res) == 0) {
                    flag = true;
                } else {
                    flag = false;
                }
                if (!flag) {
                    code = error_code+"";
                    msg = jsonObj.getString("msg");
                } else if (StringUtils.isBlank(telephone)) {
                    code = "-3";
                    msg = "请输出手机号";
                } else if (!PatternUtil.patternString(telephone, "mobile")) {
                    code = "-4";
                    msg = "手机号格式不正确";
                } else {
                    Member member_db = memberMapper.selectByPhone(telephone);
                    if (member_db != null) {
                        code = "-5";
                        msg = "手机号已被注册";
                    } else {
                        int random = (int) ((Math.random() * 9 + 1) * 100000);
                        SMSSendUtil.send(telephone, String.valueOf(random), "30");
                        request.getSession().setAttribute("aidianmaoRegistTelephoneCode", random);
                        request.getSession().setAttribute("aidianmaoRegistTelephone", telephone);
                        code = Constants.SUCCESS;
                        msg = "短信发送成功";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getModifyPhoneCode(String phone) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {

            int random = (int) ((Math.random() * 9 + 1) * 100000);
            SMSSendUtil.send(phone, String.valueOf(random), "30");
            request.getSession().setAttribute("aidianmaoModifyPhoneCode", random);
            request.getSession().setAttribute("aidianmaoModifyPhone", phone);
            code = Constants.SUCCESS;
            msg = "短信发送成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getRetrievePhoneCode(String phone,String verify) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isBlank(verify)){
                code="-3";
                msg="人机验证结果不能为空";
            }else {
                boolean flag = false;
                HttpClient httpclient = new HttpClient();
                PostMethod post = new PostMethod(VALIDATE_URL);
                post.setRequestHeader("Accept", "application/json");
                post.getParams().setParameter("http.protocol.content-charset", "utf-8");
                post.addParameter("api_key", APIKEY);
                post.addParameter("response", verify);
                httpclient.executeMethod(post);
                String httpResponse = new String(post.getResponseBody(), "utf-8");
                JSONObject jsonObj = new JSONObject(httpResponse);
                int error_code = jsonObj.getInt("error");
                String res = jsonObj.getString("res");
                if (error_code == 0 && "success".compareTo(res) == 0) {
                    flag = true;
                } else {
                    flag = false;
                }
                if (!flag) {
                    code = error_code + "";
                    msg = jsonObj.getString("msg");
                } else if (StringUtils.isBlank(phone)) {
                    code = "-3";
                    msg = "请输出手机号";
                } else if (!PatternUtil.patternString(phone, "mobile")) {
                    code = "-4";
                    msg = "手机号格式不正确";
                } else {
                    Member member_db = memberMapper.selectByPhone(phone);
                    if (member_db == null) {
                        code = "-5";
                        msg = "该手机号对应的用户不存在";
                    } else {
                        int random = (int) ((Math.random() * 9 + 1) * 100000);
                        SMSSendUtil.send(phone, String.valueOf(random), "30");
                        request.getSession().setAttribute("aidianmaoRetrievePhoneCode", random);
                        request.getSession().setAttribute("aidianmaoRetrievePhone", phone);
                        code = Constants.SUCCESS;
                        msg = "短信发送成功";
                    }
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 人机验证发送短信验证码
     * @param phone
     * @param captcha
     * @return
     */
    @Override
    public Result sendPhoneCode(String phone,String captcha) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isBlank(captcha)){
                code="-3";
                msg="人机验证结果不能为空";
            }else {
                boolean flag = false;
                HttpClient httpclient = new HttpClient();
                PostMethod post = new PostMethod(VALIDATE_URL);
                post.setRequestHeader("Accept", "application/json");
                post.getParams().setParameter("http.protocol.content-charset", "utf-8");
                post.addParameter("api_key", APIKEY);
                post.addParameter("response", captcha);
                httpclient.executeMethod(post);
                String httpResponse = new String(post.getResponseBody(), "utf-8");
                JSONObject jsonObj = new JSONObject(httpResponse);
                int error_code = jsonObj.getInt("error");
                String res = jsonObj.getString("res");
                if (error_code == 0 && "success".compareTo(res) == 0) {
                    flag = true;
                } else {
                    flag = false;
                }
                if (!flag) {
                    code = "-7";
                    msg = jsonObj.getString("msg");
                } else if (StringUtils.isBlank(phone)) {
                    code = "-4";
                    msg = "手机号不能为空";
                } else if (!PatternUtil.patternString(phone, "mobile")) {
                    code = "-8";
                    msg = "手机号格式不正确";
                } else {
                    int random = (int) ((Math.random() * 9 + 1) * 100000);
                    SMSSendUtil.send(phone, String.valueOf(random), "30");
                    request.getSession().setAttribute("aidianmaoModifyPhoneCode", random);
                    request.getSession().setAttribute("aidianmaoModifyPhone", phone);
                    code = Constants.SUCCESS;
                    msg = "短信发送成功";
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }





    public Boolean checkAuth(String resp) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("api_key", APIKEY);
        params.put("response", resp);
        String json = JsonUtil.mapToJson(params);
        boolean flag = false;
        try {
            String httpResponse = HttpClientUtil.dataExchange(VALIDATE_URL, json);
            JSONObject jsonObj = new JSONObject(httpResponse);
            int error_code = jsonObj.getInt("error");
            String res = jsonObj.getString("res");
            if (error_code == 0 && "success".compareTo(res) == 0) {
                flag = true;
            } else {
                flag=false;
                String error_msg = jsonObj.getString("msg");
            }
        } catch (Exception e) {
            flag=false;
            e.printStackTrace();
        }
        return flag;
    }

}
