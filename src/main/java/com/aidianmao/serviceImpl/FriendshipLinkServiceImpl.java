package com.aidianmao.serviceImpl;

import com.aidianmao.entity.FriendshipLink;
import com.aidianmao.mapper.FriendshipLinkMapper;
import com.aidianmao.service.FriendshipLinkService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

/**
 *
 * @author W1665
 * @date 2018/12/14
 */
@Transactional
@Service
public class FriendshipLinkServiceImpl implements FriendshipLinkService {

    @Autowired
    private FriendshipLinkMapper friendshipLinkMapper;

    @Override
    public Result getFriendshipLinkList() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<FriendshipLink> friendshipLinks = friendshipLinkMapper.selectByOrder();
            result.setData(friendshipLinks);
            code=Constants.SUCCESS;
            msg="查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
