package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.TmallOrderFreezeMapper;
import com.aidianmao.mapper.WithdrawFreezeDetailMapper;
import com.aidianmao.service.WithdrawFreezeDetailService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class WithdrawFreezeDetailServiceImpl implements WithdrawFreezeDetailService {

    @Autowired
    private WithdrawFreezeDetailMapper withdrawFreezeDetailMapper;

    @Autowired
    private TmallOrderFreezeMapper tmallOrderFreezeMapper;

    /**
     * 获取冻结明细
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result getFreezeDetail(String aidianmaoMemberToken,Integer pageNum,Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            //根据memberId查询天猫订单冻结明细
            List<TmallOrderFreeze> tmallOrderFreezes = tmallOrderFreezeMapper.selectByMemberId(memberId);
            //根据memberId查询提现冻结明细
            List<WithdrawFreezeDetail> withdrawFreezeDetails = withdrawFreezeDetailMapper.selectByMemberId(memberId);
            ArrayList<FreezeDetail> freezeDetails = new ArrayList<>();
            //如果查询到的天猫冻结明细不为空
            if (tmallOrderFreezes!=null&&tmallOrderFreezes.size()>0){
                for (TmallOrderFreeze tmallOrderFreeze:
                        tmallOrderFreezes) {
                    //向集合中添加FreezeDetail实体类
                    freezeDetails.add(new FreezeDetail(tmallOrderFreeze.getCreateTime(),tmallOrderFreeze.getFreezeMoney(),tmallOrderFreeze.getStatus(),"0"));
                }
            }
            //同上
            if (withdrawFreezeDetails!=null&&withdrawFreezeDetails.size()>0){
                for (WithdrawFreezeDetail withdrawFreezeDetail:
                        withdrawFreezeDetails) {
                    freezeDetails.add(new FreezeDetail(withdrawFreezeDetail.getCreateTime(),withdrawFreezeDetail.getFreezeMoney(),withdrawFreezeDetail.getStatus(),"1"));
                }
            }
            PageInfo page = new PageInfo(freezeDetails);
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(page);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
