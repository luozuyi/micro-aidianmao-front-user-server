package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.WithdrawDepositService;
import com.aidianmao.service.WithdrawFreezeDetailService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class WithdrawDepositServiceImpl implements WithdrawDepositService {
    @Autowired
    private WithdrawDepositMapper withdrawDepositMapper;
    @Autowired
    private MemberBankInfoMapper memberBankInfoMapper;
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private IncomeExpensesDetailMapper incomeExpensesDetailMapper;
    @Autowired
    private WithdrawFreezeDetailMapper withdrawFreezeDetailMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Autowired
    private CitysMapper citysMapper;
    @Autowired
    private ProvincesMapper provincesMapper;
    @Autowired
    private BankMapper bankMapper;

    @Override
    public Result add(WithdrawDeposit withdrawDeposit, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (withdrawDeposit.getMoney() == null) {
                code = "-3";
                msg = "提现金额不能为空";
            }else if (withdrawDeposit.getMoney().compareTo(BigDecimal.ZERO)<=0){
                code = "-7";
                msg = "提现金额不能为负或者零";
            }else if(StringUtils.isBlank(withdrawDeposit.getMemberBankInfoId())){
                code = "-4";
                msg = "提现个人账号不能为空";
            }else{
                MemberBankInfo memberBankInfo_db = memberBankInfoMapper.selectByPrimaryKey(withdrawDeposit.getMemberBankInfoId());
                if(memberBankInfo_db == null){
                    code = "-5";
                    msg = "个人银行不存在";
                }else if(!memberBankInfo_db.getMemberId().equals(memberId)){
                    code = "-6";
                    msg = "不是您的个人银行";
                }else{
                    withdrawDeposit.setId(CommonUtil.getUUID());
                    withdrawDeposit.setCreateTime(new Date());
                    withdrawDeposit.setDelFlag("0");
                    withdrawDeposit.setStatus(Constants.WithdrawDepositStatus.TYPE0.getType());
                    withdrawDepositMapper.insertSelective(withdrawDeposit);
                    code = Constants.SUCCESS;
                    msg = "提交成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 前台提现申请
     * @param money
     * @param payPassword
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result frontWithdraw(String money,String payPassword, String aidianmaoMemberToken,String remark) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(memberId);
            if (money== null||Integer.valueOf(money)<10) {
                code = "-3";
                msg = "提现金额不能为空或者小于10";
            }else if (StringUtils.isBlank(member.getPayPassword())){
                code = "-7";
                msg = "请先设置提现密码";
            }else if (!StringUtils.equals(member.getPayPassword(),payPassword)){
                code = "-4";
                msg = "提现密码错误";
            }else if (member.getMoney().compareTo(new BigDecimal(money))<0){
                code="-5";
                msg="提现金额不能大于账户余额";
            } else {
                MemberBankInfo memberBankInfo = memberBankInfoMapper.selectDefault(memberId);
                if (memberBankInfo == null) {
                    code = "-6";
                    msg = "请设置提现账户";
                } else {
                    WithdrawDeposit withdrawDeposit = new WithdrawDeposit();
                    BigDecimal moneys = new BigDecimal(money);
                    //保留两位小数
                    withdrawDeposit.setMoney(moneys.setScale(2, BigDecimal.ROUND_HALF_UP));
                    BigDecimal serviceFee = BigDecimal.valueOf(0);
                    //提现手续费大于50按50收取,小于5按5收取，其余的按照‰5收取
                    if (moneys.compareTo(BigDecimal.valueOf(10000)) >= 0) {
                        serviceFee = BigDecimal.valueOf(50).setScale(2, BigDecimal.ROUND_HALF_UP);
                    } else if (moneys.compareTo(BigDecimal.valueOf(1000)) <= 0) {
                        serviceFee = BigDecimal.valueOf(5).setScale(2, BigDecimal.ROUND_HALF_UP);
                    } else {
                        serviceFee = moneys.multiply(BigDecimal.valueOf(0.005)).setScale(0, BigDecimal.ROUND_UP);
                    }
                    withdrawDeposit.setServiceFee(serviceFee);
                    //真正到账金额
                    BigDecimal realMoney = moneys.subtract(serviceFee).setScale(0, BigDecimal.ROUND_UP);
                    withdrawDeposit.setActualAppropriation(realMoney);
                    withdrawDeposit.setId(CommonUtil.getUUID());
                    withdrawDeposit.setCreateTime(new Date());
                    withdrawDeposit.setDelFlag("0");
                    withdrawDeposit.setRemark(remark);
                    withdrawDeposit.setMemberId(memberId);
                    withdrawDeposit.setStatus(Constants.WithdrawDepositStatus.TYPE0.getType());
                    withdrawDeposit.setMemberBankInfoId(memberBankInfo.getId());
                    //保存到提现记录
                    withdrawDepositMapper.insertSelective(withdrawDeposit);
                    //收支详情表 0:正常 1：删除 2：待删除 /0:收入 1:支出 /10:提现
                    incomeExpensesDetailMapper.insertSelective(new IncomeExpensesDetail(CommonUtil.getUUID(), new Date(), "0", "1", Constants.IncomeExpensesMoneyScene.TYPE10.getType(), realMoney, memberId));
                    //收支详情表 0:正常 1：删除 2：待删除 /0:收入 1:支出 /16:提现手续费
                    incomeExpensesDetailMapper.insertSelective(new IncomeExpensesDetail(CommonUtil.getUUID(), new Date(), "0", "1", Constants.IncomeExpensesMoneyScene.TYPE16.getType(), serviceFee, memberId));
                    //冻结明细  0:正常 1：删除 2：待删除/0: 冻结中 1: 已解冻
                    withdrawFreezeDetailMapper.insertSelective(new WithdrawFreezeDetail(CommonUtil.getUUID(), new Date(), "0", memberId, withdrawDeposit.getId(), moneys, "0"));
                    //修改账户余额和冻结金额
                    updateMemberDetail(memberId, moneys);
                    code = Constants.SUCCESS;
                    msg = "提交成功";
                }
            }
        }catch (Exception e) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                e.printStackTrace();
                code = Constants.ERROR;
                msg = "系统繁忙";
            }
            result.setCode(code);
            result.setMsg(msg);
            return result;
    }


    /**
     * 多条件查询用户对应的提现记录
     * @param withdrawDeposit
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getByMember(WithdrawDeposit withdrawDeposit, String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            withdrawDeposit.setMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<WithdrawDeposit> withdrawDeposits = withdrawDepositMapper.selectByMemberId(withdrawDeposit);
            PageInfo page = new PageInfo(withdrawDeposits);
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(page);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 获取提现账户
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getWithdrawAccount(String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            List<MemberBankInfo> memberBankInfos=memberBankInfoMapper.selectMemberBankInfo(memberId);
            PageInfo page = new PageInfo(memberBankInfos);
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(page);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 新增提现账户信息
     * @param memberBankInfo
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result addWithdrawAccountInfo(MemberBankInfo memberBankInfo, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        String cityName="";
        String provinceName="";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(memberId);
            if (StringUtils.isBlank(member.getRealName())){
                return new Result("-11","请设置实名认证");
            }
            if (!StringUtils.equals(memberBankInfo.getRealName(),member.getRealName())){
                return new Result("-3","开户人姓名和用户真实名字不一致");
            }
            if (StringUtils.isEmpty(memberBankInfo.getBankId())){
                return new Result("-4","提现银行不能为空");
            }
            if (StringUtils.isEmpty(memberBankInfo.getCitysId())){
                return new Result("-8","城市id不能为空");
            }
            Map<String, Object> map = citysMapper.selectByPrimaryKey(memberBankInfo.getCitysId());
            cityName = (String) map.get("name");
            if (StringUtils.isEmpty(memberBankInfo.getProvincesId())){
                return new Result("-9","省id不能为空");
            }else if(!StringUtils.equals(memberBankInfo.getProvincesId(),(String)map.get("provincesId"))){
                return new Result("-10","省id和城市id不匹配");
            }
            provinceName = provincesMapper.selectByKey(memberBankInfo.getProvincesId()).getName();
            if (StringUtils.isEmpty(memberBankInfo.getBankDetail())){
                return new Result("-5","详细支行名称不能为空");
            }
            if (StringUtils.isEmpty(memberBankInfo.getAccount())){
                return new Result("-6","银行账号不能为空");
            }
            if (memberBankInfo.getAccount().length()<16 || memberBankInfo.getAccount().length()>19){
                return new Result("-8","银行卡号长度只能为16到19位");
            }
            if (!StringUtils.equals(memberBankInfo.getAccount(),memberBankInfo.getAccountAgain())){
                return new Result("-7","两次输入银行卡号不一致");
            }
            //新增详细支行记录
            String id=CommonUtil.getUUID();
            provincesCitysCountrysMapper.insertSelective(new ProvincesCitysCountrys(id,memberBankInfo.getCitysId(),memberBankInfo.getProvincesId(),provinceName,cityName,memberBankInfo.getBankDetail()));
            memberBankInfo.setId(CommonUtil.getUUID());
             memberBankInfo.setCreateTime(new Date());
             memberBankInfo.setDelFlag("0");
             memberBankInfo.setMemberId(memberId);
             memberBankInfo.setProvinceCityCountyId(id);
            List<MemberBankInfo> list=memberBankInfoMapper.selectByMemberId(memberId);
            //如果是第一条记录，设置成提现默认账户
            if (list.size()==0||list==null){
                memberBankInfo.setIsDefault("0");
            }else {
                //是否默认0默认1不默认
                memberBankInfo.setIsDefault("1");
            }
             memberBankInfoMapper.insertSelective(memberBankInfo);
            code=Constants.SUCCESS;
            msg="新增成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 修改账户提现信息(不包含设为默认)
     * @param memberBankInfo
     * @return
     */
    @Override
    public Result updateWithdrawAccountInfo(MemberBankInfo memberBankInfo,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(memberBankInfo.getId())){
                return new Result("-2","提现账户id不能为空");
            }
            if (StringUtils.isEmpty(memberBankInfo.getAccount())){
                return new Result("-10","银行卡号不能为空");
            }else if (StringUtils.isNotEmpty(memberBankInfo.getAccountAgain())) {
                if (memberBankInfo.getAccount().length() < 16 || memberBankInfo.getAccount().length() > 19) {
                    return new Result("-5", "银行卡号长度只能为16到19位");
                }
                if (!StringUtils.equals(memberBankInfo.getAccount(), memberBankInfo.getAccountAgain())) {
                    return new Result("-3", "两次输入的账号不一致");
                }
            }
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            MemberBankInfo memberBankInfo1 = memberBankInfoMapper.selectByPrimaryKey(memberBankInfo.getId());
            if (!StringUtils.equals(memberId,memberBankInfo1.getMemberId())){
                return new Result("-4","不是本人的提现账户，不能修改");
            }
            if (StringUtils.isEmpty(memberBankInfo.getCitysId())){
                return new Result("-8","城市id不能为空");
            }
            Map<String, Object> map = citysMapper.selectByPrimaryKey(memberBankInfo.getCitysId());
            if (StringUtils.isEmpty(memberBankInfo.getProvincesId())){
                return new Result("-9","省id不能为空");
            }else if(!StringUtils.equals(memberBankInfo.getProvincesId(),(String)map.get("provincesId"))){
                return new Result("-10","省id和城市id不匹配");
            }
            String provinceName = provincesMapper.selectByKey(memberBankInfo.getProvincesId()).getName();
            //如果修改了详细支行，则先新增详细支行，在进行修改
            if (!StringUtils.isEmpty(memberBankInfo.getBankDetail())){
                String id=CommonUtil.getUUID();
                provincesCitysCountrysMapper.insertSelective(new ProvincesCitysCountrys(id,memberBankInfo.getCitysId(),memberBankInfo.getProvincesId(),provinceName,(String)map.get("name"),memberBankInfo.getBankDetail()));
                memberBankInfo.setProvinceCityCountyId(id);
            }
            memberBankInfoMapper.updateByPrimaryKeySelective(memberBankInfo);
            code=Constants.SUCCESS;
            msg="修改成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 设置默认账户
     * @param aidianmaoMemberToken
     * @param id
     * @return
     */
    @Override
    public Result setDefault(String aidianmaoMemberToken,String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
                code="-2";
                msg="提现账户id不能为空";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                MemberBankInfo memberBankInfo = memberBankInfoMapper.selectByPrimaryKey(id);
                if (!StringUtils.equals(memberId, memberBankInfo.getMemberId())) {
                    code = "-3";
                    msg = "不是您的账户，无法操作";
                } else {
                    memberBankInfo.setIsDefault("0");
                    memberBankInfoMapper.updateByPrimaryKeySelective(memberBankInfo);
                    List<MemberBankInfo> memberBankInfoList = memberBankInfoMapper.selectByMemberId(memberId);
                    for (MemberBankInfo m :
                            memberBankInfoList) {
                        //将其他的提现账户设置为不是默认
                        if (!StringUtils.equals(m.getId(), id)) {
                            m.setIsDefault("1");
                        }
                        memberBankInfoMapper.updateByPrimaryKeySelective(m);
                    }
                }
            }
            code = Constants.SUCCESS;
            msg = "修改成功";
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 获取所有银行信息并分页
     * @return
     */
    @Override
    public Result getBankInfo() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Bank> banks = bankMapper.selectAll();
            result.setData(banks);
            code=Constants.SUCCESS;
            msg="修改成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 申请提现时修改账户余额和冻结金额
     * @param id
     * @param freezeMoney
     */
    public void updateMemberDetail(String id,BigDecimal freezeMoney){
        Member member = memberMapper.selectByPrimaryKey(id);
        //总资金减少
        member.setMoney(member.getMoney().subtract(freezeMoney));
        //冻结金额增加
        member.setFreezeMoney(member.getFreezeMoney().add(freezeMoney));
        memberMapper.updateByPrimaryKeySelective(member);
    }
}
