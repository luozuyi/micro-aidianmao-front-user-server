package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.esEntity.EsShopName;
import com.aidianmao.esEntity.EsTmallShop;
import com.aidianmao.mapper.*;
import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.service.TmallService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Transactional
@Service
public class TmallServiceImpl implements TmallService{
    @Autowired
    private TmallMapper tmallMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;

    @Autowired
    private CustomerServiceQqService serviceQqService;

    @Autowired
    private TmallCollectMapper tmallCollectMapper;

    @Autowired
    private PlatformMessageMapper platformMessageMapper;


    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private CitysMapper citysMapper;


    /**
     * 出售网店
     * @param tmall 天猫店铺
     * @param aidianmaoMemberToken 凭据
     * @param provincesCitysCountrys 省市区对象
     * @return
     */
    @Override
    public Result add(Tmall tmall,String aidianmaoMemberToken, ProvincesCitysCountrys provincesCitysCountrys) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(tmall.getShopName())){
                code = "-3";
                msg = "店铺名称不能为空";
            }else if(tmall.getShopName().length()<6||tmall.getShopName().length()>30){
                code = "-44";
                msg = "店铺名称不能小于6或大于30";
            }else if(StringUtils.isBlank(tmall.getShopProfile())){
                code = "-4";
                msg = "店铺描述不能为空";
            }else if(tmall.getShopProfile().length() < 20 || tmall.getShopProfile().length() > 200){
                code = "-5";
                msg = "店铺描述在20-200字符";
            }else if(StringUtils.isBlank(tmall.getShopOwner())){
                code = "-6";
                msg = "店铺掌柜名不能为空";
            }else if(tmall.getShopOwner().length() > 20){
                code = "-7";
                msg = "店铺掌柜名为1-20字符之间";
            }else if(tmall.getShopPrice() == null){
                code = "-8";
                msg = "请填写网店价格";
            }else if(tmall.getShopDeposit() == null){
                code = "-9";
                msg = "消费者保障金不能为空";
            }else if(!Constants.IsReturnDeposit.RETURN.getIsReturn().equals(tmall.getIsReturnDeposit())
                    && !Constants.IsReturnDeposit.UNRETURN.getIsReturn().equals(tmall.getIsReturnDeposit())){
                code = "-10";
                msg = "请正确选择消费保证金是否需要退还";
            }else if(tmall.getShopTechServiceFee() == null){
                code = "-11";
                msg = "请填写店铺技术年费";
            }else if(!Constants.IsReturnTechServiceFee.RETURN.getIsReturn().equals(tmall.getIsReturnDeposit())
                    && !Constants.IsReturnTechServiceFee.UNRETURN.getIsReturn().equals(tmall.getIsReturnDeposit())){
                code = "-12";
                msg = "请正确选择技术年费是否需要退还";
            }else if(StringUtils.isBlank(tmall.getShopType()) ||
                    !Constants.ShopType.FLAGSHIPSTORE.getType().equals(tmall.getShopType())
                     && !Constants.ShopType.MONOPOLYSTORE.getType().equals(tmall.getShopType())
                     && !Constants.ShopType.MONOSALESTORE.getType().equals(tmall.getShopType())){
                code = "-13";
                msg = "请正确选择商城类型";
            }else if(!Constants.TrademarkType.R.getType().equals(tmall.getTrademarkType())
                    && !Constants.TrademarkType.TM.getType().equals(tmall.getTrademarkType())){
                code = "-14";
                msg = "请正确选择商标类型";
            }else if(!Constants.IsCarryGoods.CARRY.getType().equals(tmall.getIsCarryGoods())
                    && !Constants.IsCarryGoods.UNCARRY.getType().equals(tmall.getIsCarryGoods())){
                code = "-15";
                msg = "请正确选择是否带货";
            }else if(!Constants.TaxPayerType.GENERAL.getType().equals(tmall.getTaxPayerType())
                    && !Constants.TaxPayerType.SMALLSCALE.getType().equals(tmall.getTaxPayerType())){
                code = "-16";
                msg = "请正确选择纳税人性质";
            }else if(!Constants.IsLoan.YES.getType().equals(tmall.getIsLoan())
                    && !Constants.IsLoan.NO.getType().equals(tmall.getIsLoan())){
                code = "-17";
                msg = "请正确选择是否贷款";
            }/*else if(tmall.getLastYearTurnover() == null){
                code = "-18";
                msg = "去年营业额不能为空";
            }*/else if(!Constants.IsCompletedTurnoverIndex.YES.getType().equals(tmall.getIsCompletedTurnoverIndex())
                    && !Constants.IsCompletedTurnoverIndex.NO.getType().equals(tmall.getIsCompletedTurnoverIndex())){
                code = "-20";
                msg = "是否完成营业额指标";
            }else if(tmall.getCompanyRegisterMoney() == null){
                code = "-21";
                msg = "请填写公司注册资金";
            }else if(StringUtils.isBlank(tmall.getIndustryType())){
                code = "-22";
                msg = "请填行业类型";
            }else if(!Constants.IndustryType.TYPE0.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE1.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE2.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE3.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE4.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE5.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE6.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE7.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE8.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE9.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE10.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE11.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE12.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE13.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE14.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE15.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE16.getType().equals(tmall.getIndustryType())
                    &&!Constants.IndustryType.TYPE17.getType().equals(tmall.getIndustryType())
                    ){
                code = "-23";
                msg = "请正确选择行业类型";
            }/*else if(!Constants.CurrentMainCamp.TYPE0.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE1.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE2.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE3.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE4.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE5.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE6.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE7.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE8.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE9.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE10.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE11.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE12.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE13.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE14.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE15.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE16.getType().equals(tmall.getCurrentMainCamp())
                    &&!Constants.CurrentMainCamp.TYPE17.getType().equals(tmall.getCurrentMainCamp())){
                code = "-24";
                msg = "请正确选择当前主营行业";
            }*/else if(StringUtils.isBlank(tmall.getShopUrl())){
                code = "-25";
                msg = "店铺网址不能为空";
            }else if(tmall.getShopEnterTime() == null){
                code = "-26";
                msg = "天猫入驻时间不能为空";
            }else if(StringUtils.isBlank(tmall.getGeneralViolationPoint())){
                code = "-27";
                msg = "一般违规扣分不能为空";
            }else if(StringUtils.isBlank(tmall.getSeriousViolationPoint())){
                code = "-28";
                msg = "严重违规扣分不能为空";
            }else if(StringUtils.isBlank(tmall.getCounterfeitViolationPoint())){
                code = "-29";
                msg = "售假违规扣分不能为空";
            }else if(!Constants.IsTransfer.YES.getType().equals(tmall.getIsTransfer())
                    &&!Constants.IsTransfer.NO.getType().equals(tmall.getIsTransfer())){
                code = "-30";
                msg = "请正确选择是否可以过户";
            }else if(!Constants.IsDisputeOverObligation.YES.getType().equals(tmall.getIsTransfer())
                    &&!Constants.IsDisputeOverObligation.NO.getType().equals(tmall.getIsTransfer())){
                code = "-31";
                msg = "请正确选择是否有债务纠纷";
            }/*else if(StringUtils.isBlank(tmall.getShopQualification())){
                code = "-32";
                msg = "请选择店铺资质";
            }else if(StringUtils.isBlank(tmall.getCredentials())){
                code = "-33";
                msg = "请选择可以提供的证件";
            }*/else if(StringUtils.isBlank(tmall.getContactName())){
                code = "-34";
                msg = "联系人姓名不能为空";
            }else if(StringUtils.isBlank(tmall.getContactPhone())){
                code = "-35";
                msg = "联系人电话不能为空";
            }else if (!PatternUtil.patternString(tmall.getContactPhone(),"mobile")){
                code="-45";
                msg="联系人电话格式不规范";
            }else if(StringUtils.isBlank(tmall.getContactQq())){
                code = "-36";
                msg = "联系人QQ不能为空";
            }/*else if(StringUtils.isBlank(tmall.getContactWechat())){
                code = "-37";
                msg = "联系人微信不能为空";
            }else if(StringUtils.isBlank(tmall.getContactWechat())){
                code = "-38";
                msg = "联系人微信不能为空";
            }*/else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-39";
                msg = "请选择省";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-40";
                msg = "请选择城市";
            }/*else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-41";
                msg = "省名称";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())){
                code = "-42";
                msg = "市名称";
            }*/else if (StringUtils.isNotEmpty(tmall.getUrgentPhone()) && (!PatternUtil.patternString(tmall.getUrgentPhone(),"mobile"))){
                code="-44";
                msg="紧急联系人手机号格式不规范";
            } /*else if (tmallMapper.limitSale(memberId)>=5){
                code="-43";
                msg="您当日已无法再进行此操作";
            }*/else{
                String provincesCitysCountrysId = CommonUtil.getUUID();
                provincesCitysCountrys.setId(provincesCitysCountrysId);
                provincesCitysCountrysMapper.insertSelective(provincesCitysCountrys);
                tmall.setId(CommonUtil.getUUID());
                tmall.setCreateTime(new Date());
                tmall.setDelFlag("0");
                tmall.setStatus(Constants.Status.ADUITING.getType());
                tmall.setTmallSerialNumber(RandomCodeUtil.getRandomCode(6));
                tmall.setVisitPageViewsCount(0);
                tmall.setCompanyAddressId(provincesCitysCountrysId);
                tmall.setMemberId(memberId);
                tmall.setCurrentMainCamp(tmall.getIndustryType());
                tmallMapper.insertSelective(tmall);
                code = Constants.SUCCESS;
                msg = "成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 多条件筛选天猫商城
     * @param tmallLess
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result selectTmallSelective(TmallLess tmallLess, String pageNum, String pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        Integer size = tmallMapper.selectTmallCount();
        try {
            if (StringUtils.isBlank(pageNum)){
                pageNum="1";
            }
            if (StringUtils.isBlank(pageSize)){
                pageSize="10";
            }
            TermQueryBuilder termQueryBuilder1=null;
            //new 一个BoolQueryBuilder，类似于StringBuffer，然后判断是否为空，将条件拼接
            BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();
            termQueryBuilder1=termQuery("status","0");
            boolQueryBuilder.mustNot(termQueryBuilder1);
            termQueryBuilder1=termQuery("status","2");
            boolQueryBuilder.mustNot(termQueryBuilder1);
            termQueryBuilder1=termQuery("status","3");
            boolQueryBuilder.mustNot(termQueryBuilder1);
            if (StringUtils.isNotEmpty(tmallLess.getShopType())){
                //termQuery是完全匹配，
                 termQueryBuilder1=termQuery("shopType", tmallLess.getShopType());
                 boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty(tmallLess.getIndustryType())){
                termQueryBuilder1=termQuery("industryType",tmallLess.getIndustryType());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty(tmallLess.getTrademarkType())){
                termQueryBuilder1 =termQuery("trademarkType",tmallLess.getTrademarkType());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty(tmallLess.getTaxPayerType())){
                termQueryBuilder1=termQuery("taxPayerType",tmallLess.getTaxPayerType());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty(tmallLess.getProvincesId())){
                termQueryBuilder1=termQuery("provincesId",tmallLess.getProvincesId());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            if (StringUtils.isNotEmpty(tmallLess.getStartNum())){
                //filter是过滤，rangeQuery是过滤的条件
                boolQueryBuilder.filter(rangeQuery("shopPrice").gte(tmallLess.getStartNum()));
            }
            if (StringUtils.isNotEmpty(tmallLess.getEndNum())){
                boolQueryBuilder.filter(rangeQuery("shopPrice").lte(tmallLess.getEndNum()));
            }
            if (!StringUtils.isEmpty(tmallLess.getShopName())){
                boolQueryBuilder.must(matchQuery("shopName",tmallLess.getShopName()));
            }
            if (!StringUtils.isEmpty(tmallLess.getStatus())){
                termQueryBuilder1=termQuery("status",tmallLess.getStatus());
                boolQueryBuilder.must(termQueryBuilder1);
            }
            //将拼接的条件直接放入查询
            NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder().withQuery(boolQueryBuilder);
            Pageable pageable=PageRequest.of(0, size);
            //将上面的查询结果进行排序
            if (StringUtils.equals(tmallLess.getShopPriceOrder(),"1")){
                nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("shopPrice").order(SortOrder.ASC));
            }else if (StringUtils.equals(tmallLess.getShopPriceOrder(),"2")){
                nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("shopPrice").order(SortOrder.DESC));
            }else if (StringUtils.equals(tmallLess.getApproveTimeOrder(),"1")){
                nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("approveTime").order(SortOrder.ASC));
            }else if (StringUtils.equals(tmallLess.getApproveTimeOrder(),"2")){
                nativeSearchQueryBuilder.withPageable(pageable).withSort(SortBuilders.fieldSort("approveTime").order(SortOrder.DESC));
            }
            //得到的最终的结果进行build创建一个query语句
            SearchQuery searchQuery=nativeSearchQueryBuilder.withPageable(pageable).build();
            List<EsTmallShop> tmallLesses = elasticsearchTemplate.queryForList(searchQuery, EsTmallShop.class);
            //如果多条件筛选无结果，则返回客服qq，默认type=1
            if (tmallLesses.isEmpty()){
                return serviceQqService.contactQq("1");
            }
            for (EsTmallShop tmall :
                    tmallLesses) {
                //没有一般违规扣分和严重违规扣分时显示不扣分。0
                if (tmall.getGeneralViolationPoint()!=null&&tmall.getSeriousViolationPoint()!=null){
                    tmall.setIsDeduction("0");
                }else {
                    tmall.setIsDeduction("1");
                }
            }
            //自定义分页
            ListPageUtil page = new ListPageUtil(tmallLesses.size(),Integer.parseInt(pageNum),Integer.parseInt(pageSize),tmallLesses);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }




    @Override
    public Result TmallDetail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        if (StringUtils.isEmpty(id)){
            return new Result(Constants.FAIL,"天猫店铺Id不能为空");
        }
        try {
            Tmall tmall = tmallMapper.selectByPrimaryKey(id);
            if (StringUtils.isNotEmpty(tmall.getCompanyAddressId())){
                ProvincesCitysCountrys provincesCitysCountrys = provincesCitysCountrysMapper.selectByPrimaryKey(tmall.getCompanyAddressId());
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                tmall.setShowTime(formatter.format(tmall.getShopEnterTime()));
                tmall.setCitysName(citysMapper.selectById(provincesCitysCountrys.getCitysId()).getName());
                tmall.setCitysId(provincesCitysCountrys.getCitysId());
                tmall.setProvincesId(provincesCitysCountrys.getProvincesId());
                if (StringUtils.equals(tmall.getStatus(),Constants.Status.ADUITPASS.getType())){
                        tmall.setTradeStatusInfo("正在出售");
                    }else if (StringUtils.equals(tmall.getStatus(),Constants.Status.PREPAY.getType())){
                        tmall.setTradeStatusInfo("已预约");
                    }else if (StringUtils.equals(tmall.getStatus(),Constants.Status.HASPAY.getType())){
                        tmall.setTradeStatusInfo("待交接");
                    } else if (StringUtils.equals(tmall.getStatus(),Constants.Status.TRANSFERING.getType())){
                    tmall.setTradeStatusInfo("交接中");
                    } else if (StringUtils.equals(tmall.getStatus(),Constants.Status.HASSALE.getType())){
                        tmall.setTradeStatusInfo("已售出");
                }
            }
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(tmall);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateTmall(Tmall tmall,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (StringUtils.isEmpty(tmall.getId())){
                code="-3";
                msg="天猫店铺id不能为空";
            }else {
                Tmall tmall1 = tmallMapper.selectByPrimaryKey(tmall.getId());
                if (!StringUtils.equals(tmall1.getMemberId(), memberId)) {
                    code = "-4";
                    msg = "不是您的店铺，无法修改";
                } else if (!StringUtils.equals(tmall1.getStatus(), Constants.Status.ADUITING.getType())) {
                    code = "-5";
                    msg = "当前店铺的无法修改";
                } else {
                    tmallMapper.updateByPrimaryKeySelective(tmall);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 根据memberId获取用户出售店铺
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result getUserTmall(String aidianmaoMemberToken,Integer pageNum,Integer pageSize,Tmall tmall) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                tmall.setMemberId(memberId);
                List<Tmall> tmallList = tmallMapper.selectSelective(tmall);
            for (Tmall tmall1 :
                    tmallList) {
                if (StringUtils.equals(tmall1.getStatus(), Constants.Status.ADUITING.getType())) {
                    tmall1.setPlayInfo("审核中");
                } else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.ADUITPASS.getType())) {
                    tmall1.setPlayInfo("正在出售");
                } else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.ADUITNOPASS.getType())) {
                    tmall1.setPlayInfo("审核失败");
                } else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.DOWN.getType())) {
                    tmall1.setPlayInfo("已下架");
                }else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.PREPAY.getType())) {
                    tmall1.setPlayInfo("锁定已预约");
                }else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.HASPAY.getType())) {
                    tmall1.setPlayInfo("锁定待交接");
                }else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.TRANSFERING.getType())) {
                    tmall1.setPlayInfo("锁定交接中");
                }else if (StringUtils.equals(tmall1.getStatus(), Constants.Status.HASSALE.getType())) {
                    tmall1.setPlayInfo("已售出交易完成");
                }
            }
                PageInfo page = new PageInfo(tmallList);
                code=Constants.SUCCESS;
                msg="查询成功";
                result.setData(page);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 查询用户的收藏店铺
     * @param aidianmaoMemberToken
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getMemberCollect(String aidianmaoMemberToken,Integer pageNum,Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            List<TmallLess> tmallList = tmallMapper.selectForCollect(memberId);
            PageInfo page = new PageInfo(tmallList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "查询成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 收藏店铺
     * @param tmallCollect
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result addMemberCollect(TmallCollect tmallCollect, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(tmallCollect.getTmallId())){
                code="-3";
                msg="店铺id不能为空";
            }else if (tmallMapper.selectByPrimaryKey(tmallCollect.getTmallId())==null){
                code="-4";
                msg="您收藏的店铺不存在";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                tmallCollect.setMemberId(memberId);
                TmallCollect tmallCollect1 = tmallCollectMapper.selectByMemberAndTmall(tmallCollect);
                if (tmallCollect1 != null) {
                    code = "-2";
                    msg = "该店铺已被收藏，请勿重复收藏";
                } else {
                    tmallCollect.setId(CommonUtil.getUUID());
                    tmallCollect.setCreateTime(new Date());
                    tmallCollect.setDelFlag("0");
                    //新增店铺收藏
                    tmallCollectMapper.insertSelective(tmallCollect);
                    Tmall tmall = tmallMapper.selectByPrimaryKey(tmallCollect.getTmallId());
                    //天猫店铺收藏数+1
                    if (tmall.getShopCollectCount() == null || tmall.getShopCollectCount() < 0) {
                        tmall.setShopCollectCount(1);
                    } else {
                        tmall.setShopCollectCount(tmall.getShopCollectCount() + 1);
                    }
                    //修改天猫店铺收藏数
                    tmallMapper.updateByPrimaryKeySelective(tmall);
                    code = Constants.SUCCESS;
                    msg = "收藏成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 删除收藏网店
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result deleteMemberCollect(String id, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(id)){
              code="-3";
                msg="收藏的店铺id不能为空";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                TmallCollect tmallCollect = new TmallCollect();
                tmallCollect.setMemberId(memberId);
                tmallCollect.setTmallId(id);
                tmallCollect = tmallCollectMapper.selectByMemberAndTmall(tmallCollect);
                tmallCollect.setDelFlag("1");
                tmallCollectMapper.updateByPrimaryKeySelective(tmallCollect);
                code = Constants.SUCCESS;
                msg = "删除成功";
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 查询用户是否收藏
     * @param id
     * @param aidianmaoMemberToken
     * @return
     */
    @Override
    public Result isCollect(String id, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (StringUtils.isEmpty(id)){
                return new Result("-4","店铺id不能为空");
            }
            int i = tmallCollectMapper.selectIsCollect(new TmallCollect(memberId, id));
            if (i>0){
                code=Constants.SUCCESS;
                msg="您已经收藏过该网店信息";
            }else {
                code="1";
                msg="您可以收藏该店铺";
            }
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 下架店铺
     * @param tmallId
     * @return
     */
    @Override
    public Result deleteTmall(String tmallId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(tmallId)){
                return new Result("-2","店铺Id不能为空");
            }
            Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
            if (!StringUtils.equals(tmall.getStatus(),"0")){
                return new Result("-3","该店铺无法进行下架操作");
            }
            tmall.setStatus("3");
            tmallMapper.updateByPrimaryKeySelective(tmall);
            //卖家站内信
            String saleContent="尊敬的爱店猫用户：" +"\r\n"+
                    "　  您的店铺已下架"+"【"+tmall.getShopName()+"】"+"("+ tmall.getShopUrl()+")。"+"如有疑问，请咨询在线客服或拨打咨询热线4008359100";
            String saleTitle="您的店铺已下架";
            platformMessageMapper.insertSelective(new PlatformMessage(CommonUtil.getUUID(),new Date(),"0",saleContent,tmall.getMemberId(),saleTitle,"0"));
            code = Constants.SUCCESS;
            msg = "下架成功";
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;

    }

    /**
     * 查询审核成功上架最近的4个天猫店铺
     * @return
     */
    @Override
    public Result selectFourTmall() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<TmallLess> tmallLesses = tmallMapper.selectFourTmall();
            result.setData(tmallLesses);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result selectShopName(String shopName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isEmpty(shopName)){
                return new Result("-3","搜索内容不能为空");
            }
            SearchQuery searchQuery = new NativeSearchQueryBuilder()
                    .withQuery(
                            matchQuery("shopName", shopName)
                    )
                    .withPageable(
                            PageRequest.of(0,20))
                    .build();
            List<EsShopName> list = elasticsearchTemplate.queryForList(searchQuery, EsShopName.class);
            ArrayList<String> esShopNames = new ArrayList<>();
            for (EsShopName esShopName:
                 list) {
                if (!esShopNames.contains(esShopName.getShopName())){
                    if (esShopNames.size()>=5){
                        break;
                    }
                    esShopNames.add(esShopName.getShopName());
                }
            }
            result.setData(esShopNames);
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
