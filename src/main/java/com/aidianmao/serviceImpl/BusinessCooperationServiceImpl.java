package com.aidianmao.serviceImpl;

import com.aidianmao.entity.BusinessCooperation;
import com.aidianmao.entity.Citys;
import com.aidianmao.mapper.BusinessCooperationMapper;
import com.aidianmao.mapper.CitysMapper;
import com.aidianmao.service.BusinessCooperationService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PatternUtil;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class BusinessCooperationServiceImpl implements BusinessCooperationService {
    @Autowired
    private BusinessCooperationMapper businessCooperationMapper;
    @Autowired
    private CitysMapper citysMapper;
    @Autowired
    private HttpServletRequest request;

    @Override
    public Result addBusinessCooperation(String citysName,String industry,String companyName,String phone,String ipAddress){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        String ipAddress1 = CommonUtil.getIpAddr(request);
        try{
            if(StringUtils.isBlank(citysName)){
                code="-3";
                msg="城市不能为空";
            }else if (StringUtils.isBlank(industry)){
                code="-4";
                msg="行业不能为空";
            }else if (StringUtils.isBlank(companyName)){
                code="-5";
                msg="公司名不能为空";
            }else if (StringUtils.isBlank(phone)){
                code="-6";
                msg="手机号不能为空";
            }else if (businessCooperationMapper.limitCooperation(ipAddress)>=5){
                code="-7";
                msg="您当日无法再进行此操作";
            }else {
                if(!PatternUtil.patternString(phone,"mobile")){
                    code="-5";
                    msg="手机号格式不正确";
                }else {
                    List<Citys> citys=citysMapper.selectByName(citysName);
                    if(citys==null){
                        code="-6";
                        msg="输入城市不存在";
                    }else if(citys.size()!=1){
                        code="-7";
                        msg="请输入精确城市";
                    }else {
                        BusinessCooperation businessCooperation=new BusinessCooperation();
                        businessCooperation.setId(CommonUtil.getUUID());
                        businessCooperation.setCreateTime(new Date());
                        businessCooperation.setDelFlag("0");
                        businessCooperation.setCitysId(citys.get(0).getId());
                        businessCooperation.setIndustry(industry);
                        businessCooperation.setCompanyName(companyName);
                        businessCooperation.setPhone(phone);
                        businessCooperation.setIpAddress(ipAddress1);
                        businessCooperationMapper.insert(businessCooperation);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
