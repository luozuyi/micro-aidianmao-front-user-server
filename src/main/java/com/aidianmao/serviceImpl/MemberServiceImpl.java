package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Member;
import com.aidianmao.entity.PlatformMessage;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.mapper.PlatformMessageMapper;
import com.aidianmao.service.MemberService;
import com.aidianmao.sms.StringUtil;
import com.aidianmao.utils.*;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Transactional
@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberMapper memberMapper;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private HttpServletResponse response;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private PlatformMessageMapper platformMessageMapper;

    public static final String APIKEY = "d0b74e51bead4efcf0d71437d3f729d2";

    public static final String VALIDATE_URL = "https://captcha.luosimao.com/api/site_verify";

    @Override
    public Result regist(String telephone, String password, String surePassword, String telePhoneCode, String userName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(telephone)) {
                    code = "-3";
                    msg = "手机号不能为空";
                } else if (StringUtils.isBlank(password)) {
                    code = "-4";
                    msg = "密码不能为空";
                } else if (StringUtils.isBlank(surePassword)) {
                    code = "-5";
                    msg = "确认密码不能为空";
                } else if (!PatternUtil.patternString(telephone, "mobile")) {
                    code = "-6";
                    msg = "手机号格式不正确";
                } else if (!password.equals(surePassword)) {
                    code = "-7";
                    msg = "两次密码不一致";
                } else if (StringUtils.isBlank(userName)) {
                    code = "-8";
                    msg = "用户名不能为空";
                } else if (!PatternUtil.patternString(userName, "username")) {
                    code = "-9";
                    msg = "用户名格式不正确";
                } else {
                    HttpSession session = request.getSession();
                    Object sessionRegistPhone = session.getAttribute("aidianmaoRegistTelephone");
                    Object sessionRegistPhoneCode = session.getAttribute("aidianmaoRegistTelephoneCode");
                    if (sessionRegistPhone == null || sessionRegistPhoneCode == null) {
                        code = "-10";
                        msg = "验证码已经过期";
                    } else {
                        if (!telephone.equals(sessionRegistPhone.toString())) {
                            code = "-9";
                            msg = "发送验证码手机和注册手机号不一致";
                        } else if (!telePhoneCode.equals(sessionRegistPhoneCode.toString())) {
                            code = "-10";
                            msg = "验证码错误";
                        } else {
                            Member member_db = memberMapper.selectByPhone(telephone);
                            Member member_db_user_name = memberMapper.selectByUserName(userName);
                            if (member_db != null) {
                                code = "-11";
                                msg = "手机号已被注册";
                            } else if (member_db_user_name != null) {
                                code = "-12";
                                msg = "用户名已被注册";
                            } else {
                                Member member = new Member();
                                member.setId(CommonUtil.getUUID());
                                member.setCreateTime(new Date());
                                member.setDelFlag("0");
                                member.setPassword(password);
                                member.setPhone(telephone);
                                member.setFreezeMoney(new BigDecimal("0"));
                                member.setMoney(new BigDecimal("0"));
                                member.setLoginCount(0);
                                member.setRegisterIp(CommonUtil.getIpAddr(request));
                                member.setUserName(userName);
                                memberMapper.insertSelective(member);
                                code = Constants.SUCCESS;
                                msg = "注册成功";
                                PlatformMessage platformMessage = new PlatformMessage();
                                platformMessage.setId(CommonUtil.getUUID());
                                platformMessage.setCreateTime(new Date());
                                platformMessage.setDelFlag("0");
                                platformMessage.setTitle("你的爱店猫注册通知");
                                platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                        "　 尊敬的 " + userName + "，感谢您对爱店猫的支持。 爱店猫，专注中小企业服务。为了您的账户安全，请完善注册信息。如果想了解转让，请阅读《网店转让用户必读》。有疑问请咨询在线客服，或拨打4008359100。");
                                platformMessage.setStatus("0");
                                platformMessageMapper.insert(platformMessage);
                            }
                        }
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result telephoneCheck(String telephone) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(telephone)){
                code = "-3";
                msg = "手机号不能为空";
            }else if(!PatternUtil.patternString(telephone, "mobile")){
                code = "-4";
                msg = "手机号格式不正确";
            } else {
                Member member_db = memberMapper.selectByPhone(telephone);
                if (member_db != null) {
                    code = "-5";
                    msg = "手机号已被注册";
                } else {
                    code = Constants.SUCCESS;
                    msg = "可以注册";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result userNameCheck(String userName) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(userName)){
                code = "-3";
                msg = "用户名不能为空";
            }else if(!PatternUtil.patternString(userName, "username")){
                code = "-4";
                msg = "用户名格式不正确";
            } else {
                Member member_db = memberMapper.selectByUserName(userName);
                if (member_db != null) {
                    code = "-5";
                    msg = "用户名已被注册";
                } else {
                    code = Constants.SUCCESS;
                    msg = "可以注册";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result moneyDetail(String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            Member member = memberMapper.selectByPrimaryKey(memberId);
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(member);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result modifyPassword(String oldPassword,String newPassword,String newPasswordtwo,String aidianmaoMemberToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if (StringUtils.isBlank(oldPassword)){
                code = "-3";
                msg = "旧密码不能为空";
            }else if (StringUtils.isBlank(newPassword)){
                code = "-4";
                msg = "新密码不能为空";
            }else if (StringUtils.isBlank(newPasswordtwo)){
                code = "-5";
                msg = "再次输入不能为空";
            }else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                String password=memberMapper.selectByPassword(memberId);
                if(!password.equals(oldPassword)){
                    code = "-6";
                    msg = "旧密码错误";
                }else if (!newPassword.equals(newPasswordtwo)){
                    code = "-7";
                    msg = "两次输入不一样";
                }else {
                    Member member=new Member();
                    member.setId(memberId);
                    member.setPassword(newPassword);
                    memberMapper.updateByPassword(member);
                    code = Constants.SUCCESS;
                    msg = "修改成功";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateMember(String newIdCard, String newrealName, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(newIdCard)) {
                code = "-3";
                msg = "身份证号码不能为空";
            } else if (StringUtils.isBlank(newrealName)) {
                code = "-4";
                msg = "真实姓名不能为空";
            } else {
                String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                Member member = memberMapper.selectByPrimaryKey(memberId);
                member.setIdCard(newIdCard);
                member.setRealName(newrealName);
                memberMapper.updateByPrimaryKeySelective(member);
                code = Constants.SUCCESS;
                msg = "修改成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updatePayPassword(String verification, String newPayPassword, String newPayPasswordTwo, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if(StringUtils.isBlank(verification)){
                code = "-3";
                msg = "验证码不能为空";
            }else if (StringUtils.isBlank(newPayPassword)){
                code = "-4";
                msg = "新支付密码不能为空";
            }else if (StringUtils.isBlank(newPayPasswordTwo)){
                code = "-5";
                msg = "确定新密码不能为空";
            }else {
                if(!newPayPassword.equals(newPayPasswordTwo)){
                    code = "-6";
                    msg = "两次密码不一样";
                }else {
                    String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                    Member member = memberMapper.selectByPrimaryKey(memberId);
                    HttpSession session = request.getSession();
                    Object sessionRegistPhone = session.getAttribute("aidianmaoModifyPhone");
                    Object sessionRegistPhoneCode = session.getAttribute("aidianmaoModifyPhoneCode");
                    if(sessionRegistPhone == null ||  sessionRegistPhoneCode == null){
                        code = "-10";
                        msg = "验证码已经过期";
                    }else{
                        if(!verification.equals(sessionRegistPhoneCode.toString())){
                            code = "-10";
                            msg = "验证码错误";
                        }else{
                            member.setPayPassword(newPayPasswordTwo);
                            memberMapper.updateByPrimaryKeySelective(member);
                            code = Constants.SUCCESS;
                            msg = "修改成功";
                            PlatformMessage platformMessage=new PlatformMessage();
                            platformMessage.setId(CommonUtil.getUUID());
                            platformMessage.setCreateTime(new Date());
                            platformMessage.setDelFlag("0");
                            platformMessage.setTitle("支付密码修改提醒");
                            platformMessage.setContent("尊敬的爱店猫用户：\n" +
                                    "　 亲爱的用户您好,您的支付密码发生了变更，如非本人操作请联系在线客服或拨打4008359100 ");
                            platformMessage.setStatus("0");
                            platformMessageMapper.insert(platformMessage);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result retrievePhone(String phone,String verification,String newPassword,String newPasswordTwo){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(StringUtils.isBlank(phone)){
                code = "-3";
                msg = "手机号不能为空不能为空";
            }else if (!PatternUtil.patternString(phone,"mobile")){
                code ="-8";
                msg="手机号格式不正确";
            }else if (memberMapper.selectByPhone(phone)==null){
                code="-10";
                msg="该手机号暂未注册";
            }else if (StringUtils.isBlank(verification)){
                code = "-4";
                msg = "验证码不能为空";
            }else if(StringUtils.isBlank(newPassword)){
                code = "-5";
                msg = "新密码不能为空";
            }else if(StringUtils.isBlank(newPasswordTwo)){
                code = "-6";
                msg = "确定新密码不能为空";
            }else {
                if (!newPassword.equals(newPasswordTwo)) {
                    code = "-7";
                    msg = "两次密码不一致";
                } else {
                    Member member = memberMapper.selectByPhone(phone);
                    HttpSession session = request.getSession();
                    Object sessionRegistPhone = session.getAttribute("aidianmaoRetrievePhone");
                    Object sessionRegistPhoneCode = session.getAttribute("aidianmaoRetrievePhoneCode");
                    if (sessionRegistPhoneCode == null) {
                        code = "-8";
                        msg = "验证码已经过期";
                    } else {
                        if (!phone.equals(sessionRegistPhone.toString())) {
                            code = "-9";
                            msg = "发送验证码手机和注册手机号不一致";
                        } else if (!verification.equals(sessionRegistPhoneCode.toString())) {
                            code = "-9";
                            msg = "验证码错误";
                        } else {
                            member.setPassword(newPasswordTwo);
                            memberMapper.updateByPrimaryKeySelective(member);
                            code = Constants.SUCCESS;
                            msg = "找回成功";
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
