package com.aidianmao.serviceImpl;

import com.aidianmao.entity.*;
import com.aidianmao.mapper.*;
import com.aidianmao.service.ArticleService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.*;
import java.util.stream.IntStream;


@Transactional
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ChannelMapper channelMapper;
    @Autowired
    private ChannelArticleTemplateMapper channelArticleTemplateMapper;
    @Autowired
    private ArticleContentMapper articleContentMapper;
    @Autowired
    private AccessoryMapper accessoryMapper;

    /**
     * 首页通过parentID获取子栏目文章
     *
     * @param parentId
     * @return
     */
    @Override
    public Result recentlyList(String parentId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
//            List<Map<String, Object>> sonColunmList = new ArrayList<>();
            List<Map<String, Object>> recentlyList = new ArrayList<>();
            if (StringUtils.isBlank(parentId)) {
                code = "-1";
                msg = "请选择要查询的父栏目";
            } else {
                List<String> columnList = articleMapper.selectByParentId(parentId);
                if (columnList.isEmpty()) {
                    code = "-2";
                    msg = "没有子栏目";
                } else {

                    for (int i = 0; i < columnList.size(); i++) {
                        if (i == 0) {
                            List<Map<String, Object>> articleList1 = articleMapper.selectByChannelId(columnList.get(i));//先取三篇最新的文章
                            if (articleList1.size() < 3) {
                                code = "-3";
                                msg = "子栏目没有3篇文章";
                            } else {
                                recentlyList.add(articleList1.get(0));
                                recentlyList.add(articleList1.get(1));
                                recentlyList.add(articleList1.get(2));
                            }
                        } else {
                            List<Map<String, Object>> articleList1 = articleMapper.selectByChannelId(columnList.get(i));//剩下的栏目每个栏目取两篇
                            if (articleList1.size() < 2) {
                                code = "-3";
                                msg = "子栏目没有2篇文章";
                            } else {
                                recentlyList.add(articleList1.get(0));
                                recentlyList.add(articleList1.get(1));
                            }
                        }
                    }
//                    for (String columnId : columnList//对查出来的子栏目id遍历
//                    ) {
//                        List<Map<String, Object>> articleList1 = articleMapper.selectByChannelId(columnId);//每查出来一份子栏目文章就取出来
//                        if (articleList1.size() < 2) {
//                            code = "-3";
//                            msg = "子栏目没有2篇文章";
//                        } else {
//                            recentlyList.add(articleList1.get(0));
//                            recentlyList.add(articleList1.get(1));//先添加子栏目最新的两篇文章
//                            sonColunmList.addAll(articleList1);//再将子栏目所有的文章都放到sonColunmList
//                            sonColunmList.remove(articleList1.get(0));
//                            sonColunmList.remove(articleList1.get(1));//再将recentlyList里的两篇文章去掉
//                        }
//                    }
//                    Random random = new Random();
//                    int i = random.nextInt(sonColunmList.size() - 1);
//                    recentlyList.add(sonColunmList.get(i));
                    result.setData(recentlyList);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result listPageBySelection(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("delFlag", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList = articleMapper.selectAllBySelection(params);
            for (ModelMap modelData : modelMapList) {
                String coverId = modelData.get("coverId").toString();
                Accessory cover = accessoryMapper.selectByPrimaryKey(coverId);
                String articleTemplateId = modelData.get("channelArticleTemplateId").toString();
                ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(articleTemplateId);
                modelData.addAttribute("channelArticleTemplate", channelArticleTemplate);
                modelData.addAttribute("cover", cover);
            }
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result delById(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)) {
                code = "-3";
                msg = "请选择要删除的对象";
            } else {
                Article article_db = articleMapper.selectByPrimaryKey(id);
                if (article_db == null) {
                    code = "-4";
                    msg = "删除的对象不存在";
                } else {
                    article_db.setDelFlag("1");
                    articleMapper.updateByPrimaryKeySelective(article_db);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addArticle(Article article, String content) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(article.getChannelId())) {
                code = "-3";
                msg = "栏目不能为空";
            } else if (StringUtils.isBlank(article.getChannelArticleTemplateId())) {
                code = "-4";
                msg = "模板不能为空";
            } else {
                Channel channel = channelMapper.selectByPrimaryKey(article.getChannelId());
                ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(article.getChannelArticleTemplateId());
                if (channel == null) {
                    code = "-5";
                    msg = "选择的栏目不存在";
                } else if (channelArticleTemplate == null) {
                    code = "-6";
                    msg = "选择的模板不存在";
                } else if (StringUtils.isBlank(content)) {
                    code = "-7";
                    msg = "内容不能为空";
                } else if (StringUtils.isBlank(article.getTitle())) {
                    code = "-8";
                    msg = "标题不能为空";
                } else if (StringUtils.isBlank(article.getCoverId())) {
                    code = "-9";
                    msg = "请上传封面";
                } else {
                    article.setId(UUID.randomUUID().toString().replace("-", ""));
                    article.setDelFlag("0");
                    article.setCreateTime(new Date());

                    String articleContentId = UUID.randomUUID().toString().replace("-", "");
                    article.setArticleContentId(articleContentId);

                    ArticleContent articleContent = new ArticleContent();
                    articleContent.setId(articleContentId);
                    articleContent.setContent(content);
                    articleContentMapper.insertSelective(articleContent);

                    articleMapper.insertSelective(article);

                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result updateArticle(Article article, String content) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(article.getChannelId())) {
                code = "-3";
                msg = "栏目不能为空";
            } else if (StringUtils.isBlank(article.getChannelArticleTemplateId())) {
                code = "-4";
                msg = "模板不能为空";
            } else {
                Channel channel = channelMapper.selectByPrimaryKey(article.getChannelId());
                ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(article.getChannelArticleTemplateId());
                Accessory cover = accessoryMapper.selectByPrimaryKey(article.getCoverId());
                if (channel == null) {
                    code = "-5";
                    msg = "选择的栏目不存在";
                } else if (channelArticleTemplate == null) {
                    code = "-6";
                    msg = "选择的模板不存在";
                } else if (StringUtils.isBlank(content)) {
                    code = "-7";
                    msg = "内容不能为空";
                } else if (StringUtils.isBlank(article.getTitle())) {
                    code = "-8";
                    msg = "标题不能为空";
                } else if (StringUtils.isBlank(article.getId())) {
                    code = "-9";
                    msg = "主键不能为空";
                } else if (cover == null) {
                    code = "-11";
                    msg = "请上传封面";
                } else {
                    Article article1_db = articleMapper.selectByPrimaryKey(article.getId());
                    if (article1_db == null) {
                        code = "-10";
                        msg = "修改的文章对象不存在";
                    } else {
                        articleMapper.updateByPrimaryKeySelective(article);
                        String articleContentId = article1_db.getArticleContentId();
                        ArticleContent articleContent = articleContentMapper.selectByPrimaryKey(articleContentId);
                        articleContent.setContent(content);
                        articleContentMapper.updateByPrimaryKeySelective(articleContent);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result toDetailArticle(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Article article = articleMapper.selectByPrimaryKey(id);
            if (null != article) {
                ModelMap modelData = new ModelMap();
                modelData.addAttribute("article", article);

                String channelId = article.getChannelId();
                if (StringUtils.isNotBlank(channelId)) {
                    Channel channel = channelMapper.selectByPrimaryKey(channelId);
                    modelData.addAttribute("channel", channel);
                } else {
                    modelData.addAttribute("channel", null);
                }

                String articleTemplateId = article.getChannelArticleTemplateId();
                if (StringUtils.isNotBlank(articleTemplateId)) {
                    ChannelArticleTemplate channelArticleTemplate = channelArticleTemplateMapper.selectByPrimaryKey(articleTemplateId);
                    modelData.addAttribute("channelArticleTemplate", channelArticleTemplate);
                } else {
                    modelData.addAttribute("channelArticleTemplate", null);
                }

                String coverId = article.getCoverId();
                if (StringUtils.isNotBlank(coverId)) {
                    Accessory cover = accessoryMapper.selectByPrimaryKey(coverId);
                    modelData.addAttribute("cover", cover);
                } else {
                    modelData.addAttribute("cover", null);
                }

                String articleContentId = article.getArticleContentId();
                if (StringUtils.isNotBlank(articleContentId)) {
                    ArticleContent articleContent = articleContentMapper.selectByPrimaryKey(articleContentId);
                    modelData.addAttribute("articleContent", articleContent);
                } else {
                    modelData.addAttribute("articleContent", null);
                }
                //根据栏目id查询所有
                List<Map<String, Object>> maps = articleMapper.selectByChannelId(channelId);
                HashMap<String, Object> strings = new LinkedHashMap<>();
                HashMap<String, Object> strings1 = new LinkedHashMap<>();
                ArrayList<HashMap<String, Object>> list = new ArrayList<>();
                //如果查询的结果大于两条，直接取第一，二条为前一条和后一条
                if (maps.size() >= 3) {
                    Iterator<Map<String, Object>> iterator = maps.iterator();
                    while (iterator.hasNext()) {
                        Map map = iterator.next();
                        if (StringUtils.equals(article.getId(), (String) map.get("id"))) {
                            //找出当前文章的记录并删除
                            iterator.remove();   //注意这个地方
                        }
                    }
                    //产生一个小于List下标的随机数，获取id，title存入map
                    Random random = new Random();
                    int i = random.nextInt(maps.size() - 1);
                    strings.put("id", maps.get(i).get("id"));
                    strings.put("title", maps.get(i).get("title"));
                    //map添加近list
                    list.add(strings);
                    //同时删除下标为i的位置的元素
                    maps.remove(i);
                    //再次取下标为i的元素
                    strings1.put("id", maps.get(i).get("id"));
                    strings1.put("title", maps.get(i).get("title"));
                    list.add(strings1);
                    //如果查询结果只有两条，则两条直接为前一条，后一条
                } else if (maps.size() == 2) {
                    strings.put("id", maps.get(0).get("id"));
                    strings.put("title", maps.get(0).get("title"));
                    list.add(strings);
                    strings1.put("id", maps.get(1).get("id"));
                    strings1.put("title", maps.get(1).get("title"));
                    list.add(strings1);
                    //如果查询无结果，直接显示当前文章为前一条和后一条
                } else {
                    strings.put("id", article.getId());
                    strings.put("title", article.getTitle());
                    list.add(strings);
                    strings1.put("id", article.getId());
                    strings1.put("title", article.getTitle());
                    list.add(strings1);
                }
                modelData.addAttribute("otherArticle", list);
                code = Constants.SUCCESS;
                msg = "成功";
                result.setData(modelData);
            } else {
                msg = "记录不存在";
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result ArticleDetail(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(id)){
                code="-3";
                msg="文章详情不能为空";
            }else {
                List<Map<String, Object>> mapList = articleMapper.selectDetailById(id);
                result.setData(mapList);
                code=Constants.SUCCESS;
                msg="查询成功";
            }
    } catch (Exception e) {
        code = Constants.ERROR;
        msg = "后台繁忙";
        e.printStackTrace();
    }
    result.setCode(code);
    result.setMsg(msg);
    return result;
    }

    @Override
    public Result list(String channelId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<Map<String, Object>> mapList = articleMapper.selectByChannelId(channelId);
            result.setData(mapList);
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result list(Integer pageNum, Integer pageSize, String channelId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            PageHelperNew.startPage(pageNum, pageSize);
            List<Map<String, Object>> mapList = articleMapper.selectByChannelId(channelId);

            PageInfo<Map<String, Object>> page = new PageInfo<>(mapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
