package com.aidianmao.serviceImpl;

import com.aidianmao.entity.CustomerQqRecord;
import com.aidianmao.entity.CustomerServiceQq;
import com.aidianmao.mapper.AdminMapper;
import com.aidianmao.mapper.CustomerQqRecordMapper;
import com.aidianmao.mapper.CustomerServiceQqMapper;
import com.aidianmao.service.CustomerServiceQqService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class CustomerServiceQqServiceImpl implements CustomerServiceQqService {
    @Autowired
    private CustomerServiceQqMapper customerServiceQqMapper;
    @Autowired
    private CustomerQqRecordMapper customerQqRecordMapper;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AdminMapper adminMapper;


    static int visit=0;
    @Override
    public Result contactQqLand(String type,String aidianmaoMemberToken){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(type)){
                code="-3";
                msg="联系的组不能为空";
            }else {
                List<CustomerServiceQq> customerServiceQq=customerServiceQqMapper.selectByType(type);
                if(customerServiceQq.size()==0){
                    code="-4";
                    msg="没有此组";
                }else {
                    if(visit<=customerServiceQq.size()){
                        if(visit==customerServiceQq.size()){
                            visit-=customerServiceQq.size();
                        }
                        CustomerServiceQq customer=customerServiceQq.get(visit);
                        String qq=customer.getQq();
                        CustomerQqRecord customerQqRecord=new CustomerQqRecord();
                        customerQqRecord.setId(CommonUtil.getUUID());
                        customerQqRecord.setCreateTime(new Date());
                        customerQqRecord.setDelFlag("0");
                        String ipAddress = CommonUtil.getIpAddr(request);
                        customerQqRecord.setIp(ipAddress);
                        customerQqRecord.setMemberId(memberId);
                        String adminname=adminMapper.selectByName(customer.getAdminId());
                        customerQqRecord.setName(adminname);
                        customerQqRecordMapper.insert(customerQqRecord);
                        result.setData(qq);
                        code = Constants.SUCCESS;
                        msg = "成功";
                        visit++;
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
        }

    @Override
    public Result contactQq(String type){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            if(StringUtils.isBlank(type)){
                code="-3";
                msg="联系的组不能为空";
            }else {
                List<CustomerServiceQq> customerServiceQq = customerServiceQqMapper.selectByType(type);
                if (customerServiceQq.size()==0) {
                    code = "-4";
                    msg = "没有此组";
                } else {
                    if (visit <= customerServiceQq.size()) {
                        if (visit == customerServiceQq.size()) {
                            visit -= customerServiceQq.size();
                        }
                        CustomerServiceQq customer = customerServiceQq.get(visit);
                        String qq = customer.getQq();
                        CustomerQqRecord customerQqRecord = new CustomerQqRecord();
                        customerQqRecord.setId(CommonUtil.getUUID());
                        customerQqRecord.setCreateTime(new Date());
                        customerQqRecord.setDelFlag("0");
                        String ipAddress = CommonUtil.getIpAddr(request);
                        customerQqRecord.setIp(ipAddress);
                        customerQqRecordMapper.insert(customerQqRecord);
                        result.setData(qq);
                        code = Constants.SUCCESS;
                        msg = "成功";
                        visit++;
                    }
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
