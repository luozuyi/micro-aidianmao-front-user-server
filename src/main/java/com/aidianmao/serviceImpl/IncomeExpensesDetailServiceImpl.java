package com.aidianmao.serviceImpl;

import com.aidianmao.entity.FreezeDetail;
import com.aidianmao.entity.IncomeExpensesDetail;
import com.aidianmao.entity.TmallOrderFreeze;
import com.aidianmao.entity.WithdrawFreezeDetail;
import com.aidianmao.mapper.IncomeExpensesDetailMapper;
import com.aidianmao.service.IncomeExpensesDetailService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class IncomeExpensesDetailServiceImpl implements IncomeExpensesDetailService {

    @Autowired
    private IncomeExpensesDetailMapper dao;

    @Override
    public Result getAllBySelective(IncomeExpensesDetail record,String aidianmaoMemberToken, Integer pageNum, Integer pageSize) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            record.setMemberId(memberId);
            PageHelperNew.startPage(pageNum, pageSize);
            List<IncomeExpensesDetail> incomeExpensesDetails = dao.selectSelective(record);
            PageInfo page = new PageInfo(incomeExpensesDetails);
            code=Constants.SUCCESS;
            msg="查询成功";
            result.setData(page);
        }catch (Exception e){
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
