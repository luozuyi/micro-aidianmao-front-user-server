package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AssessTmall;
import com.aidianmao.mapper.AssessTmallMapper;
import com.aidianmao.mapper.MemberMapper;
import com.aidianmao.service.AssessTmallService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Transactional
@Service
public class AssessTmallServiceImpl implements AssessTmallService {

    @Autowired
    private AssessTmallMapper assessTmallMapper;
    @Autowired
    private MemberMapper memberMapper;

    @Override
    public Result addAssessTmall(AssessTmall assessTmall,String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            if (StringUtils.isBlank(assessTmall.getShopUrl())) {
                code = "-3";
                msg = "估价url不能为空";
            } else if (assessTmall.getYearTurnover() == null ||assessTmall.getYearTurnover().compareTo(BigDecimal.ZERO)<0) {
                code = "-4";
                msg = "年营业额不能为空或者负数";
            }  else if (assessTmall.getMonthTurnover() == null||assessTmall.getMonthTurnover().compareTo(BigDecimal.ZERO)<0) {
                code = "-5";
                msg = "月营业额不能为空或者负数";
            } else if (assessTmall.getPerTicketSales() == null ||assessTmall.getPerTicketSales().compareTo(BigDecimal.ZERO)<0) {
                code = "-6";
                msg = "客单价不能为空或者负数";
            } else if (!Constants.TaxPayerType.GENERAL.getInfo().equals(assessTmall.getTaxPayerType())
                    && !Constants.TaxPayerType.SMALLSCALE.getInfo().equals(assessTmall.getTaxPayerType())) {
                code = "-7";
                msg = "纳税人性质不能为空或者不匹配";
            } else if (StringUtils.isBlank(assessTmall.getPhone())) {
                code = "-8";
                msg = "电话不能为空";
            } else {
                if(!PatternUtil.patternString(assessTmall.getPhone(),"mobile")){
                    code = "-11";
                    msg = "手机号格式错误";
                }else{
                    String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
                    assessTmall.setMemberId(memberId);
                    assessTmall.setId(CommonUtil.getUUID());
                    if (Constants.TaxPayerType.GENERAL.getInfo().equals(assessTmall.getTaxPayerType())){
                        assessTmall.setTaxPayerType("0");
                    }else{
                        assessTmall.setTaxPayerType("1");
                    }
                    assessTmall.setCreateTime(new Date());
                    assessTmall.setDelFlag("0");
                    assessTmallMapper.insert(assessTmall);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }

            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }



    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("delFlag", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList = assessTmallMapper.selectAllBySelection(params);
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getCount() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            Integer count = assessTmallMapper.selectCount();
            result.setData(count);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
