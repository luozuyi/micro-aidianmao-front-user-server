package com.aidianmao.serviceImpl;

import com.aidianmao.entity.AccountKeepingAgency;
import com.aidianmao.entity.Citys;
import com.aidianmao.mapper.AccountKeepingAgencyMapper;
import com.aidianmao.mapper.CitysMapper;
import com.aidianmao.service.AccountKeepingAgencyService;
import com.aidianmao.utils.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Transactional
@Service
public class AccountKeepingAgencyServiceImpl  implements AccountKeepingAgencyService {
    @Autowired
    private AccountKeepingAgencyMapper accountKeepingAgencyMapper;
    @Autowired
    private CitysMapper citysMapper;
    @Autowired
    private HttpServletRequest request;

    @Override
    public Result addAccountKeepingAgencyCity() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String city = "武汉/深圳/上海/广州/北京/哈尔滨/长春/沈阳/大连/青岛/济南/石家庄/郑州/长沙/南昌/合肥/南京/苏州/杭州/宁波/厦门/福州/珠海/南宁/昆明/贵阳/重庆/成都/西安/其他";
            String[] a1 = city.split("/");
            result.setData(a1);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addAccountKeepingAgency(AccountKeepingAgency accountKeepingAgency) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        String ipAddress = CommonUtil.getIpAddr(request);
        try {
            if (StringUtils.isBlank(accountKeepingAgency.getPhone())) {
                code = "-3";
                msg = "手机号不能为空";
            } else if (StringUtils.isBlank(accountKeepingAgency.getCitysName())) {
                code = "-4";
                msg = "城市不能为空";
            } else if (accountKeepingAgencyMapper.limitAccountKeepingAgency(ipAddress) >= 5) {
                code = "-5";
                msg = "您当日已无法再进行此操作";
            } else {
                if (!PatternUtil.patternString(accountKeepingAgency.getPhone(), "mobile")) {
                    code = "-5";
                    msg = "手机号格式不正确";
                } else {
                    List<Citys> citys = citysMapper.selectByName(accountKeepingAgency.getCitysName());
                    if (citys == null) {
                        code = "-6";
                        msg = "输入城市不存在";
                    } else if (citys.size() != 1) {
                        code = "-7";
                        msg = "请输入精确城市";
                    } else {
                        accountKeepingAgency.setId(CommonUtil.getUUID());
                        accountKeepingAgency.setCreateTime(new Date());
                        accountKeepingAgency.setDelFlag("0");
                        accountKeepingAgency.setIpAddress(ipAddress);
                        accountKeepingAgency.setIsTakeOver("1");
                        accountKeepingAgencyMapper.insertSelective(accountKeepingAgency);
                        code = Constants.SUCCESS;
                        msg = "成功";
                    }
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, Map<String, Object> params) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            params.put("delFlag", "0");
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList = accountKeepingAgencyMapper.selectAllBySelection(params);
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getDynamic() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            List<String[]> list = getList();
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "后台繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public List<String[]> getList() {
        List<String[]> list = new ArrayList<>();
        List<AccountKeepingAgency> accountKeepingAgencyList = accountKeepingAgencyMapper.selectByTime();
        for (int i = 0; i < accountKeepingAgencyList.size(); i++) {
            AccountKeepingAgency accountKeepingAgency = accountKeepingAgencyList.get(i);
            StringBuffer buf = new StringBuffer();
            buf.append(accountKeepingAgency.getPhone());
            int size = accountKeepingAgency.getPhone().length() - 3;
            buf.replace(3, size, "****");
            Long date = (new Date()).getTime() - accountKeepingAgency.getCreateTime().getTime();
            String a = date / (1000 * 60) + ",用户 ," + buf + ", 申请了 ," + "“" + accountKeepingAgency.getCitysName() + "”" + ", 地区代理记账服务";
            String[] a1 = a.split(",");
            list.add(a1);
            if (i == 5) {
                break;
            }
        }
        return list;
    }

}
