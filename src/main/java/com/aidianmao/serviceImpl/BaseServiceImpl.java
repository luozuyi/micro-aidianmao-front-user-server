package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.BaseMapper;
import com.aidianmao.service.BaseService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.RandomCodeUtil;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * 公共查询接口实现类
 * 
 * @author Catch22
 * @date 2018年6月6日
 */
public abstract class BaseServiceImpl<M extends BaseMapper> implements BaseService {

	@Autowired
	private M mapper;

	@Override
	public Result listPage(Integer pageNum, Integer pageSize, Map<String, Object> params) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			String time = null;
			// 处理时间搜索,将截止日期往后推一天
			if (params.containsKey("endTime")) {
				time = params.get("endTime").toString();
				if (StringUtils.isNotBlank(time)) {
					String endTime = RandomCodeUtil.dateToStr(RandomCodeUtil.dateAddOne(RandomCodeUtil.convert(time)));
					params.put("endTime", endTime);
				}
			} else if (params.containsKey("orderEndTime")) {
				time = params.get("orderEndTime").toString();
				if (StringUtils.isNotBlank(time)) {
					String orderEndTime = RandomCodeUtil.dateToStr(RandomCodeUtil.dateAddOne(RandomCodeUtil.convert(time)));
					params.put("orderEndTime", orderEndTime);
				}
			}else if (params.containsKey("endSuccessTime")) {
				time = params.get("endSuccessTime").toString();
				if (StringUtils.isNotBlank(time)) {
					String endSuccessTime = RandomCodeUtil.dateToStr(RandomCodeUtil.dateAddOne(RandomCodeUtil.convert(time)));
					params.put("endSuccessTime", endSuccessTime);
				}
			}
			if(params.containsKey("invoiceCode")) {
				String invoiceCode = (String) params.get("invoiceCode");
				if(invoiceCode != null) {
					params.put("invoiceCode",invoiceCode.trim());
				}
			}
			if (params.containsKey("delFlag")) {
				PageHelperNew.startPage(pageNum, pageSize);
			}
			List<Map<String, Object>> maps = mapper.selectAllBySelection(params);
			if (params.containsKey("delFlag")) {
				PageInfo<Map<String, Object>> page = new PageInfo<>(maps);
				result.setData(page);
			} else {
				result.setData(maps);
			}
			code = Constants.SUCCESS;
			msg = "成功";
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

	@Override
	public Result selectByPrimaryKey(String id) {
		Result result = new Result();
		String code = Constants.FAIL;
		String msg = "初始化";
		try {
			if(StringUtils.isNotBlank(id)) {
				Map<String, Object> maps = mapper.selectByPrimaryKey(id);
				if (maps != null) {
					result.setData(maps);
					code = Constants.SUCCESS;
					msg = "成功";
				}
			}else {
				code = "-5";
				msg = "id不能为空";
			}
		} catch (Exception e) {
			code = Constants.ERROR;
			msg = "系统繁忙";
			e.printStackTrace();
		}
		result.setCode(code);
		result.setMsg(msg);
		return result;
	}

}
