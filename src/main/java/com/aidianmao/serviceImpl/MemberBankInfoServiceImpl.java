package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Bank;
import com.aidianmao.entity.MemberBankInfo;
import com.aidianmao.entity.ProvincesCitysCountrys;
import com.aidianmao.mapper.BankMapper;
import com.aidianmao.mapper.MemberBankInfoMapper;
import com.aidianmao.mapper.ProvincesCitysCountrysMapper;
import com.aidianmao.service.MemberBankInfoService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class MemberBankInfoServiceImpl implements MemberBankInfoService{
    @Autowired
    private MemberBankInfoMapper memberBankInfoMapper;
    @Autowired
    private BankMapper bankMapper;
    @Autowired
    private ProvincesCitysCountrysMapper provincesCitysCountrysMapper;
    @Override
    public Result add(MemberBankInfo memberBankInfo, ProvincesCitysCountrys provincesCitysCountrys, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if(StringUtils.isBlank(memberBankInfo.getBankId())){
                code = "-3";
                msg = "银行不能为空";
            }else if(StringUtils.isBlank(memberBankInfo.getAccount())){
                code = "-4";
                msg = "账户不能为空";
            }else if(StringUtils.isBlank(memberBankInfo.getIsDefault())){
                code = "-5";
                msg = "请选择是否默认";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesId())){
                code = "-6";
                msg = "省不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getProvincesName())){
                code = "-7";
                msg = "省名称不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysId())){
                code = "-8";
                msg = "城市不能为空";
            }else if(StringUtils.isBlank(provincesCitysCountrys.getCitysName())){
                code = "-9";
                msg = "城市名称不能为空";
            }else{
                Bank bank_db = bankMapper.selectByPrimaryKey(memberBankInfo.getBankId());
                List<MemberBankInfo> memberBankInfos_db = memberBankInfoMapper.selectByAccount(memberBankInfo.getAccount());
                if(bank_db == null){
                    code = "-10";
                    msg = "银行不存在";
                }else if(!memberBankInfos_db.isEmpty()){
                    code = "-11";
                    msg = "银行账户已经存在";
                }else{
                    /*保存地址信息*/
                    String provincesCitysCountrysId = CommonUtil.getUUID();
                    provincesCitysCountrys.setId(provincesCitysCountrysId);
                    provincesCitysCountrysMapper.insertSelective(provincesCitysCountrys);
                    /*如果选择默认则查询其他银行 把默认状态的个人 修改为不是默认状态*/
                    if(memberBankInfo.getIsDefault().equals(Constants.MemberBankInfoIsDefault.TYPE0.getType())){
                        List<MemberBankInfo> memberBankInfoList = memberBankInfoMapper.selectByMemberId(memberId);
                        for (MemberBankInfo memberBankInfo_item:memberBankInfoList) {
                            if(memberBankInfo_item.getIsDefault().equals(Constants.MemberBankInfoIsDefault.TYPE0.getType())){
                                memberBankInfo_item.setIsDefault(Constants.MemberBankInfoIsDefault.TYPE1.getType());
                                memberBankInfoMapper.updateByPrimaryKeySelective(memberBankInfo_item);
                            }
                        }
                    }
                    /*保存银行卡*/
                    memberBankInfo.setId(CommonUtil.getUUID());
                    memberBankInfo.setCreateTime(new Date());
                    memberBankInfo.setMemberId(memberId);
                    memberBankInfo.setDelFlag("0");
                    memberBankInfo.setProvinceCityCountyId(provincesCitysCountrysId);
                    memberBankInfoMapper.insertSelective(memberBankInfo);

                    code = Constants.SUCCESS;
                    msg = "成功";
                }
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
