package com.aidianmao.serviceImpl;

import com.aidianmao.mapper.AdvertiseMapper;
import com.aidianmao.service.AdvertiseService;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
@Transactional
@Service
public class AdvertiseServiceImpl implements AdvertiseService {
    @Autowired
    private AdvertiseMapper advertiseMapper;
    @Override
    public Result selectSourceByType(String type) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<String> source=advertiseMapper.selectSourceByType(type);
            result.setData(source);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
