package com.aidianmao.serviceImpl;


import com.aidianmao.entity.OnlineQuestionAnswer;
import com.aidianmao.mapper.OnlineQuestionAnswerMapper;
import com.aidianmao.service.OnlineQuestionAnswerService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.PageHelperNew;
import com.aidianmao.utils.Result;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.ModelMap;

import java.util.*;

@Transactional
@Service
public class OnlineQuestionAnswerServiceImpl implements OnlineQuestionAnswerService {
    @Autowired
    private OnlineQuestionAnswerMapper onlineQuestionAnswerMapper;

    @Override
    public Result addOnlineQuestionAnswer(OnlineQuestionAnswer onlineQuestionAnswer, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            if (StringUtils.isBlank(onlineQuestionAnswer.getTitle())) {
                code = "-3";
                msg = "标题不能为空";
            } else if (StringUtils.isBlank(onlineQuestionAnswer.getQuestion())) {
                code = "-4";
                msg = "提问不能为空";
            }else if (StringUtils.isEmpty(onlineQuestionAnswer.getTradeType())) {
                code="-6";
                msg="交易类型不能为空";
            }else if (!(StringUtils.equals(onlineQuestionAnswer.getTradeType(),Constants.QuestionType.PRETRANSACTION.getType())||
                    StringUtils.equals(onlineQuestionAnswer.getTradeType(),Constants.QuestionType.INTRANSACTION.getType())||
                    StringUtils.equals(onlineQuestionAnswer.getTradeType(),Constants.QuestionType.AFTERTRANSACTION.getType())||
                    StringUtils.equals(onlineQuestionAnswer.getTradeType(),Constants.QuestionType.OTHERQUESTION.getType()))
                    ){
                code="-7";
                msg="交易类型不正确";
            } else {
                onlineQuestionAnswer.setId(CommonUtil.getUUID());
                onlineQuestionAnswer.setCreateTime(new Date());
                onlineQuestionAnswer.setDelFlag("0");
                onlineQuestionAnswer.setMemberId(memberId);
                onlineQuestionAnswer.setStatus("0");
                Integer list=onlineQuestionAnswerMapper.selectByDay(memberId);
                if(list<5){
                    onlineQuestionAnswerMapper.insert(onlineQuestionAnswer);
                    code = Constants.SUCCESS;
                    msg = "成功";
                }else {
                    code = "-8";
                    msg = "您当日已无法再进行此操作！";
                }
            }
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result pageList(Integer pageNum, Integer pageSize, String flag,String aidianmaoMemberToken,String tradeType) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try {
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            PageHelperNew.startPage(pageNum, pageSize);
            List<ModelMap> modelMapList =null;
            OnlineQuestionAnswer onlineQuestionAnswer = new OnlineQuestionAnswer();
            onlineQuestionAnswer.setTradeType(tradeType);
            if (StringUtils.isEmpty(flag)){
                return new Result("-2","查询类型不能为空");
                //flag=1查询所有
            }else if (StringUtils.equals(flag,"1")){
                modelMapList=onlineQuestionAnswerMapper.selectAllBySelection(onlineQuestionAnswer);
                //flag为2查询当前用户
            }else if(StringUtils.equals(flag,"2")){
                onlineQuestionAnswer.setMemberId(memberId);
                modelMapList=onlineQuestionAnswerMapper.selectAllByMember(onlineQuestionAnswer);
            }
            PageInfo<ModelMap> page = new PageInfo<>(modelMapList);
            result.setData(page);
            code = Constants.SUCCESS;
            msg = "成功";
        } catch (Exception e) {
            code = Constants.ERROR;
            msg = "后台繁忙";
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    /**
     * 随机出现已回答问题
     * @return
     */
    @Override
    public Result pageInterested(){
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            List<OnlineQuestionAnswer> modelMapList = onlineQuestionAnswerMapper.selectByExample();
            ArrayList<OnlineQuestionAnswer> modelMaps = new ArrayList<>();
            //获取一个随机数
            int random = new Random().nextInt(modelMapList.size());
            if (modelMapList.size()<8){
                modelMaps.addAll(modelMapList);
                for (OnlineQuestionAnswer on :
                        modelMapList) {
                    modelMaps.add(on);
                    if (modelMaps.size()>8){
                        break;
                    }
                }
            }
            if (random<(modelMapList.size()-8)){
                modelMaps.addAll(modelMapList.subList(random,random+8));
            }else{
                modelMaps.addAll(modelMapList.subList(0,8));
            }
            result.setData(modelMaps);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getCount(String flag, String aidianmaoMemberToken) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
            String memberId = CommonUtil.getMemberId(aidianmaoMemberToken);
            ArrayList<Object> list = new ArrayList<>();
            Integer count=0;
            if (StringUtils.isEmpty(flag)){
                return new Result("-2","查询类型不能为空");
                //flag=1查询所有
            }else if (StringUtils.equals(flag,"1")){
                OnlineQuestionAnswer onlineQuestionAnswer = new OnlineQuestionAnswer();
                onlineQuestionAnswer.setStatus("1");
                //全部已答问题
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.All.getInfo()+"("+count+")");
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.PRETRANSACTION.getType());
                //交易前
                count=onlineQuestionAnswerMapper.selectAllCount(onlineQuestionAnswer);
                list.add(Constants.QuestionType.PRETRANSACTION.getInfo()+"("+count+")");
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.INTRANSACTION.getType());
                //交易中
                count=onlineQuestionAnswerMapper.selectAllCount(onlineQuestionAnswer);
                list.add(Constants.QuestionType.INTRANSACTION.getInfo()+"("+count+")");
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.AFTERTRANSACTION.getType());
                //交易后
                count=onlineQuestionAnswerMapper.selectAllCount(onlineQuestionAnswer);
                list.add(Constants.QuestionType.AFTERTRANSACTION.getInfo()+"("+count+")");
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.OTHERQUESTION.getType());
                //其他问题
                count=onlineQuestionAnswerMapper.selectAllCount(onlineQuestionAnswer);
                list.add(Constants.QuestionType.OTHERQUESTION.getInfo()+"("+count+")");
                //flag为2查询当前用户
            }else if(StringUtils.equals(flag,"2")){
                OnlineQuestionAnswer onlineQuestionAnswer = new OnlineQuestionAnswer();
                onlineQuestionAnswer.setMemberId(memberId);
                //当前用户的全部问题
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.All.getInfo()+"("+count+")");
                //当前用户的交易前的问题
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.PRETRANSACTION.getType());
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.PRETRANSACTION.getInfo()+"("+count+")");
                //当前用户的交易中的问题
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.INTRANSACTION.getType());
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.INTRANSACTION.getInfo()+"("+count+")");
                //当前用户的交易后的问题
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.AFTERTRANSACTION.getType());
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.AFTERTRANSACTION.getInfo()+"("+count+")");
                //当前用户的其他问题
                onlineQuestionAnswer.setTradeType(Constants.QuestionType.OTHERQUESTION.getType());
                count=onlineQuestionAnswerMapper.selectCountSelection(onlineQuestionAnswer);
                list.add(Constants.QuestionType.OTHERQUESTION.getInfo()+"("+count+")");
            }
            result.setData(list);
            code = Constants.SUCCESS;
            msg = "成功";
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result getOnline(String id) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化";
        try{
           if (StringUtils.isEmpty(id)){
               code="-2";
               msg="问答id不能为空";
           }else {
               OnlineQuestionAnswer onlineQuestionAnswer = onlineQuestionAnswerMapper.selectById(id);
               if (onlineQuestionAnswer == null) {
                   code = "-4";
                   msg = "该问答不存在";
               } else {
                   //进入详情页增加浏览量
                   if (onlineQuestionAnswer.getBrowers() == null || onlineQuestionAnswer.getBrowers() < 0) {
                       onlineQuestionAnswer.setBrowers(1);
                   } else {
                       onlineQuestionAnswer.setBrowers(onlineQuestionAnswer.getBrowers() + 1);
                   }
                   onlineQuestionAnswerMapper.updateByPrimaryKeySelective(onlineQuestionAnswer);
                   result.setData(onlineQuestionAnswer);
                   code = Constants.SUCCESS;
                   msg = "成功";
               }
           }
        }catch (Exception e){
            code = Constants.ERROR;
            msg = "后台繁忙";
            e.printStackTrace();
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
