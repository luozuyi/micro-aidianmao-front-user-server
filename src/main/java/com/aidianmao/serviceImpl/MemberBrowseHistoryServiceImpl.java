package com.aidianmao.serviceImpl;

import com.aidianmao.entity.Tmall;
import com.aidianmao.mapper.TmallMapper;
import com.aidianmao.service.MemberBrowseHistoryService;
import com.aidianmao.utils.CommonUtil;
import com.aidianmao.utils.Constants;
import com.aidianmao.utils.RedisUtil;
import com.aidianmao.utils.Result;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MemberBrowseHistoryServiceImpl implements MemberBrowseHistoryService {


    @Autowired
    private TmallMapper tmallMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private HttpServletRequest request;

    private final static Logger log = LoggerFactory.getLogger(MemberBrowseHistoryServiceImpl.class);


    /**
     * 看店铺历史记录
     * @return
     */
    @Override
    public Result getMemberBrowseHistory() {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化值";
        try {
            String ipAddress = CommonUtil.getIpAddr(request);
            String history=ipAddress+"||ListHistory";
            long index=9;
            while (true) {
                List<Object> list = redisUtil.listRange(history, 0, index);
                ArrayList<Tmall> tmallList = new ArrayList<>();
                for (Object o :
                        list) {
                    Tmall tmall = tmallMapper.selectByPrimaryKey((String) o);
                    if (tmall!=null){
                        tmallList.add(tmall);
                    }
                    //集合大小为10时终止循环
                    if (tmallList.size()==10){
                        result.setData(tmallList);
                        break;
                    }
                }
                //index左位移1
                index=9<<1;
                //如果list的大小小于10，则直接退出循环
                if (list.size()<=10){
                    result.setData(tmallList);
                    break;
                }
            }
            code = Constants.SUCCESS;
            msg = "查询成功";
        }catch (Exception e){
            log.error("查看历史浏览记录"+e);
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    @Override
    public Result addMemberBrowseHistory(String tmallId) {
        Result result = new Result();
        String code = Constants.FAIL;
        String msg = "初始化值";
        try {
            String ipAddress = CommonUtil.getIpAddr(request);
            String history=ipAddress+"||ListHistory";
            if (StringUtils.isEmpty(tmallId)){
                return new Result("-3","店铺id不能为空");
            }
            Tmall tmall = tmallMapper.selectByPrimaryKey(tmallId);
            if (tmall==null){
                code="-2";
                msg="该店铺不存在";
            }else if (!StringUtils.equals(tmall.getStatus(),"1")){
                code="-3";
                msg="只添加正在出售的店铺";
            }else {
                List<Object> list =redisUtil.listRange(history, 0, 9);
                if (list!=null && list.size()>0) {
                    for (int i = 0; i < list.size(); i++) {
                        //如果存在相同的id，则删除之前id所在位置的值，然后重新放入
                        if (StringUtils.equals((String) list.get(i), tmallId)) {
                            redisUtil.listRemove(history, i, list.get(i));
                        }
                    }
                }
                redisUtil.listPush(history,tmallId);
                code=Constants.SUCCESS;
                msg="新增成功";
            }
        }catch (Exception e){
            log.error("添加历史浏览记录"+e);
            e.printStackTrace();
            code = Constants.ERROR;
            msg = "系统繁忙";
        }
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
